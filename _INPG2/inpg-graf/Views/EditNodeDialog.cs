﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Windows.Forms;
using ZdenekDrahos.App.Model;
using inpg_graf.Editor;

namespace inpg_graf.Views
{
    public delegate void EditNodeAttributesEventHandler(object sender, AppEventArgs<EditNodeType> e);

    public enum EditNodeType
    {
        BUS_STOP, REST_AREA, CROSS_ROAD
    }

    public partial class EditNodeDialog : Form
    {
        private ICrossRoad crossRoad;
        public event EditNodeAttributesEventHandler EditNodeAttributes;

        public EditNodeDialog(ICrossRoad crossRoad, EditNodeAttributesEventHandler handler)
        {
            this.crossRoad = crossRoad;
            EditNodeAttributes += handler;
            InitializeComponent();
        }

        private void EditNodeDialog_Load(object sender, System.EventArgs e)
        {
            labelID.Text = crossRoad.ID;
            if (crossRoad.IsBusStop)
                radioButtonBus.Checked = true;
            else if (crossRoad.IsRestArea)
                radioButtonRest.Checked = true;
            else
                radioButtonSimple.Checked = true;
        }

        private void EditNodeDialog_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (!radioButtonBus.Checked && !radioButtonRest.Checked && !radioButtonSimple.Checked)
            {
                Messages.Error("Musíte vybrat jeden typ křižovatky");
                e.Cancel = true;
            }
        }

        private void EditNodeDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                if (radioButtonBus.Checked)
                    OnEditNode(EditNodeType.BUS_STOP);
                else if (radioButtonRest.Checked)
                    OnEditNode(EditNodeType.REST_AREA);
                else
                    OnEditNode(EditNodeType.CROSS_ROAD);     
            }
        }

        protected virtual void OnEditNode(EditNodeType newType)
        {
            EditNodeAttributesEventHandler handler = this.EditNodeAttributes;
            if (handler != null)
                handler(this, new AppEventArgs<EditNodeType>(newType));
        }  
    }
}
