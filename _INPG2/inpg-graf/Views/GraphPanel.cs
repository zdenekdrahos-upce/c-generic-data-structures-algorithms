﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using inpg_graf.Editor;
using inpg_graf.Editor.AddLine;
using inpg_graf.Editor.AddRectangle;
using inpg_graf.Editor.ClosestElement;
using inpg_graf.Editor.DrawTools;
using inpg_graf.Editor.MinimalArea;
using inpg_graf.Editor.MoveElement;
using inpg_graf.Helpers;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph;

namespace inpg_graf.Views
{
    public delegate void ClosestNodeFoundEventHandler(object sender, AppEventArgs<ICrossRoad> e);
    public delegate void ClosestEdgeFoundEventHandler(object sender, AppEventArgs<Edge<string, ICrossRoad, double>> e);

    public partial class GraphPanel : Panel
    {
        private IForestArea forest;
        private ZoomHelper zoom;
        private DrawingTools drawTools = new DrawingTools();
        private MouseEvents mouseEvents = new MouseEvents();
        private ClosestElement<ICrossRoad> closestNode;
        private ClosestElement<Edge<string, ICrossRoad, double>> closestEdge;
        private MinimalRectangle minimalPaintArea = new MinimalRectangle(Properties.Settings.Default.ClipMargin);

        public ClosestNodeFoundEventHandler ClosestNodeFound;
        public ClosestEdgeFoundEventHandler ClosestEdgeFound;

        # region INIT_GRAPH_PANEL

        public GraphPanel(Size defaultSize, ZoomHelper zoom)
        {
            InitializeComponent();
            LoadGraph();
            Size = defaultSize;
            InitZoom(zoom);
            InitClosestElements();
            AddDoubleBuffering();
        }

        private void LoadGraph()
        {
            string projectDirectory = Directory.GetParent(Application.ExecutablePath).Parent.Parent.FullName;
            IForestDataMapper dm = new ForestDataMapper(new CrossRoadFactory());
            forest = dm.LoadForest(Path.Combine(projectDirectory, "inpg2-graph.txt"));
        }

        private void InitZoom(ZoomHelper zoom)
        {
            this.zoom = zoom;
            zoom.ZoomChanged += this.OnZoomChanged;
            zoom.SetWindowSize(Size);
        }

        public void InitClosestElements()
        {
            MinDistanceSearch minDistance = new MinDistanceSearch(Properties.Settings.Default.GraphElementMagnetism);
            closestNode = new ClosestElement<ICrossRoad>(minDistance, PointHelper.CrossRoadDistance);
            closestNode.SavePrevious();
            closestEdge = new ClosestElement<Edge<string, ICrossRoad, double>>(minDistance, PointHelper.GetDistancePointLine);
            closestEdge.SavePrevious();
        }

        private void AddDoubleBuffering()
        {
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
        }

        # endregion

        #region PANEL_METHODS

        public void ResizePanel(Size newSize)
        {
            zoom.ResizeWindow(newSize);
        }

        private void GraphPanel_MouseEnter(object sender, EventArgs e)
        {
            Focus();
        }

        # endregion

        # region PAINT

        private void GraphPanel_Paint(object sender, PaintEventArgs e)
        {
            PaintGraph(e.Graphics);
        }

        private void PaintInMouseEvent()
        {
            Invalidate(minimalPaintArea.Rectangle);
        }

        private void PaintGraph(Graphics g)
        {
            if (!g.IsVisibleClipEmpty)
            {
                DrawEdges(g);
                DrawNodes(g);
            }
        }

        private void DrawEdges(Graphics g)
        {
            foreach (Edge<string, ICrossRoad, double> edge in forest.EdgesIterator())
                DrawEdge(g, drawTools.EdgeDefault, edge);
            if (closestEdge.Exists)
                DrawEdge(g, drawTools.EdgeClosest, closestEdge.Element);
        }

        private void DrawEdge(Graphics g, DrawToolEdge drawEdge, Edge<string, ICrossRoad, double> edge)
        {
            g.DrawLine(drawEdge.Pen, zoom.GetRelativePointLocation(edge.StartNode.Point), zoom.GetRelativePointLocation(edge.EndNode.Point));
        }

        private void DrawNodes(Graphics g)
        {
            foreach (ICrossRoad crossRoad in forest.CrossRoadIterator())
                DrawNode(g, GetNodeFillColor(crossRoad), crossRoad);
            if (closestNode.Exists)
                DrawNode(g, drawTools.NodeClosest, closestNode.Element);
        }

        private void DrawNode(Graphics g, DrawToolNode drawNode, ICrossRoad crossRoad)
        {
            Point relativePosition = zoom.GetRelativePointLocation(crossRoad.Point);
            g.FillEllipse(drawNode.Brush, relativePosition.X - drawNode.Radius, relativePosition.Y - drawNode.Radius, drawNode.Diameter(), drawNode.Diameter());
            g.DrawEllipse(drawNode.Pen, relativePosition.X - drawNode.Radius, relativePosition.Y - drawNode.Radius, drawNode.Diameter(), drawNode.Diameter());
        }

        private DrawToolNode GetNodeFillColor(ICrossRoad crossRoad)
        {
            if (crossRoad.IsBusStop)
                return drawTools.NodeBusStop;
            else if (crossRoad.IsRestArea)
                return drawTools.NodeRestArea;
            else
                return drawTools.NodeDefault;
        }

        # endregion

        #region ZOOM

        private void OnZoomChanged(object sender, EventArgs e)
        {
            zoom.UpdateSize();
            UpdateLocation();
            Size = zoom.ActualSize;
        }

        public void Zoom_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            zoom.AutoZoom(e.Delta);
            zoom.SetTopLeftCornerMouseWheel(e.Location);
            UpdateLocation();
        }

        private void UpdateLocation()
        {
            Location = zoom.GetLocation();
        }

        # endregion

        #region ZOOM_RECT

        private AddRectangle zoomRect = new AddRectangle(Properties.Settings.Default.ZoomRectMinSize);

        public void ZoomRect_MouseDown(object sender, MouseEventArgs e)
        {
            zoomRect.Down(e.Location);
        }

        public void ZoomRect_MouseMove(object sender, MouseEventArgs e)
        {
            zoomRect.Move(e.Location, CreateGraphics());
        }

        public void ZoomRect_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                Rectangle rectangle = zoomRect.Up(e.Location, CreateGraphics());
                zoom.ZoomRect(rectangle);
                zoom.SetTopLeftCornerZoomRect(rectangle.Location);
                UpdateLocation();
            }
            catch (SmallRectangleException) { }
        }

        # endregion

        # region NODE_MOUSE_MOVE

        public void Node_MouseMove(object sender, MouseEventArgs e)
        {
            Point mouseLocation = Point.Round(zoom.GetRealPointLocation(e.Location));
            closestNode.FindClosestElement(mouseLocation, forest.CrossRoadIterator());
            SetClipForClosestNode();
            if (closestNode.Exists || closestNode.Previous.Exists)
                PaintInMouseEvent();
            OnClosestNodeFound();
        }

        private void SetClipForClosestNode()
        {
            minimalPaintArea.ResetClip();
            if (closestNode.Exists)
                minimalPaintArea.AddPoint(closestNode.Element.Point, zoom);
            if (closestNode.Previous.Exists)
                minimalPaintArea.AddPoint(closestNode.Previous.Element.Point, zoom);
        }

        protected virtual void OnClosestNodeFound()
        {
            ClosestNodeFoundEventHandler handler = this.ClosestNodeFound;
            ICrossRoad crossRoad = closestNode.Exists ? closestNode.Element : null;
            if (handler != null)
                handler(this, new AppEventArgs<ICrossRoad>(crossRoad));
        }

        # endregion

        # region EDGE_MOUSE_MOVE

        public void Edge_MouseMove(object sender, MouseEventArgs e)
        {
            Point mouseLocation = Point.Round(zoom.GetRealPointLocation(e.Location));
            closestEdge.FindClosestElement(mouseLocation, forest.EdgesIterator());
            SetClipForClosestEdge();
            if (closestEdge.Exists || closestEdge.Previous.Exists)
                PaintInMouseEvent();
            OnClosestEdgeFound();
        }

        private void SetClipForClosestEdge()
        {
            minimalPaintArea.ResetClip();
            if (closestEdge.Exists)
            {// až budou správně sousedi, tak by měl stačit jeden bod
                SetClipAccordingNode(closestEdge.Element.StartNode);
                SetClipAccordingNode(closestEdge.Element.EndNode);
            }
            if (closestEdge.Previous.Exists)
            {
                try
                {
                    SetClipAccordingNode(closestEdge.Previous.Element.StartNode);
                    SetClipAccordingNode(closestEdge.Previous.Element.EndNode);
                }
                catch (KeyNotFoundException) {/* vrchol už může být smazaný, takže nebude existovat v hashTabulce vrcholů grafu */ }
            }
        }

        protected virtual void OnClosestEdgeFound()
        {
            ClosestEdgeFoundEventHandler handler = this.ClosestEdgeFound;
            Edge<string, ICrossRoad, double> edge = closestEdge.Exists ? closestEdge.Element : default(Edge<string, ICrossRoad, double>);
            if (handler != null)
                handler(this, new AppEventArgs<Edge<string, ICrossRoad, double>>(edge));
        }

        # endregion

        # region EDIT_NODE_EDGE

        public void EditNodeEdge_MouseDown(object sender, MouseEventArgs e)
        {
            if (closestNode.Exists)
                EditNode();
            else if (closestEdge.Exists)
                EditEdge();
            else
                Messages.Error("Vyberte vrchol nebo hranu, kterou chcete editovat");
        }

        private void EditNode()
        {
            using (EditNodeDialog dialog = new EditNodeDialog(closestNode.Element, this.OnEditNodeAttributes))
                dialog.ShowDialog();
        }

        private void OnEditNodeAttributes(object sender, AppEventArgs<EditNodeType> e)
        {
            switch (e.Data)
            {
                case EditNodeType.BUS_STOP: forest.MarkCrossRoadAsBusStop(closestNode.Element.ID); break;
                case EditNodeType.REST_AREA: forest.MarkCrossRoadAsRestArea(closestNode.Element.ID); break;
                case EditNodeType.CROSS_ROAD: forest.MarkCrossRoadAsBasic(closestNode.Element.ID); break;
            }
            Invalidate(); // TODO: remove
        }

        private void EditEdge()
        {
            using (EditEdgeDialog dialog = new EditEdgeDialog(closestEdge.Element, this.OnEditEdgeAttributes))
                dialog.ShowDialog();
        }

        private void OnEditEdgeAttributes(object sender, AppEventArgs<double> e)
        {
            forest.ModifyEdge(closestEdge.Element.StartNode.ID, closestEdge.Element.EndNode.ID, e.Data);
            Invalidate(); // TODO: remove
        }

        # endregion

        # region NODE_ADD

        public void NodeAdd_MouseDown(object sender, MouseEventArgs e)
        {
            if (!closestNode.Exists)
            {
                Point point = Point.Round(zoom.GetRealPointLocation(e.Location));
                forest.AddCrossRoad(point);
                SetNewClipAccordingPoint(point);
                PaintInMouseEvent();
            }
            else
                Messages.Error("Nemůže přidat vrchol, pokud se nacházíte tak blízko jiného vrcholu");
        }

        # endregion

        # region NODE_DELETE

        public void NodeDelete_MouseDown(object sender, MouseEventArgs e)
        {
            if (!closestNode.Exists)
                Messages.Error("Musíte najet na vrchol, který chcete smazat");
            else if (CanBeNodeDeleted())
            {
                SetNewClipAccordingNode(closestNode.Element);
                forest.RemoveCrossRoad(closestNode.Element.ID);
                PaintInMouseEvent();
            }
        }

        private bool CanBeNodeDeleted()
        {
            return Messages.QuestionYesNo("Opravdu chcete smazat vrchol?");
        }

        # endregion

        # region NODE_MOVE

        private MovePoint movePoint = new MovePoint();

        private ICrossRoad moveStart;

        public void NodeMove_MouseDown(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            if (closestNode.Exists)
            {
                movePoint.Down(e.Location, closestNode.Element.Point);
                moveStart = closestNode.Element;
            }
            else
                Messages.Error("Musíte najet na vrchol, který chcete posouvat");
        }

        public void NodeMove_MouseMove(object sender, MouseEventArgs e)
        {
            if (movePoint.IsMoving)
            {
                movePoint.Move(e.Location);
                MoveNodeAndRepaint();
                movePoint.Down(e.Location, movePoint.NewPosition);
            }
        }

        public void NodeMove_MouseUp(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
            if (movePoint.IsMoving)
            {
                movePoint.Up(e.Location);
                MoveNodeAndRepaint();
            }
        }

        private void MoveNodeAndRepaint()
        {
            if (closestNode.Exists)
            {
                forest.ModifyCrossRoad(closestNode.Element.ID, movePoint.NewPosition);
                SetNewClipAccordingNode(moveStart);
                SetClipAccordingNode(closestNode.Element);
                PaintInMouseEvent();
            }
        }

        # endregion

        # region EDGE_ADD

        private AddLine addEdge = new AddLine();
        private ICrossRoad startCrossRoad;

        public void EdgeAdd_MouseDown(object sender, MouseEventArgs e)
        {
            if (closestNode.Exists)
            {
                startCrossRoad = closestNode.Element;
                addEdge.Down(zoom.GetRelativePointLocation(closestNode.Element.Point));
            }
            else
                Messages.Error("Musíte najet na/k vrchol, který bude počáteční");
        }

        public void EdgeAdd_MouseMove(object sender, MouseEventArgs e)
        {
            if (addEdge.IsActive)
                addEdge.Move(e.Location, CreateGraphics());
        }

        public void EdgeAdd_MouseUp(object sender, MouseEventArgs e)
        {
            if (addEdge.IsActive)
            {
                try
                {
                    addEdge.Up(closestNode.Element.Point, CreateGraphics());
                    forest.AddEdge(startCrossRoad.ID, closestNode.Element.ID, 5);
                    RepaintAfterAddEdge();
                }
                catch (InvalidLineException) { Messages.Error("Zvolen neexistující vrchol"); }
                catch (ArgumentException) { Messages.Error("Hrana už existuje mezi vrcholy"); }

            }
        }

        private void RepaintAfterAddEdge()
        {
            Point start = Point.Round(zoom.GetRealPointLocation(addEdge.StartPoint));
            Point end = Point.Round(zoom.GetRelativePointLocation(closestNode.Element.Point));
            minimalPaintArea.ResetClip();
            minimalPaintArea.AddPoint(start, zoom);
            minimalPaintArea.AddPoint(end, zoom);
            PaintInMouseEvent();
        }

        # endregion

        # region EDGE_DELETE

        public void EdgeDelete_MouseDown(object sender, MouseEventArgs e)
        {
            if (!closestEdge.Exists)
                Messages.Error("Musíte najet na/k hraně, kterou chcete smazat");
            else
                DeleteEdge();
        }

        private void DeleteEdge()
        {
            try
            {
                SetNewClipAccordingNode(closestEdge.Element.StartNode);
                forest.RemoveEdge(closestEdge.Element.StartNode.ID, closestEdge.Element.EndNode.ID);
                PaintInMouseEvent();
            }
            catch (Exception)
            {
                Messages.Error("Vymazání hrany se nezdařilo");
            }
        }

        # endregion

        # region EDGE_MOVE

        private MoveLine moveLine = new MoveLine();
        private Edge<string, ICrossRoad, double> movedEdge;

        public void EdgeMove_MouseDown(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
            if (closestEdge.Exists)
            {
                movedEdge = closestEdge.Element;
                SetMoveLineDown(e.Location);
            }
            else
                Messages.Error("Musíte najet nad hranu, kterou chcete posouvat");
        }

        private void SetMoveLineDown(Point mouseLocation)
        {
            KeyValuePair<Point, Point> line = new KeyValuePair<Point, Point>(movedEdge.StartNode.Point, movedEdge.EndNode.Point);
            moveLine.Down(mouseLocation, line);
        }

        public void EdgeMove_MouseMove(object sender, MouseEventArgs e)
        {
            if (moveLine.IsMoving)
            {
                moveLine.Move(e.Location);
                MoveEdgeAndRepatint();
                movedEdge = forest.FindEdge(movedEdge.StartNode.ID, movedEdge.EndNode.ID);
                SetMoveLineDown(e.Location);
            }
        }

        public void EdgeMove_MouseUp(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
            if (moveLine.IsMoving)
            {
                moveLine.Up(e.Location);
                MoveEdgeAndRepatint();
            }
        }

        private void MoveEdgeAndRepatint()
        {
            SetNewClipAccordingNode(movedEdge.StartNode);
            SetNewClipAccordingNode(movedEdge.EndNode);
            forest.MoveEdge(movedEdge.StartNode.ID, movedEdge.EndNode.ID, moveLine.NewLine);
            minimalPaintArea.AddPoint(moveLine.NewLine.Key, zoom);
            minimalPaintArea.AddPoint(moveLine.NewLine.Value, zoom);
            PaintInMouseEvent();
        }

        # endregion

        # region CLIP

        private void SetNewClipAccordingNode(ICrossRoad node)
        {
            minimalPaintArea.ResetClip();
            SetClipAccordingNode(node);
        }

        private void SetNewClipAccordingPoint(Point point)
        {
            minimalPaintArea.ResetClip();
            minimalPaintArea.AddPoint(point, zoom);
        }

        private void SetClipAccordingNode(ICrossRoad node)
        {
            minimalPaintArea.AddPoint(node.Point, zoom);
            foreach (ICrossRoad neighbour in forest.IncidenceNodesIterator(node.ID))
                minimalPaintArea.AddPoint(neighbour.Point, zoom);
        }

        # endregion

        # region MOUSE_EVENTS

        public void ResetMouseEvents()
        {
            foreach (MouseEventHandler handler in mouseEvents.MouseDown) MouseDown -= handler;
            foreach (MouseEventHandler handler in mouseEvents.MouseMove) MouseMove -= handler;
            foreach (MouseEventHandler handler in mouseEvents.MouseUp) MouseUp -= handler;
            foreach (MouseEventHandler handler in mouseEvents.MouseWheel) MouseWheel -= handler;
        }

        public void AddMouseDownHandler(MouseEventHandler handler)
        {
            MouseDown += handler;
            mouseEvents.MouseDown.Add(handler);
        }

        public void AddMouseMoveHandler(MouseEventHandler handler)
        {
            MouseMove += handler;
            mouseEvents.MouseMove.Add(handler);
        }

        public void AddMouseUpHandler(MouseEventHandler handler)
        {
            MouseUp += handler;
            mouseEvents.MouseUp.Add(handler);
        }

        public void AddMouseWheelHandler(MouseEventHandler handler)
        {
            MouseWheel += handler;
            mouseEvents.MouseWheel.Add(handler);
        }

        # endregion
    }
}
