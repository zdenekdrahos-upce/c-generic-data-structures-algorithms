﻿namespace inpg_graf.Views
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.panel = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.zoomLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripDropDownButtonAkce = new System.Windows.Forms.ToolStripDropDownButton();
            this.graphEditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeMoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeDeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edgeAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edgeMoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edgeDeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomRectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomResetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelNode = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelEdge = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.AutoScroll = true;
            this.panel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(514, 420);
            this.panel.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.zoomLabel,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabelNode,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabelEdge,
            this.toolStripStatusLabel4,
            this.toolStripDropDownButtonAkce});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(514, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Zoom:";
            // 
            // zoomLabel
            // 
            this.zoomLabel.Name = "zoomLabel";
            this.zoomLabel.Size = new System.Drawing.Size(18, 17);
            this.zoomLabel.Text = "1x";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(249, 17);
            this.toolStripStatusLabel4.Spring = true;
            // 
            // toolStripDropDownButtonAkce
            // 
            this.toolStripDropDownButtonAkce.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButtonAkce.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.graphEditToolStripMenuItem,
            this.nodeAddToolStripMenuItem,
            this.nodeMoveToolStripMenuItem,
            this.nodeDeleteToolStripMenuItem,
            this.edgeAddToolStripMenuItem,
            this.edgeMoveToolStripMenuItem,
            this.edgeDeleteToolStripMenuItem,
            this.zoomToolStripMenuItem,
            this.zoomRectToolStripMenuItem,
            this.zoomResetToolStripMenuItem});
            this.toolStripDropDownButtonAkce.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButtonAkce.Image")));
            this.toolStripDropDownButtonAkce.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButtonAkce.Name = "toolStripDropDownButtonAkce";
            this.toolStripDropDownButtonAkce.Size = new System.Drawing.Size(46, 20);
            this.toolStripDropDownButtonAkce.Text = "Akce";
            this.toolStripDropDownButtonAkce.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripDropDownButton1_DropDownItemClicked);
            // 
            // graphEditToolStripMenuItem
            // 
            this.graphEditToolStripMenuItem.Name = "graphEditToolStripMenuItem";
            this.graphEditToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.graphEditToolStripMenuItem.Tag = "10";
            this.graphEditToolStripMenuItem.Text = "Editace vrcholů a hran";
            // 
            // nodeAddToolStripMenuItem
            // 
            this.nodeAddToolStripMenuItem.Name = "nodeAddToolStripMenuItem";
            this.nodeAddToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.nodeAddToolStripMenuItem.Tag = "1";
            this.nodeAddToolStripMenuItem.Text = "Vložení nového uzlu";
            // 
            // nodeMoveToolStripMenuItem
            // 
            this.nodeMoveToolStripMenuItem.Name = "nodeMoveToolStripMenuItem";
            this.nodeMoveToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.nodeMoveToolStripMenuItem.Tag = "2";
            this.nodeMoveToolStripMenuItem.Text = "Posun uzlu";
            // 
            // nodeDeleteToolStripMenuItem
            // 
            this.nodeDeleteToolStripMenuItem.Name = "nodeDeleteToolStripMenuItem";
            this.nodeDeleteToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.nodeDeleteToolStripMenuItem.Tag = "3";
            this.nodeDeleteToolStripMenuItem.Text = "Vymazání uzlu";
            // 
            // edgeAddToolStripMenuItem
            // 
            this.edgeAddToolStripMenuItem.Name = "edgeAddToolStripMenuItem";
            this.edgeAddToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.edgeAddToolStripMenuItem.Tag = "7";
            this.edgeAddToolStripMenuItem.Text = "Přidání nové hrany";
            // 
            // edgeMoveToolStripMenuItem
            // 
            this.edgeMoveToolStripMenuItem.Name = "edgeMoveToolStripMenuItem";
            this.edgeMoveToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.edgeMoveToolStripMenuItem.Tag = "8";
            this.edgeMoveToolStripMenuItem.Text = "Přesun hrany";
            // 
            // edgeDeleteToolStripMenuItem
            // 
            this.edgeDeleteToolStripMenuItem.Name = "edgeDeleteToolStripMenuItem";
            this.edgeDeleteToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.edgeDeleteToolStripMenuItem.Tag = "9";
            this.edgeDeleteToolStripMenuItem.Text = "Smazat hranu";
            // 
            // zoomToolStripMenuItem
            // 
            this.zoomToolStripMenuItem.Name = "zoomToolStripMenuItem";
            this.zoomToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.zoomToolStripMenuItem.Tag = "4";
            this.zoomToolStripMenuItem.Text = "Zoom kolečkem myši";
            // 
            // zoomRectToolStripMenuItem
            // 
            this.zoomRectToolStripMenuItem.Name = "zoomRectToolStripMenuItem";
            this.zoomRectToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.zoomRectToolStripMenuItem.Tag = "5";
            this.zoomRectToolStripMenuItem.Text = "Zoom obdélníkem";
            // 
            // zoomResetToolStripMenuItem
            // 
            this.zoomResetToolStripMenuItem.Name = "zoomResetToolStripMenuItem";
            this.zoomResetToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.zoomResetToolStripMenuItem.Tag = "6";
            this.zoomResetToolStripMenuItem.Text = "Reset zoomu";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.AutoScroll = true;
            this.toolStripContainer1.ContentPanel.Controls.Add(this.panel);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(514, 420);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(514, 442);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            this.toolStripContainer1.TopToolStripPanelVisible = false;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Margin = new System.Windows.Forms.Padding(5, 3, 0, 2);
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(47, 17);
            this.toolStripStatusLabel2.Text = "Vrchol: ";
            // 
            // toolStripStatusLabelNode
            // 
            this.toolStripStatusLabelNode.Name = "toolStripStatusLabelNode";
            this.toolStripStatusLabelNode.Size = new System.Drawing.Size(12, 17);
            this.toolStripStatusLabelNode.Text = "-";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Margin = new System.Windows.Forms.Padding(5, 3, 0, 2);
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel3.Text = "Hrana:";
            // 
            // toolStripStatusLabelEdge
            // 
            this.toolStripStatusLabelEdge.Name = "toolStripStatusLabelEdge";
            this.toolStripStatusLabelEdge.Size = new System.Drawing.Size(12, 17);
            this.toolStripStatusLabelEdge.Text = "-";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 442);
            this.Controls.Add(this.toolStripContainer1);
            this.DoubleBuffered = true;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.MainWindow_HelpButtonClicked);
            this.Resize += new System.EventHandler(this.MainWindow_Resize);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel zoomLabel;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButtonAkce;
        private System.Windows.Forms.ToolStripMenuItem nodeAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStripMenuItem nodeMoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nodeDeleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomRectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomResetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edgeAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edgeMoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edgeDeleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphEditToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelNode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelEdge;
    }
}

