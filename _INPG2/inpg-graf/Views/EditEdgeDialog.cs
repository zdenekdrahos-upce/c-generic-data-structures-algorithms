﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Windows.Forms;
using ZdenekDrahos.Graph;
using ZdenekDrahos.App.Model;
using inpg_graf.Editor;

namespace inpg_graf.Views
{
    public delegate void EditEdgeAttributesEventHandler(object sender, AppEventArgs<double> e);

    public partial class EditEdgeDialog : Form
    {

        public event EditEdgeAttributesEventHandler EditEdgeAttributes;
        private Edge<string, ICrossRoad, double> edge;

        public EditEdgeDialog(Edge<string, ICrossRoad, double> edge, EditEdgeAttributesEventHandler handler)
        {
            this.edge = edge;
            EditEdgeAttributes += handler;
            InitializeComponent();
        }

        private void EditEdgeDialog_Load(object sender, System.EventArgs e)
        {
            numericUpDownCost.Minimum = 1;
            numericUpDownCost.Maximum = 100;
            labelStart.Text = edge.StartNode.ID;
            labelEnd.Text = edge.EndNode.ID;
            numericUpDownCost.Value = (int)edge.EdgeCost;
        }

        private void EditEdgeDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (numericUpDownCost.Value <= 0 || numericUpDownCost.Value > 100)
            {
                Messages.Error("Cena musí být číslo větší než 0 a menší než 100");
                e.Cancel = true;
            }
        }

        private void EditEdgeDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
                OnEditEdge((double)numericUpDownCost.Value);
        }

        protected virtual void OnEditEdge(double newCost)
        {
            EditEdgeAttributesEventHandler handler = this.EditEdgeAttributes;
            if (handler != null)
                handler(this, new AppEventArgs<double>(newCost));
        }  
    }
}
