﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using inpg_graf.Editor;
using inpg_graf.Helpers;
using inpg_graf.Menu;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph;

namespace inpg_graf.Views
{
    public partial class MainWindow : Form
    {
        private Size OriginalSize;
        private MenuActions menu;
        private ZoomHelper zoom;
        private GraphPanel graphPanel;

        # region INIT_MAIN_WINDOW

        public MainWindow()
        {
            InitializeComponent();
            MinimumSize = Size;
            OriginalSize = Size;
            LoadMenu();
            InitZoom();
            InitGraphPanel();
            panel.Controls.Add(graphPanel);
        }

        private void LoadMenu()
        {
            menu = new MenuActions();
            menu.AddActionCallback(MenuAction.EDIT_NODE_EDGE, this.OnEditNodeEdge);
            menu.AddActionCallback(MenuAction.NODE_ADD, this.OnNodeAdd);
            menu.AddActionCallback(MenuAction.NODE_DELETE, this.OnNodeDelete);
            menu.AddActionCallback(MenuAction.NODE_MOVE, this.OnNodeMove);
            menu.AddActionCallback(MenuAction.EDGE_ADD, this.OnEdgeAdd);
            menu.AddActionCallback(MenuAction.EDGE_DELETE, this.OnEdgeDelete);
            menu.AddActionCallback(MenuAction.EDGE_MOVE, this.OnEdgeMove);
            menu.AddActionCallback(MenuAction.ZOOM, this.OnZoom);
            menu.AddActionCallback(MenuAction.ZOOM_RECT, this.OnZoomRect);
            menu.AddActionCallback(MenuAction.ZOOM_RESET, this.OnZoomReset);
        }

        private void InitZoom()
        {
            zoom = new ZoomHelper(Properties.Settings.Default.ZoomIncrement);
            zoom.ZoomChanged += this.OnZoomChanged;
        }

        private void InitGraphPanel()
        {
            graphPanel = new GraphPanel(panel.Size, zoom);
            graphPanel.MouseMove += this.Title_MouseMove;
            graphPanel.MouseMove += graphPanel.Edge_MouseMove;
            graphPanel.MouseMove += graphPanel.Node_MouseMove;
            graphPanel.ClosestNodeFound += this.OnClosestNodeFound;
            graphPanel.ClosestEdgeFound += this.OnClosestEdgeFound;
        }

        # endregion

        # region FORM_METHOD

        private void MainWindow_Resize(object sender, EventArgs e)
        {
            graphPanel.ResizePanel(panel.Size);
        }

        private void Title_MouseMove(object sender, MouseEventArgs e)
        {
            Text = StringFormatter.ActualPositionToString(zoom.GetRealPointLocation(e.Location));
        }

        public void OnZoomChanged(object sender, EventArgs e)
        {
            zoomLabel.Text = StringFormatter.ZoomToString(zoom.Zoom);
        }

        private void MainWindow_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string message = menu.SelectedAction != MenuAction.INVALID ? 
                menu.GetDescriptionOfSelectedAction() : "Nápověda je k dispozici pro každou používanou akci během jejího použití.";
            Messages.Information(message);
        }

        private void OnClosestNodeFound(object sender, AppEventArgs<ICrossRoad> e)
        {
            toolStripStatusLabelNode.Text = e.Data == null ? "-" : e.Data.ToString();
        }

        private void OnClosestEdgeFound(object sender, AppEventArgs<Edge<string, ICrossRoad, double>> e)
        {
            toolStripStatusLabelEdge.Text = e.Data.StartNode == null ? "-" : StringFormatter.FormatEdge(e.Data);
        }

        # endregion

        # region MENU

        private void toolStripDropDownButton1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem clickedItem = e.ClickedItem;
            foreach (ToolStripItem item in toolStripDropDownButtonAkce.DropDown.Items)            
                (item as ToolStripMenuItem).Checked = item.Equals(clickedItem);
            PerformClickedItem(clickedItem.Tag.ToString());
        }

        private void PerformClickedItem(string id)
        {
            menu.LoadMenuAction(id);
            graphPanel.ResetMouseEvents();
            graphPanel.InitClosestElements();
            menu.TryPerformMenuAction();
        }

        private void OnEditNodeEdge()
        {
            graphPanel.AddMouseDownHandler(graphPanel.EditNodeEdge_MouseDown);
        }

        private void OnNodeAdd()
        {            
            graphPanel.AddMouseDownHandler(graphPanel.NodeAdd_MouseDown);
        }

        private void OnNodeDelete()
        {
            graphPanel.AddMouseDownHandler(graphPanel.NodeDelete_MouseDown);
        }

        private void OnNodeMove()
        {
            graphPanel.AddMouseDownHandler(graphPanel.NodeMove_MouseDown);
            graphPanel.AddMouseMoveHandler(graphPanel.NodeMove_MouseMove);
            graphPanel.AddMouseUpHandler(graphPanel.NodeMove_MouseUp); 
        }

        private void OnEdgeAdd()
        {
            graphPanel.AddMouseDownHandler(graphPanel.EdgeAdd_MouseDown);
            graphPanel.AddMouseMoveHandler(graphPanel.EdgeAdd_MouseMove);
            graphPanel.AddMouseUpHandler(graphPanel.EdgeAdd_MouseUp); 
        }

        private void OnEdgeDelete()
        {
            graphPanel.AddMouseDownHandler(graphPanel.EdgeDelete_MouseDown);
        }

        private void OnEdgeMove()
        {
            graphPanel.AddMouseDownHandler(graphPanel.EdgeMove_MouseDown);
            graphPanel.AddMouseMoveHandler(graphPanel.EdgeMove_MouseMove);
            graphPanel.AddMouseUpHandler(graphPanel.EdgeMove_MouseUp); 
        }

        private void OnZoom()
        {
            graphPanel.AddMouseWheelHandler(graphPanel.Zoom_MouseWheel);            
        }

        private void OnZoomRect()
        {
            graphPanel.AddMouseDownHandler(graphPanel.ZoomRect_MouseDown);
            graphPanel.AddMouseMoveHandler(graphPanel.ZoomRect_MouseMove);
            graphPanel.AddMouseUpHandler(graphPanel.ZoomRect_MouseUp);
        }

        private void OnZoomReset()
        {
            zoom.ResetZoom();
            Size = OriginalSize;            
        }

        # endregion

    }
}
