﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace inpg_graf.Menu
{
    /// <summary>
    /// Třída pro uchovávání, načítání a spouštění akcí pro zvolenou položku z menu.
    /// </summary>
    class MenuActions
    {
        private int countItems;
        private IDictionary<MenuAction, MenuActionCallback> actions;

        public MenuAction SelectedAction { get; private set; }

        public MenuActions()
        {
            countItems = Enum.GetValues(typeof(MenuAction)).Length;
            actions = new Dictionary<MenuAction, MenuActionCallback>();
        }

        public void AddActionCallback(MenuAction action, MenuActionCallback callback)
        {
            actions.Add(action, callback);
        }

        public void TryPerformMenuAction()
        {
            if (actions.ContainsKey(SelectedAction))
                actions[SelectedAction]();
        }

        public void LoadMenuAction(string menuTag)
        {
            int id = -1;
            int.TryParse(menuTag, out id);
            SelectedAction = isValidAction(id) ? (MenuAction)id : Menu.MenuAction.INVALID;
        }

        private bool isValidAction(int id)
        {
            return id > 0 && id < countItems;
        }

        public string GetDescriptionOfSelectedAction()
        {
            FieldInfo fieldInfo = SelectedAction.GetType().GetField(SelectedAction.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
                return attributes[0].Description;
            else
                return SelectedAction.ToString();
        }

    }
}
