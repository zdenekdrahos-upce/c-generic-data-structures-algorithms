﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.ComponentModel;

namespace inpg_graf.Menu
{
    /// <summary>
    /// Výčet dostupných akcí v hlavním editoru. Každý akce má svoje ID
    /// a základní nápovědu k dané akci.
    /// </summary>
    enum MenuAction
    {
        [Description("Zvolili jste neplatnou akci")]
        INVALID = 0,
        [Description("Vkládejte nové uzly kliknutím do mapy")]
        NODE_ADD = 1,
        [Description("Posun uzlu - vyberte uzel a táhnutím se stisknutým tlačítkem myši ho přesuňte na novou pozici")]
        NODE_MOVE = 2,
        [Description("Odstranění uzlu pomocí kliknutí na uzel, který chcete smazat")]
        NODE_DELETE = 3,
        [Description("Zoomování kolečkem myši")]
        ZOOM = 4,
        [Description("Tahem po stisknutí tlačítka myši vymezte obdélníkovou oblast pro zoom")]
        ZOOM_RECT = 5,
        [Description("")]
        ZOOM_RESET = 6,
        [Description("Klikněte na počáteční uzel a poté táhněte hranu až ke koncovému uzlu")]
        EDGE_ADD = 7,
        [Description("Najeďte nad hranu a tahem ji přesuňte na novou pozici")]
        EDGE_MOVE = 8,
        [Description("Najeďte nad hranu a klikem na ní ji smažete")]
        EDGE_DELETE = 9,
        [Description("Editace vrcholů a hran - klikněte na vrchol/hranu a zobrazí se nové dialogové okno pro editaci")]
        EDIT_NODE_EDGE = 10,
    }
}
