﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace inpg_graf.Menu
{
    /// <summary>
    /// Delegát pro metodu, která má být spuštěna po zvolené akce v menu.
    /// </summary>
    public delegate void MenuActionCallback();

}
