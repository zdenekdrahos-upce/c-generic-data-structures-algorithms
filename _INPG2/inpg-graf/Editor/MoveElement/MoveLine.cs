﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using System.Drawing;

namespace inpg_graf.Editor.MoveElement
{
    /// <summary>
    /// Pro přesun čáry, kde je čára definována počátečním a koncovým bodem.
    /// </summary>
    class MoveLine
    {

        private MovePoint startLinePoint;
        private MovePoint endLinePoint;

        public MoveLine()
        {
            startLinePoint = new MovePoint();
            endLinePoint = new MovePoint();
        }

        public bool IsMoving {
            get { return startLinePoint.IsMoving && endLinePoint.IsMoving; }
            private set {} 
        }

        public KeyValuePair<Point, Point> MovedLine
        {
            get { return new KeyValuePair<Point, Point>(startLinePoint.OldPosition, endLinePoint.OldPosition); }
            private set { }
        }

        public KeyValuePair<Point, Point> NewLine
        {
            get { return new KeyValuePair<Point, Point>(startLinePoint.NewPosition, endLinePoint.NewPosition); }
            private set { }
        }

        public void Down(Point startPoint, KeyValuePair<Point, Point> line)
        {
            startLinePoint.Down(startPoint, line.Key);
            endLinePoint.Down(startPoint, line.Value);
        }

        public void Move(Point currentPoint)
        {
            if (IsMoving)
            {
                startLinePoint.Move(currentPoint);
                endLinePoint.Move(currentPoint);
            }
        }

        public void Up(Point endPoint)
        {
            if (IsMoving)
            {
                startLinePoint.Up(endPoint);
                endLinePoint.Up(endPoint);
            }
        }

    }
}
