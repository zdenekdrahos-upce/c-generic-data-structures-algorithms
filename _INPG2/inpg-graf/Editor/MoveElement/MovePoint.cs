﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;

namespace inpg_graf.Editor.MoveElement
{
    /// <summary>
    /// Třída pro přesun bodu pomocí event: down, move a up.
    /// </summary>
    class MovePoint
    {
        private Point movedPointStartPosition;
        private Point movedPointCurrentPosition;
        private Point startMovePoint;

        public bool IsMoving { get; private set; }

        public Point OldPosition {
            get { return movedPointStartPosition; }
            private set { movedPointStartPosition = value; }
        }

        public Point NewPosition
        {
            get { return movedPointCurrentPosition; }
            private set { movedPointCurrentPosition = value; }
        }

        public void Down(Point startPoint, Point movedPoint)
        {
            IsMoving = true;
            OldPosition = movedPoint;
            NewPosition = movedPoint;
            startMovePoint = startPoint;
        }

        public void Move(Point currentPoint)
        {
            if (IsMoving)
                RecalculatePosition(currentPoint);
        }

        public void Up(Point endPoint)
        {
            if (IsMoving)
            {
                IsMoving = false;
                RecalculatePosition(endPoint);
            }
        }

        private void RecalculatePosition(Point mouseLocation)
        {
            int x = movedPointStartPosition.X + mouseLocation.X - startMovePoint.X;
            int y = movedPointStartPosition.Y + mouseLocation.Y - startMovePoint.Y;
            NewPosition = new Point(x, y);
        }

    }
}
