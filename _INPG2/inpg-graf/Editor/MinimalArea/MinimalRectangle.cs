﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using inpg_graf.Helpers;

namespace inpg_graf.Editor.MinimalArea
{
    /// <summary>
    /// Třída pro dynamické výpočty minimální pravoúhlé oblasti pro
    /// bod, bod(y) a jeho okolí.
    /// </summary>
    class MinimalRectangle
    {

        private int margin;
        private int minX, minY, maxX, maxY;
        public Rectangle Rectangle { get; private set; }

        public MinimalRectangle(int margin)
        {
            this.margin = margin;
            ResetClip();
        }

        public void ResetClip()
        {
            Rectangle = new Rectangle();
            minX = -1;
            minY = -1;
            maxX = 0;
            maxY = 0;
        }

        public void AddPoint(Point point, ZoomHelper zoom)
        {
            UpdateBounds(point);
            LoadRectangle(zoom);
        }

        private void UpdateBounds(Point point)
        {
            minX = minX == -1 ? point.X : Math.Min(minX, point.X);
            minY = minY == -1 ? point.Y : Math.Min(minY, point.Y);
            maxX = Math.Max(maxX, point.X);
            maxY = Math.Max(maxY, point.Y);
        }

        private void LoadRectangle(ZoomHelper zoom) 
        {
            Point start = new Point(minX - margin, minY - margin);
            double width = (maxX - minX + 2 * margin) * zoom.Zoom;
            double height = (maxY - minY + 2 * margin) * zoom.Zoom;
            Rectangle = new Rectangle(
                zoom.GetRelativePointLocation(start), 
                new Size((int)width, (int)height)
           );
        }

    }
}
