﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace inpg_graf.Editor.WinApi
{
    public enum PenStyles
    {
        PS_SOLID = 0,
        PS_DASH = 1, // -------
        PS_DOT = 2, // .......
        PS_DASHDOT = 3, // _._._._
        PS_DASHDOTDOT = 4, // _.._.._
        PS_NULL = 5,
        PS_INSIDEFRAME = 6,
        PS_USERSTYLE = 7,
        PS_ALTERNATE = 8
    }

}
