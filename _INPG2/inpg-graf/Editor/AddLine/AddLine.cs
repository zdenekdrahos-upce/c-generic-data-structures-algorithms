﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;
using inpg_graf.Editor.WinApi;

namespace inpg_graf.Editor.AddLine
{
    /// <summary>
    /// Zapouzdření pro vytvoření nové čáry. Průběžná čára je kreslená pomocí XOR kreslení XOR režimu. 
    /// Obsahuje základní metody pro eventy: down, move a up. Kreslení XOR pomocí Win API.
    /// </summary>
    class AddLine
    {

        public bool IsActive { get; private set; }
        public Point StartPoint { get; private set; }
        public Point EndPoint { get; private set; }

        public void Down(Point startLinePoint)
        {
            IsActive = true;
            StartPoint = startLinePoint;
            EndPoint = startLinePoint;
        }

        public void Move(Point currentPoint, Graphics graphics)
        {
            if (IsActive)
            {
                DrawLine(graphics);
                EndPoint = currentPoint;
                DrawLine(graphics);
            }
        }

        public void Up(Point endPoint, Graphics graphics)
        {
            if (IsActive)
            {
                DrawLine(graphics);
                IsActive = false;
                EndPoint = endPoint;
                if (IsLineValid())
                    return;
            }
            throw new InvalidLineException();
        }

        private bool IsLineValid()
        {
            return StartPoint.X != EndPoint.X || StartPoint.Y != EndPoint.Y;
        }

        private void DrawLine(Graphics graphics)
        {
            if (IsLineValid())
                using (WinApiGraphics g = new WinApiGraphics(graphics))
                    g.DrawXorLine(StartPoint, EndPoint);
        }
    }
}
