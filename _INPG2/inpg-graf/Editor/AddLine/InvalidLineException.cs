﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace inpg_graf.Editor.AddLine
{
    class InvalidLineException : InvalidOperationException
    {
    }
}
