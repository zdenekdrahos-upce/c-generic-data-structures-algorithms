﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace inpg_graf.Editor.ClosestElement
{
    /// <summary>
    /// Pro udržování hodnoty minimální vzdálenosti a také magnetismu k 
    /// možnosti nepřesného najetí např. nad bod.
    /// </summary>
    public class MinDistanceSearch
    {
        private double minDistance;
        private double magnetism;

        public MinDistanceSearch(double magnetism)
        {
            this.magnetism = magnetism;
        }

        public void PrepareSearch()
        {
            minDistance = double.MaxValue;
        }

        public bool IsMinimum(double distance)
        {
            if (distance <= magnetism && distance < minDistance)
            {
                minDistance = distance;
                return true;
            }
            return false;
        }

    }
}
