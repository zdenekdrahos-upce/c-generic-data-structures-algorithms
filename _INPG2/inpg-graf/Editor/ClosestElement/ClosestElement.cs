﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using System.Drawing;
using System;
using ZdenekDrahos.Graph;
using ZdenekDrahos.App.Model;

namespace inpg_graf.Editor.ClosestElement
{
    /// <summary>
    /// Univerzální hledač nejbližšího elementu vůči aktuální pozici (např. myši).
    /// Musí se inicializovat pomocí <see cref="MinDistanceSearch"/>, ve kterém se nastavuje
    /// magnetismus. A pomocí delegátu pro výpočet vzdálenosti mezi pozicí myši a
    /// zkoumaným elementem <typeparamref name="T"/>
    /// </summary>
    public class ClosestElement<T>
    {

        private T closestElement;
        private MinDistanceSearch minDistance;
        private DistanceCalculatorCallback<T> distanceCalculator;

        public ClosestElement(MinDistanceSearch minDistance, DistanceCalculatorCallback<T> distanceCalculator)
        {
            closestElement = default(T);
            Exists = false;
            this.minDistance = minDistance;
            this.distanceCalculator = distanceCalculator;
        }

        public bool Exists { get; private set; }

        public T Element
        {
            get
            {
                return closestElement;
            }
            private set
            {
                closestElement = value;
                Exists = true;
            }
        }

        public ClosestElement<T> Previous { get; private set; }

        public void FindClosestElement(Point mouseLocation, IEnumerable<T> availableElements)
        {
            SavePrevious();
            Exists = false;
            minDistance.PrepareSearch();
            foreach (T element in availableElements)
                if (IsClosestElement(mouseLocation, element))
                    Element = element;
        }

        public void SavePrevious() 
        {
            ClosestElement<T> clone = new ClosestElement<T>(minDistance, distanceCalculator);
            clone.Element = Element;
            clone.Exists = Exists;
            Previous = clone;
        }

        private bool IsClosestElement(Point mouseLocation, T element)
        {
            return minDistance.IsMinimum(distanceCalculator(mouseLocation, element));
        }

    }
}
