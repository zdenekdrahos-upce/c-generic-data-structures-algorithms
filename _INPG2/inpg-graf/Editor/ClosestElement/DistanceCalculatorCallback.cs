﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;

namespace inpg_graf.Editor.ClosestElement
{
    /// <summary>
    /// Delegát pro zjištění vzdáleností pro <see cref="ClosestElement"/>
    /// </summary>
    /// <param name="mouseLocation">aktuální pozice myši</param>
    /// <param name="element">element, ke kterému je měřena vzdálenost</param>
    /// <returns>Vzdálenost od pozice myši k elementu1</returns>
    public delegate double DistanceCalculatorCallback<T>(Point mouseLocation, T element);
}
