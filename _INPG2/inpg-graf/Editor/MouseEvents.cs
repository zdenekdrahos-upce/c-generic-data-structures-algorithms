﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using System.Windows.Forms;

namespace inpg_graf.Editor
{
    /// <summary>
    /// Pro udržování přidaných handleru pro události myši, aby se dalo při resetu
    /// snadno odstranit všechny nové handlery. Protože není tam snadné smazat všechny
    /// handlery, které jsou přidané k event.
    /// </summary>
    class MouseEvents
    {
        public IList<MouseEventHandler> MouseDown {get; private set; }
        public IList<MouseEventHandler> MouseMove { get; private set; }
        public IList<MouseEventHandler> MouseUp { get; private set; }
        public IList<MouseEventHandler> MouseWheel { get; private set; }

        public MouseEvents()
        {
            MouseDown = new List<MouseEventHandler>();
            MouseMove = new List<MouseEventHandler>();
            MouseUp = new List<MouseEventHandler>();
            MouseWheel = new List<MouseEventHandler>();
        }
    }
}
