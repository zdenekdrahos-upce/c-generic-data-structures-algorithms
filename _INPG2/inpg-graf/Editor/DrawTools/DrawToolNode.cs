﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;

namespace inpg_graf.Editor.DrawTools
{
    /// <summary>
    /// Nástroje, příp. nastavení pro vykreslování vrcholů
    /// </summary>
    public class DrawToolNode
    {

        public Brush Brush { get; private set; }
        public Pen Pen { get; private set; }
        public int Radius { get; private set; }
        
        public DrawToolNode(Brush brush, Color penColor)
        {
            Brush = brush;
            Pen = new Pen(penColor, 3);
            Radius = 4;
        }

        public int Diameter()
        {
            return 2 * Radius;
        }

    }
}
