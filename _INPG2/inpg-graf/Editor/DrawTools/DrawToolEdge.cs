﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;

namespace inpg_graf.Editor.DrawTools
{
    /// <summary>
    /// Nástroje, příp. nastavení pro vykreslování hran
    /// </summary>
    class DrawToolEdge
    {

        public Pen Pen { get; private set; }

        public DrawToolEdge(Color colorLine)
        {
            Pen = new Pen(colorLine, 2);
        }

    }
}
