﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;

namespace inpg_graf.Editor.DrawTools
{
    /// <summary>
    /// Definice nástrojů pro vykreslování vrcholů a hran.
    /// </summary>
    class DrawingTools
    {
        public DrawToolNode NodeDefault { get; private set; }
        public DrawToolNode NodeBusStop { get; private set; }
        public DrawToolNode NodeRestArea { get; private set; }
        public DrawToolNode NodeClosest { get; private set; }

        public DrawToolEdge EdgeDefault { get; private set; }
        public DrawToolEdge EdgeClosest { get; private set; }

        public DrawingTools()
        {
            LoadToolsForNodes();
            LoadToolsForEdges();
        }

        private void LoadToolsForNodes()
        {
            NodeDefault = new DrawToolNode(Brushes.LightGray, Color.Black);
            NodeBusStop = new DrawToolNode(Brushes.Yellow, Color.Black);
            NodeRestArea = new DrawToolNode(Brushes.LightGreen, Color.Black);
            NodeClosest = new DrawToolNode(Brushes.Black, Color.Red);
        }

        private void LoadToolsForEdges()
        {
            EdgeDefault = new DrawToolEdge(Color.Blue);
            EdgeClosest = new DrawToolEdge(Color.Red);
        }


    }
}
