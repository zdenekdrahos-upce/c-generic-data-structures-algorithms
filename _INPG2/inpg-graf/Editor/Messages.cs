﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Windows.Forms;

namespace inpg_graf.Editor
{
    /// <summary>
    /// Třída pro informační hlášky, aby styl hlášek (ikonky, titulky oken) byl
    /// v celé aplikaci jednotný
    /// </summary>
    static class Messages
    {

        public static void Information(string message)
        {
            if (message.Length > 0)
                MessageBox.Show(message, "Informace", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void Error(string message)
        {
            if (message.Length > 0)
                MessageBox.Show(message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static bool QuestionYesNo(string message)
        {
            DialogResult result = MessageBox.Show(message, "Otázka?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            return result == DialogResult.Yes;
        }

    }
}
