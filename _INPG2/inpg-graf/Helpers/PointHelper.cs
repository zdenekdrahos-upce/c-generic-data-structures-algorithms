﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph;

namespace inpg_graf.Helpers
{
    /// <summary>
    /// Pro výpočty vzdáleností mezi body, příp. body a jinými útvary (úsečka apod.)
    /// </summary>
    class PointHelper
    {

        public static double CrossRoadDistance(Point mouseLocation, ICrossRoad element)
        {
            return PointHelper.GetDistancePoints(mouseLocation, element.Point);
        }

        public static double GetDistancePoints(Point start, Point end)
        {
            return Math.Sqrt(Math.Pow(end.X - start.X, 2) + Math.Pow(end.Y - start.Y, 2));
        }

        public static double GetDistancePointLine(Point point, Edge<string, ICrossRoad, double> line)
        {
            return GetDistancePointLine(point, line.StartNode.Point, line.EndNode.Point);
        }

        public static double GetDistancePointLine(Point point, KeyValuePair<Point, Point> line)
        {
            return GetDistancePointLine(point, line.Key, line.Value);
        }

        public static double GetDistancePointLine(Point point, Point startLine, Point endLine)
        {
            int A = point.X - startLine.X;
            int B = point.Y - startLine.Y;
            int C = endLine.X - startLine.X;
            int D = endLine.Y - startLine.Y;

            double dot = A * C + B * D;
            double len_sq = C * C + D * D;
            double param = dot / len_sq;

            PointF linePoint;
            if (param < 0)
                linePoint = startLine;
            else if (param > 1)
                linePoint = endLine;
            else
                linePoint = new PointF(
                    (float)(startLine.X + param * C),
                    (float)(startLine.Y + param * D)
                );
            return GetDistancePoints(point, Point.Round(linePoint));
        }

    }
}
