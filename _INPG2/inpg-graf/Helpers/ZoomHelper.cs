﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace inpg_graf.Helpers
{
    /// <summary>
    /// Pomocník pro zoomování obrazu, kde zoom je řešen pomocí zvětšování 
    /// oblasti (úprava Size) a posunu pozice oblasti (změna Location)
    /// </summary>
    public class ZoomHelper
    {
        public double Zoom { get; private set; }
        public Size WindowSize { get; private set; }
        public Size ActualSize { get; private set; }
        public event EventHandler ZoomChanged;

        private Point topLeftCorner; // if Location: [0,0]
        private double increase;

        public ZoomHelper(double increase)
        {
            Zoom = 1;
            this.increase = increase;
            topLeftCorner = new Point();
        }

        public void SetWindowSize(Size size)
        {
            WindowSize = size;
        }

        public void ResizeWindow(Size newActualSize)
        {
            topLeftCorner = new Point();
            double newZoom = 1;
            if (newActualSize.Width > ActualSize.Width || newActualSize.Height > ActualSize.Height)
                newZoom = Math.Max(
                    newActualSize.Width / (double)WindowSize.Width,
                    newActualSize.Height / (double)WindowSize.Height
                );
            else
                newZoom = Math.Min(
                    (ActualSize.Width) / (double)newActualSize.Width,
                    (ActualSize.Height) / (double)newActualSize.Height
                );
            if (newZoom == Zoom || newZoom < 1)
                return;
            Zoom = newZoom;
            ZoomChanged(this, EventArgs.Empty);
        }

        public void AutoZoom(int delta)
        {
            if (delta > 0)
                ZoomIn();
            else
                ZoomOut();
            ZoomChanged(this, EventArgs.Empty);
        }

        private void ZoomIn()
        {
            Zoom += increase;
        }

        private void ZoomOut()
        {
            Zoom -= increase;
            if (!IsZoomActive())
                ResetZoomValues();
        }

        public bool IsZoomActive()
        {
            return Zoom > 1;
        }

        public void ResetZoom()
        {
            ResetZoomValues();
            ZoomChanged(this, EventArgs.Empty);
        }

        private void ResetZoomValues()
        {
            topLeftCorner = new Point();
            Zoom = 1;        
        }

        public void ZoomRect(Rectangle zoomRect)
        {
            Zoom = Math.Min(
                WindowSize.Width / (double)zoomRect.Width,
                WindowSize.Height / (double)zoomRect.Height
            );
            ZoomChanged(this, EventArgs.Empty);
        }

        public void UpdateSize()
        {
            ActualSize = new Size(
                (int)(WindowSize.Width * Zoom),
                (int)(WindowSize.Height * Zoom)
            );
        }

        public void SetTopLeftCornerZoomRect(Point rectTopLeftPoint)
        {
            int scrollX = (int)(rectTopLeftPoint.X * (Zoom - increase) - topLeftCorner.X);
            int scrollY = (int)(rectTopLeftPoint.Y * (Zoom - increase) - topLeftCorner.Y);            
            if (scrollX + WindowSize.Width > ActualSize.Width)
                scrollX = ActualSize.Width - WindowSize.Width;
            if (scrollY + WindowSize.Height > ActualSize.Height)
                scrollY = ActualSize.Height - WindowSize.Height;
            topLeftCorner = new Point(scrollX, scrollY);
        }

        public void SetTopLeftCornerMouseWheel(Point mys)
        {
            topLeftCorner = IsZoomActive() ? GetNewTopLeftCorner(mys) : new Point();
        }

        private Point GetNewTopLeftCorner(Point mouseLocation)
        {
            int diffMouseX = mouseLocation.X - topLeftCorner.X;
            int diffMouseY = mouseLocation.Y - topLeftCorner.Y;
            // nova pozice mysi - abych měl správný zoom, tak nutno odečíst aktuální polohu rohu
            Point zoomedMouseLocation = new Point(
                (int)(diffMouseX * Zoom),
                (int)(diffMouseY * Zoom)
            );
            // výpočet relativní vzdálenosti pro klasické okno
            double percentageX = diffMouseX / (double)WindowSize.Width;
            double percentageY = diffMouseY / (double)WindowSize.Height;            
            // kontrola, zda nepretece - muze nastat chyba, pokud rychle zoom a nestihne se prekreslit
            if (percentageX > 1 || percentageX < 0 || percentageY > 1 || percentageY < 0)
                return new Point();
            return new Point(Math.Abs(zoomedMouseLocation.X - diffMouseX), Math.Abs(zoomedMouseLocation.Y - diffMouseY));
        }

        public Point GetLocation()
        {
            return new Point(-topLeftCorner.X, -topLeftCorner.Y);
        }

        public PointF GetRealPointLocation(Point mouseLocation)
        {
            return new PointF(mouseLocation.X / (float)Zoom, mouseLocation.Y / (float)Zoom);
        }

        public Point GetRelativePointLocation(PointF nodePosition)
        {
            return GetRelativePointLocation(Point.Round(nodePosition));
        }

        public Point GetRelativePointLocation(Point nodePosition)
        {
            return new Point(
                (int)(nodePosition.X * Zoom),
                (int)(nodePosition.Y * Zoom)
            );
        }

    }
}
