﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;
using ZdenekDrahos.Graph;
using ZdenekDrahos.App.Model;

namespace inpg_graf.Helpers
{
    /// <summary>
    /// Pro specifický převod na řetezěc, který je pak v aplikaci 
    /// používán pro nějaký výpis.
    /// </summary>
    static class StringFormatter
    {

        public static string ZoomToString(double zoom)
        {
            return zoom == 1 ? "1x" : string.Format("{0:0.00}x", zoom);
        }

        public static string ActualPositionToString(PointF point)
        {
            return string.Format("Poloha: [{0:0.00};{1:0.00}]", point.X, point.Y);
        }

        public static string FormatEdge(Edge<string, ICrossRoad, double> edge)
        {
            return string.Format("{0} -> {1} ({2:0})", edge.StartNode, edge.EndNode, edge.EdgeCost);
        }

    }
}
