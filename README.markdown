# C# Generic Data Structures & Algorithms

## About

Project developed as semestral project in [Data Structures and Algorithms](http://ects.upce.cz/predmet/KST/INDSA?lang=en&rocnik=1) at [University of Pardubice](http://www.upce.cz/en/index.html).

* **Graph** - undirected graph.
* **Shortest Path** - find the shortest path in graph with Dijkstra algorithm or Floyd Matrix.
* **Table** - associative array, implementations with hash table, linked list, table in external memory.
* **Priority Queue** - implemented with PairingHeap.
* **Binary Tree** - basic tree data structure.
* **Binary Search Tree** - ordered binary tree.
* **Range Tree** - available 1D and 2D Range Tree.
* **Red Black Tree** - self-balancing binary search tree used as index structure for block files.
* **Block Files** - block oriented files, heap file or sorted file, used for table in external memory
* **Search Algorithms** - binary and interpolation search.

## "Libraries"

* [Mark Allen Weiss](http://users.cis.fiu.edu/~weiss/dsj2/code/weiss/nonstandard/PairingHeap.java) - pairing heap implementation
* [LiteratePrograms](http://en.literateprograms.org/Red-black_tree_(Java)) - red-black tree implementation
    
## Facts

License: [New BSD License](https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms/src/tip/license.txt).

Author: [Zdenek Drahos](https://bitbucket.org/zdenekdrahos).
