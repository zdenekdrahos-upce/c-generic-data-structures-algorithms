﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Table;

namespace Tests
{
    class Equal
    {
        private bool worksEqual;
        private string text;

        public Equal(string text, bool worksEqual)
        {
            this.text = text;
            this.worksEqual = worksEqual;
        }

        public override bool Equals(object obj)
        {
            Equal b = obj as Equal;
            return worksEqual ? text.Equals(b.text) : false;
        }

        public override int GetHashCode()
        {
            return 1;
        }

    }


    [TestClass()]
    public class HashTableCollisionTest
    {

        private HashTable<Equal, string> hashTable = new HashTable<Equal, string>();

        [TestMethod()]
        public void TestWorkingEqual()
        {
            hashTable.Add(new Equal("A", true), "A");
            hashTable.Add(new Equal("a", true), "a");

            Assert.AreEqual(2, hashTable.Count);

            Assert.AreEqual("a", hashTable.Remove(new Equal("a", true)));
            Assert.AreEqual(1, hashTable.Count);

            Assert.AreEqual("A", hashTable.Remove(new Equal("A", true)));
            Assert.AreEqual(0, hashTable.Count);
        }

        [TestMethod()]
        public void TestInvalidEqualMethod()
        {
            // insert will be OK, even if they have equal hash
            hashTable.Add(new Equal("A", false), "A");
            hashTable.Add(new Equal("a", false), "a");

            Assert.AreEqual(2, hashTable.Count);

            // but if equal method is not working, then elements cannot be found
            try
            {
                hashTable.Remove(new Equal("a", false));
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }
    }
}
