﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Table.ExternalMemory;

namespace Tests
{

    [TestClass()]
    public class FileSortedTableTest
    {
        
        private IFileSortedTable<int, Point> table;

        public FileSortedTableTest()
        {
            InitTable();
        }

        private void InitTable()
        {
            table = new FileSortedTable<int, Point>("tempTest.bin", 5, 2000);
            table.KeyNumberConverter = (key) => (double)key;
        }

        [TestMethod()]
        public void AddTest()
        {
            AssertEmptyTable();
            table.Build(new KeyValuePair<int, Point>[] { new KeyValuePair<int, Point>(0, Point.Empty) });
            Assert.AreEqual(1, table.Count);
            Assert.IsFalse(table.IsEmpty());
            table.Clear();
            AssertEmptyTable();
        }

        private void AssertEmptyTable()
        {
            Assert.AreEqual(0, table.Count);
            Assert.IsTrue(table.IsEmpty());
        }

        [TestMethod()]
        public void FindBinarySearchTest()
        {
            BuildDefaultTable();
            for (int i = 0; i < table.Count; i++)
                AssertPoints(new Point(i, i), table.FindBinarySearch(i));
        }

        [TestMethod()]
        public void FindInterpolationSearchTest()
        {
            BuildDefaultTable();
            for (int i = 0; i < table.Count; i++)
                AssertPoints(new Point(i, i), table.FindInterpolationSearch(i));
        }

        private static void AssertPoints(Point expected, Point found)
        {
            Assert.AreEqual(expected.X, found.X);
            Assert.AreEqual(expected.Y, found.Y);
        }

        [TestMethod()]
        public void RemoveTest()
        {
            BuildDefaultTable();
            int tableCount = table.Count;
            for (int i = 0; i < tableCount; i++)
                table.Remove(i);
            AssertEmptyTable();
        }

        [TestMethod()]
        public void RemoveAndInterpolationTest()
        {
            BuildDefaultTable();
            table.Remove(50);
            AssertUnexistingKey(50, table.FindBinarySearch);
            AssertUnexistingKey(50, table.FindInterpolationSearch);
        }

        [TestMethod()]
        public void DuplicateKeysTest()
        {
            KeyValuePair<int, Point>[] input = {
                new KeyValuePair<int, Point>(1, Point.Empty),
                new KeyValuePair<int, Point>(1, Point.Empty),
            };
            try
            {
                table.Clear();
                table.Build(input);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                // exception raised when input is sorted via adding to SortedSet
                Assert.IsTrue(ex is ArgumentException);
            }            
        }

        [TestMethod()]
        public void UnexistingKeyTest()
        {
            BuildDefaultTable();
            int[] unexistingKeys = { 10000, -10000, -1 };
            for (int i = 0; i < unexistingKeys.Length; i++)
            {
                AssertUnexistingKey(unexistingKeys[i], table.FindBinarySearch);
                AssertUnexistingKey(unexistingKeys[i], table.FindInterpolationSearch);
                AssertUnexistingKey(unexistingKeys[i], table.Remove);
            }
        }

        private void AssertUnexistingKey(int key, Func<int, Point> tableMethod)
        {
            try
            {
                tableMethod(key);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        private void BuildDefaultTable()
        {
            InitTable();
            table.Clear();
            List<KeyValuePair<int, Point>> input = new List<KeyValuePair<int, Point>>(500);
            for (int i = 0; i < 500; i++)
                input.Add(new KeyValuePair<int, Point>(i, new Point(i, i)));
            table.Build(input);
        }
        
    }
}
