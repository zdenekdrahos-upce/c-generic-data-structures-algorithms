﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Table;

namespace Tests
{

    [TestClass()]
    public class ListTableTest
    {

        private ListTable<string, string> stringTable = new ListTable<string, string>();        

        [TestMethod()]
        public void FindTest()
        {
            stringTable.Add("test", "test");
            Assert.AreEqual("test", stringTable.Find("test"));

            try 
            {
                stringTable.Find("tefsaf");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        [TestMethod()]
        public void ClearTest()
        {
            Assert.IsTrue(stringTable.IsEmpty());
            stringTable.Add("test", "test");
            Assert.IsFalse(stringTable.IsEmpty());
            stringTable.Clear();
            Assert.IsTrue(stringTable.IsEmpty());
        }

        [TestMethod()]
        public void AddTest()
        {
            Assert.AreEqual(0, stringTable.Count);
            stringTable.Add("test", "test");
            Assert.AreEqual(1, stringTable.Count);

            try
            {
                stringTable.Add("test", "same key");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }

        [TestMethod()]
        public void IsEmptyTest()
        {
            Assert.IsTrue(stringTable.IsEmpty());
            stringTable.Add("test", "test");
            Assert.IsFalse(stringTable.IsEmpty());
        }

        [TestMethod()]
        public void RemoveTest()
        {
            stringTable.Add("test", "test");
            Assert.AreEqual("test", stringTable.Remove("test"));

            try
            {
                stringTable.Remove("tefsaf");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        [TestMethod()]
        public void GetEnumeratorTest()
        {
            stringTable.Add("1", "1");
            stringTable.Add("2", "2");
            stringTable.Add("3", "3");

            int startNumber = 1;
            string number;
            foreach (KeyValuePair<string, string> item in stringTable)
            {
                number = startNumber++.ToString();
                Assert.AreEqual(number, item.Key);
                Assert.AreEqual(number, item.Value);
            }
        }

        [TestMethod()]
        public void KeysTest()
        {
            stringTable.Add("1", "1");
            stringTable.Add("2", "2");
            stringTable.Add("3", "3");
            Assert.AreEqual(3, stringTable.Keys.Count);
            for (int i = 1; i <= 3; i++)
            {
                Assert.IsTrue(stringTable.Keys.Contains(i.ToString()));
            }   
        }
    }
}
