﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Table;

namespace Tests
{
    
    [TestClass()]
    public class HashTableTest
    {

        private HashTable<string, string> hashTable = new HashTable<string, string>();

        [TestMethod()]
        public void FindTest()
        {
            hashTable.Add("test", "test");
            Assert.AreEqual("test", hashTable.Find("test"));

            try
            {
                hashTable.Find("tefsaf");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        [TestMethod()]
        public void ClearTest()
        {
            Assert.IsTrue(hashTable.IsEmpty());
            hashTable.Add("test", "test");
            Assert.IsFalse(hashTable.IsEmpty());
            hashTable.Clear();
            Assert.IsTrue(hashTable.IsEmpty());
        }

        [TestMethod()]
        public void AddTest()
        {
            Assert.AreEqual(0, hashTable.Count);
            hashTable.Add("test", "test");
            Assert.AreEqual(1, hashTable.Count);

            try
            {
                hashTable.Add("test", "same key");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }

        [TestMethod()]
        public void IsEmptyTest()
        {
            Assert.IsTrue(hashTable.IsEmpty());
            hashTable.Add("test", "test");
            Assert.IsFalse(hashTable.IsEmpty());
        }

        [TestMethod()]
        public void RemoveTest()
        {
            hashTable.Add("test", "test");
            Assert.AreEqual("test", hashTable.Remove("test"));

            try
            {
                hashTable.Remove("tefsaf");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        [TestMethod()]
        public void GetEnumeratorTest()
        {
            hashTable.Add("1", "1");
            hashTable.Add("2", "2");
            hashTable.Add("3", "3");

            int startNumber = 1;
            string number;
            foreach (KeyValuePair<string, string> item in hashTable)
            {
                number = startNumber++.ToString();
                Assert.AreEqual(number, item.Key);
                Assert.AreEqual(number, item.Value);
            }
        }

        [TestMethod()]
        public void KeysTest()
        {
            hashTable.Add("1", "1");
            hashTable.Add("2", "2");
            hashTable.Add("3", "3");
            Assert.AreEqual(3, hashTable.Keys.Count);
            for (int i = 1; i <= 3; i++)
			{
			    Assert.IsTrue(hashTable.Keys.Contains(i.ToString()));
			}            
        }
    }
}
