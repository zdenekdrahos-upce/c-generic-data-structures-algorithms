﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.PriorityQueue;

namespace Tests
{
    
    [TestClass()]
    public class PriorityQueueElementTest
    {

        private PriorityQueueElement<int, int> element;

        [TestMethod()]
        public void PriorityQueueElementConstructorTest()
        {
            element = new PriorityQueueElement<int, int>(5);
            Assert.AreEqual(5, element.Key);
            Assert.AreEqual(5, element.Value);
        }

        [TestMethod()]
        public void PriorityQueueElementInvalidConstructorTest()
        {
            try
            {
                // value cannot be cast to int
                new PriorityQueueElement<int, string>("Hello World");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }

        [TestMethod()]
        public void PriorityQueueElementConstructorTest1()
        {
            element = new PriorityQueueElement<int, int>(5, 5);
            Assert.AreEqual(5, element.Key);
            Assert.AreEqual(5, element.Value);
        }
    }
}
