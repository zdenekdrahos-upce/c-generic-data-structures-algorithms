﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.PriorityQueue;
using ZdenekDrahos.PriorityQueue.PairingHeap;

namespace Tests
{
    
    [TestClass()]
    public class PriorityQueueTest
    {
        private IPriorityQueue<int, int> priorityQueue = new PairingHeap<int, int>();

        [TestMethod()]
        public void ClearTest()
        {            
            priorityQueue.Enqueue(1);
            Assert.IsFalse(priorityQueue.IsEmpty());
            priorityQueue.Clear();
            Assert.IsTrue(priorityQueue.IsEmpty());
        }

        [TestMethod()]
        public void DequeueTest()
        {
            try
            {
                priorityQueue.Dequeue();
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is InvalidOperationException);
            }

            priorityQueue.Enqueue(1);
            Assert.AreEqual(1, priorityQueue.Dequeue());
            Assert.AreEqual(0, priorityQueue.Count);
        }

        [TestMethod()]
        public void EqualKeysEnqueueTest()
        {
            try
            {
                priorityQueue.Enqueue(1);
                Assert.AreEqual(1, priorityQueue.Count);
                priorityQueue.Enqueue(1);
                Assert.AreEqual(2, priorityQueue.Count);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentException);// duplicite
            }            
        }

        [TestMethod()]
        public void IsEmptyTest()
        {
            Assert.IsTrue(priorityQueue.IsEmpty());
            priorityQueue.Enqueue(1);
            Assert.IsFalse(priorityQueue.IsEmpty());
        }


        [TestMethod()]
        public void MaxTest()
        {
            try
            {
                priorityQueue.Max();
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is InvalidOperationException);
            }

            priorityQueue.Enqueue(1);
            Assert.AreEqual(1, priorityQueue.Max());
            Assert.AreEqual(1, priorityQueue.Count);
        }

        [TestMethod()]
        public void EnqueueTest()
        {
            priorityQueue.Enqueue(1);
            priorityQueue.Enqueue(3);
            priorityQueue.Enqueue(2);
            // default order by the highest number
            Assert.AreEqual(3, priorityQueue.Count);
            Assert.AreEqual(1, priorityQueue.Dequeue());
            Assert.AreEqual(2, priorityQueue.Dequeue());
            Assert.AreEqual(3, priorityQueue.Dequeue());
            Assert.AreEqual(0, priorityQueue.Count);
        }

        [TestMethod()]
        public void ChangePriorityTest()
        {
            priorityQueue.Enqueue(1);
            priorityQueue.Enqueue(3);
            priorityQueue.Enqueue(2);
            // order should be 1 -> 2 -> 3
            priorityQueue.ChangePriority(0, 3);
            // now order should be 3 -> 1 -> 2
            Assert.AreEqual(3, priorityQueue.Dequeue());
            Assert.AreEqual(1, priorityQueue.Dequeue());
            Assert.AreEqual(2, priorityQueue.Dequeue());

            // unexisting element in queue
            try
            {
                priorityQueue.ChangePriority(1, 5656);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }
    }
}
