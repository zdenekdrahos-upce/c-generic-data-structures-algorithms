﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Tree.Tree23;
using ZdenekDrahos.Tree.Tree23.RedBlack;

namespace Tests
{
    
    [TestClass()]
    public class RedBlackTreeTest
    {

        private const int numberOfItems = 500;
        private ITree23<int, int> tree = new RedBlackTree<int, int>();

        [TestMethod()]
        public void ClearTest()
        {
            AssertEmptyTree();
            tree.Add(1, 1);
            Assert.AreEqual(1, tree.Count);
            tree.Clear();
            AssertEmptyTree();
        }

        [TestMethod()]
        public void AddTest()
        {
            BuildDefaultTree();
            Assert.AreEqual(numberOfItems, tree.Count);
        }

        [TestMethod()]
        public void FindTest()
        {
            BuildDefaultTree();
            for (int i = 0; i < numberOfItems; i++)
                Assert.AreEqual(i, tree.Find(i));
        }

        [TestMethod()]
        public void RemoveTest()
        {
            AssertEmptyTree();
            BuildDefaultTree();
            for (int i = 0; i < numberOfItems; i++)
            {
                Assert.AreEqual(numberOfItems - i, tree.Count);
                Assert.AreEqual(i, tree.Remove(i));                
            }
            AssertEmptyTree();
        }

        [TestMethod()]
        public void VerifyTreeTest()
        {
            tree.Clear();
            RedBlackTreeVerifier<int, int> verifier = new RedBlackTreeVerifier<int, int>();
            // verify EMPTY TREE
            AssertEmptyTree();
            tree.VerifyTree(verifier);
            // verify INSERT
            for (int i = 0; i < numberOfItems; i++)
            {
                tree.Add(i, i);
                tree.VerifyTree(verifier);
            }
            // verify REMOVE
            for (int i = 0; i < numberOfItems; i++)
            {
                tree.Remove(i);
                tree.VerifyTree(verifier);
            }
            AssertEmptyTree();
        }

        private void AssertEmptyTree()
        {
            Assert.AreEqual(0, tree.Count);
            Assert.IsTrue(tree.IsEmpty());
        }

        private void BuildDefaultTree()
        {
            tree.Clear();
            for (int i = 0; i < numberOfItems; i++)
                tree.Add(i, i);
        }

    }
}
