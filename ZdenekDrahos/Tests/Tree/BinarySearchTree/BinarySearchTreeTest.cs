﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Tree.BinarySearchTree;

namespace Tests
{

    [TestClass()]
    public class BinarySearchTreeTest
    {

        private IBinarySearchTree<int, int> binarySearchTree;
        private int[] nodes = new int[] { 60, 80, 90, 85, 100, 150, 200, 350, 300, 400 };

        public BinarySearchTreeTest()
        {
            binarySearchTree = new BinarySearchTree<int, int>();
        }

        [TestMethod()]
        public void RootTest()
        {
            binarySearchTree.Clear();
            binarySearchTree.Add(10, 10);
            Assert.AreEqual(10, binarySearchTree.Find(10));
            Assert.AreEqual(10, binarySearchTree.FindClosestLeaf(10));
        }

        [TestMethod()]
        public void InsertDuplicatesTest()
        {
            try
            {
                binarySearchTree.Add(10, 10);
                binarySearchTree.Add(10, 10);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(ArgumentException));
            }
        }

        [TestMethod()]
        public void FindTest()
        {            
            BuildDefaultTree();
            foreach (int node in nodes)
                Assert.AreEqual(node, binarySearchTree.Find(node));
        }

        [TestMethod()]
        public void FindUnexistingNodeTest()
        {
            try
            {
                BuildDefaultTree();
                binarySearchTree.Find(1000);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(KeyNotFoundException));
            }
        }

        [TestMethod()]
        public void FindClosestLeafTest()
        {
            int[] leafs = new int[] { 60, 85, 150, 300, 400 };
            BuildDefaultTree();
            foreach (int node in leafs)
                Assert.AreEqual(node, binarySearchTree.FindClosestLeaf(node));

            Assert.AreEqual(60, binarySearchTree.FindClosestLeaf(40));
            Assert.AreEqual(85, binarySearchTree.FindClosestLeaf(80));
            Assert.AreEqual(85, binarySearchTree.FindClosestLeaf(99));
            Assert.AreEqual(150, binarySearchTree.FindClosestLeaf(130));
            Assert.AreEqual(400, binarySearchTree.FindClosestLeaf(500));
        }

        [TestMethod()]
        public void RemoveTest()
        {
            BuildDefaultTree();
            foreach (int node in nodes)
            {
                Assert.AreEqual(node, binarySearchTree.Remove(node));
            }
            Assert.AreEqual(0, binarySearchTree.Count);
        }

        [TestMethod()]
        public void BreadthFirstSearchTest()
        {
            BuildDefaultTree();
            IDictionary<int, int[]> dict = new Dictionary<int, int[]>();
            dict.Add(100, new int[] { 100, 80, 200, 60, 90, 150, 350, 85, 300, 400 });
            dict.Add(80, new int[] { 80, 60, 90, 85 });
            dict.Add(90, new int[] { 90, 85 });
            dict.Add(85, new int[] { 85 });
            int index;
            foreach (KeyValuePair<int, int[]> item in dict)
            {
                index = 0;
                foreach (int value in binarySearchTree.BreadthFirstSearch(item.Key))
                    Assert.AreEqual(item.Value[index++], value);
            }
            // test root
            index = 0;
            foreach (int value in binarySearchTree.BreadthFirstSearch())
                Assert.AreEqual(dict[100][index++], value);
        }

        [TestMethod()]
        public void TestBfsUnexistingNode()
        {
            try
            {
                BuildDefaultTree();
                int test;
                foreach (int item in binarySearchTree.BreadthFirstSearch(5000))
                    test = item;
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(KeyNotFoundException));
            }
        }

        [TestMethod()]
        public void KeysBreadthFirstSearchTest()
        {
            BuildDefaultTree();
            IDictionary<int, int[]> dict = new Dictionary<int, int[]>();
            dict.Add(100, new int[] { 100, 80, 200, 60, 90, 150, 350, 85, 300, 400 });
            dict.Add(80, new int[] { 80, 60, 90, 85 });
            dict.Add(90, new int[] { 90, 85 });
            dict.Add(85, new int[] { 85 });
            int index;
            foreach (KeyValuePair<int, int[]> item in dict)
            {
                index = 0;
                foreach (int value in binarySearchTree.KeysBreadthFirstSearch(item.Key))
                    Assert.AreEqual(item.Value[index++], value);
            }
            // test root
            index = 0;
            foreach (int value in binarySearchTree.KeysBreadthFirstSearch())
                Assert.AreEqual(dict[100][index++], value);
        }

        private void BuildDefaultTree()
        {
            binarySearchTree.Clear();
            // root
            binarySearchTree.Add(100, 100);
            // left branch
            binarySearchTree.Add(80, 80);
            binarySearchTree.Add(60, 60);
            binarySearchTree.Add(90, 90);
            binarySearchTree.Add(85, 85);
            // right branch
            binarySearchTree.Add(200, 200);
            binarySearchTree.Add(150, 150);
            binarySearchTree.Add(350, 350);
            binarySearchTree.Add(300, 300);
            binarySearchTree.Add(400, 400);
        }

    }
}
