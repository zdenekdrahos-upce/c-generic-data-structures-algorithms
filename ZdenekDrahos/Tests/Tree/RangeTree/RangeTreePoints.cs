﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    public class RangeTreePoints
    {
        public static int GetCoordinateX(Point point)
        {
            return point.X;
        }

        public static int GetCoordinateY(Point point)
        {
            return point.Y;
        }

        public static void AssertOnePointFound(Point expected, IList<Point> foundPoints)
        {
            Assert.AreEqual(1, foundPoints.Count);
            AssertPoints(expected, foundPoints.First());
        }

        public static void AssertIntervalSearch(Point[] inputPoints, int startIndex, int length, IList<Point> foundPoints)
        {
            Point[] expectedPoints = new Point[length];
            Array.Copy(inputPoints, startIndex, expectedPoints, 0, length);
            RangeTreePoints.AssertMultiplePointsFound(expectedPoints, foundPoints);
        }

        public static void AssertMultiplePointsFound(IEnumerable<Point> expectedPoints, IList<Point> foundPoints)
        {
            Assert.AreEqual(expectedPoints.Count(), foundPoints.Count);
            for (int i = 0; i < foundPoints.Count; i++)
                AssertPoints(expectedPoints.ElementAt(i), foundPoints.ElementAt(i));
        }

        private static void AssertPoints(Point expected, Point found)
        {
            Assert.AreEqual(expected.X, found.X);
            Assert.AreEqual(expected.Y, found.Y);
        }

        public static Point[] GetDefaultPoints()
        {
            return new Point[] {
                new Point(5,5),//0
                new Point(15,45),//1
                new Point(22,60),//2
                new Point(28,50),//3
                new Point(33,63),//4
                new Point(38,52),//5
                new Point(42,65),//6
                new Point(47,35),//7
            };
        }

        public static Point[] GetDuplicatePoints()
        {
            return new Point[] {
                new Point(5,5),new Point(5,5),
                new Point(7,7),
                new Point(40,40),new Point(40,40),new Point(40,40),
                new Point(47,47),new Point(47,47),
                new Point(65,65),
            };
        }

        public static Point[] GetPointsForComplexTest()
        {
            Point[] points = new Point[100];
            for (int i = 1; i <= points.Length; i++)
                points[i - 1] = new Point(i, i);
            return points;
        }

    }
}
