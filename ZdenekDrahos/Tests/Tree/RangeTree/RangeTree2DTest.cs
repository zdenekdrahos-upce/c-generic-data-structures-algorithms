﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Tree.RangeTree;
using ZdenekDrahos.Tree.RangeTree._2D;

namespace Tests
{
    [TestClass()]
    public class RangeTree2DTest
    {

        private IRangeTree2D<int, Point> rangeTree;
        private Point[] defaultPoints, pointDuplicates;

        public RangeTree2DTest()
        {
            rangeTree = new RangeTree2D<int, Point>(RangeTreePoints.GetCoordinateX, RangeTreePoints.GetCoordinateY);
            defaultPoints = RangeTreePoints.GetDefaultPoints();
            pointDuplicates = RangeTreePoints.GetDuplicatePoints();
            rangeTree.Build(defaultPoints);
        }

        [TestMethod()]
        public void FindTest()
        {
            // test points
            TestAllPoints(defaultPoints);
            // invalid key
            try
            {
                rangeTree.Find(100, 10050);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(KeyNotFoundException));
            }
        }

        [TestMethod()]
        public void ComplexFindTest()
        {
            // init points <1,100>
            Point[] tempPoints, points = RangeTreePoints.GetPointsForComplexTest();
            // test points, where array length is from <1,100>
            for (int i = 1; i <= points.Length; i++)
            {
                tempPoints = new Point[i];
                Array.Copy(points, 0, tempPoints, 0, i);
                rangeTree.Build(tempPoints);
                TestAllPoints(tempPoints);
            }
        }

        private void TestAllPoints(Point[] points)
        {
            for (int i = 0; i < points.Length; i++)
                RangeTreePoints.AssertOnePointFound(points[i], rangeTree.Find(
                    rangeTree.GetKeyX(points[i]),
                    rangeTree.GetKeyY(points[i])
                ));
        }

        [TestMethod()]
        public void IntervalSearchTest()
        {
            // min = [35,50] a max = [45,70]
            RangeTreePoints.AssertIntervalSearch(defaultPoints, 5, 2, rangeTree.IntervalSearch(35, 50, 45, 70));
            // min = [20,50] a max = [30,60]
            Point[] path2 = new Point[] { defaultPoints[3], defaultPoints[2] };
            RangeTreePoints.AssertIntervalSearch(path2, 0, path2.Length, rangeTree.IntervalSearch(20, 50, 30, 60));
            // full path
            Point[] pointOrderByY = (Point[])defaultPoints.Clone();
            Array.Sort(pointOrderByY, delegate(Point a, Point b) { return a.Y.CompareTo(b.Y); });
            RangeTreePoints.AssertIntervalSearch(pointOrderByY, 0, defaultPoints.Length, rangeTree.IntervalSearch(5, 5, 47, 65));
            RangeTreePoints.AssertIntervalSearch(pointOrderByY, 0, defaultPoints.Length, rangeTree.IntervalSearch(0, 0, 100, 100));
        }

        [TestMethod()]
        public void ComplexIntervalSearchTest()
        {
            int min, max;
            Random rnd = new Random();
            Point[] points = RangeTreePoints.GetPointsForComplexTest();
            rangeTree.Build(points);
            for (int i = 0; i < 100; i++)
            {
                min = rnd.Next(1, 100);
                max = rnd.Next(1, 100);
                if (min >= max)
                    AssertInvalidIntervalSearch(rangeTree, min, min, max, max, typeof(InvalidIntervalSearchException));
                else
                    RangeTreePoints.AssertIntervalSearch(points, min - 1, max - min + 1, rangeTree.IntervalSearch(min, min, max, max));
            }
        }

        [TestMethod()]
        public void IntervalSearchTestEqualNode()
        {
            // at least one dimension must be different
            AssertInvalidIntervalSearch(rangeTree, 5, 5, 5, 5, typeof(InvalidIntervalSearchException));
            // different x
            RangeTreePoints.AssertIntervalSearch(defaultPoints, 5, 1, rangeTree.IntervalSearch(35, 52, 45, 52));
            // different y
            RangeTreePoints.AssertIntervalSearch(defaultPoints, 5, 1, rangeTree.IntervalSearch(38, 50, 38, 70));
        }

        [TestMethod()]
        public void IntervalSearchTestSwappedNodes()
        {
            Point start = defaultPoints[0]; // [5,5]
            Point end = defaultPoints[5];   // [38,52]defaultPoints[4],
            Point[] expected = new Point[] { defaultPoints[0], defaultPoints[1], defaultPoints[3], defaultPoints[5] };
            RangeTreePoints.AssertIntervalSearch(expected, 0, 4, rangeTree.IntervalSearch(start.X, start.Y, end.X, end.Y));
            AssertInvalidIntervalSearch(rangeTree, end.X, end.Y, start.X, start.Y, typeof(InvalidIntervalSearchException));
        }

        [TestMethod()]
        public void FindDuplicateTest()
        {
            rangeTree.Build(pointDuplicates);
            // find
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 0, 2, rangeTree.Find(5, 5));
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 3, 3, rangeTree.Find(40, 40));
            // interval search
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 0, 3, rangeTree.IntervalSearch(5, 5, 7, 7));
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 2, 6, rangeTree.IntervalSearch(7, 7, 47, 47));
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 0, 6, rangeTree.IntervalSearch(5, 5, 40, 40));
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 0, pointDuplicates.Length, rangeTree.IntervalSearch(5, 5, 65, 65));
        }

        [TestMethod()]
        public void FindDuplicateDifferentYTest()
        {
            Point[] points = new Point[] {
                new Point(1,1),
                new Point(1,3),
                new Point(1,5),
                new Point(1,7),
            };
            rangeTree.Build(points);
            // find
            RangeTreePoints.AssertOnePointFound(points[0], rangeTree.Find(1, 1));
            RangeTreePoints.AssertOnePointFound(points[2], rangeTree.Find(1, 5));
            try
            {
                rangeTree.Find(1, 0);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(KeyNotFoundException));
            }
            // interval search
            RangeTreePoints.AssertIntervalSearch(points, 0, 4, rangeTree.IntervalSearch(1, 1, 1, 7));
            RangeTreePoints.AssertIntervalSearch(points, 0, 4, rangeTree.IntervalSearch(0, 1, 2, 7));
            RangeTreePoints.AssertIntervalSearch(points, 0, 2, rangeTree.IntervalSearch(1, 1, 1, 3));
        }

        private void AssertInvalidIntervalSearch(IRangeTree2D<int, Point> rt, int startX, int startY, int endX, int endY, Type exceptionType)
        {
            try
            {
                rt.IntervalSearch(startX, startY, endX, endY);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == exceptionType);
            }
        }
    }
}
