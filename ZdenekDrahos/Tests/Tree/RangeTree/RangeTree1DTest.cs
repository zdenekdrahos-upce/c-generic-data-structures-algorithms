﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Tree.RangeTree;
using ZdenekDrahos.Tree.RangeTree._1D;

namespace Tests
{

    [TestClass()]
    public class RangeTree1DTest
    {

        private IRangeTree1D<int, Point> rangeTree;
        private Point[] defaultPoints, pointDuplicates;

        public RangeTree1DTest()
        {
            rangeTree = new RangeTree1D<int, Point>(RangeTreePoints.GetCoordinateX);
            defaultPoints = RangeTreePoints.GetDefaultPoints();
            rangeTree.Build(defaultPoints);
            pointDuplicates = RangeTreePoints.GetDuplicatePoints();
        }

        [TestMethod()]
        public void FindTest()
        {
            // test X coordinate
            TestAllPoints(defaultPoints);
            // test Y coordinate
            rangeTree.GetKey = RangeTreePoints.GetCoordinateY;
            rangeTree.Build(defaultPoints);
            TestAllPoints(defaultPoints);
            // invalid key
            try
            {
                rangeTree.Find(100);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(KeyNotFoundException));
            }
        }

        [TestMethod()]
        public void ComplexFindTest()
        {
            rangeTree.GetKey = RangeTreePoints.GetCoordinateX;
            // init points <1,100>
            Point[] tempPoints, points = RangeTreePoints.GetPointsForComplexTest();
            // test points, where array length is from <1,100>
            for (int i = 1; i <= points.Length; i++)
            {
                tempPoints = new Point[i];
                Array.Copy(points, 0, tempPoints, 0, i);
                rangeTree.Build(tempPoints);
                TestAllPoints(tempPoints);
            }
        }

        private void TestAllPoints(Point[] points)
        {
            for (int i = 0; i < points.Length; i++)
                RangeTreePoints.AssertOnePointFound(points[i], rangeTree.Find(rangeTree.GetKey(points[i])));
        }

        [TestMethod()]
        public void IntervalSearchTest()
        {
            RangeTreePoints.AssertIntervalSearch(defaultPoints, 1, 4, rangeTree.IntervalSearch(15, 33));
            RangeTreePoints.AssertIntervalSearch(defaultPoints, 1, 4, rangeTree.IntervalSearch(10, 35));
            // full path
            RangeTreePoints.AssertIntervalSearch(defaultPoints, 0, defaultPoints.Length, rangeTree.IntervalSearch(5, 47));
            RangeTreePoints.AssertIntervalSearch(defaultPoints, 0, defaultPoints.Length, rangeTree.IntervalSearch(0, 200));
        }

        [TestMethod()]
        public void IntervalSearchTestEqualNode()
        {
            AssertInvalidIntervalSearch(rangeTree, 5, 5, typeof(InvalidIntervalSearchException));
            rangeTree.Build(pointDuplicates);
            AssertInvalidIntervalSearch(rangeTree, 40, 40, typeof(InvalidIntervalSearchException));
        }

        [TestMethod()]
        public void IntervalSearchTestSwappedNodes()
        {
            int start = 5;
            int end = 38;
            RangeTreePoints.AssertIntervalSearch(defaultPoints, 0, 6, rangeTree.IntervalSearch(start, end));
            AssertInvalidIntervalSearch(rangeTree, end, start, typeof(InvalidIntervalSearchException));
        }

        [TestMethod()]
        public void FindDuplicateTest()
        {
            rangeTree.Build(pointDuplicates);
            // find
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 0, 2, rangeTree.Find(5));
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 3, 3, rangeTree.Find(40));
            // interval search
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 0, 3, rangeTree.IntervalSearch(5, 7));
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 2, 6, rangeTree.IntervalSearch(7, 47));
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 0, 6, rangeTree.IntervalSearch(5, 40));
            RangeTreePoints.AssertIntervalSearch(pointDuplicates, 0, pointDuplicates.Length, rangeTree.IntervalSearch(5, 65));
        }

        private void AssertInvalidIntervalSearch(IRangeTree1D<int, Point> rt, int startX, int endX, Type exceptionType)
        {
            try
            {
                rt.IntervalSearch(startX, endX);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == exceptionType);
            }
        }
    }
}
