﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Graph;

namespace Tests
{
    
    [TestClass()]
    public class NodeTest
    {
        private Node<string, string, int> nodeMakov = new Node<string,string,int>("m", "Makov");
        private Node<string, string, int> nodeLitomysl = new Node<string, string, int>("l", "Litomysl");

        [TestMethod()]
        public void RemoveEdgeTest()
        {
            Assert.AreEqual(0, nodeMakov.EdgesCount());
            nodeMakov.AddEdge(nodeLitomysl, 25);
            Assert.AreEqual(1, nodeMakov.EdgesCount());
            nodeMakov.RemoveEdge(nodeLitomysl.Key);
            Assert.AreEqual(0, nodeMakov.EdgesCount());

            try
            {
                nodeMakov.RemoveEdge("litomysl");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        [TestMethod()]
        public void FindEdgeTest()
        {
            int edgeCost = 25;
            nodeMakov.AddEdge(nodeLitomysl, edgeCost);
            object incidenceElement = nodeMakov.FindEdge(nodeLitomysl.Key);
            Assert.IsInstanceOfType(incidenceElement, typeof(NodeConnection<string, string, int>));
            NodeConnection<string, string, int> element = (NodeConnection<string, string, int>)incidenceElement;
            Assert.AreEqual(nodeLitomysl.Value, element.Edge.EndNode);
            Assert.AreEqual(nodeMakov.Value, element.Edge.StartNode);
            Assert.AreEqual(edgeCost, element.Edge.EdgeCost);

            try
            {
                nodeMakov.FindEdge("litomysl");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        [TestMethod()]
        public void AddEdgeTest()
        {
            Assert.AreEqual(0, nodeMakov.EdgesCount());
            nodeMakov.AddEdge(nodeLitomysl, 25);
            Assert.AreEqual(1, nodeMakov.EdgesCount());
            
            // no mutiple edges
            try
            {
                nodeMakov.AddEdge(nodeLitomysl, 15);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }
        }
    }
}
