﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tests.Helpers;
using ZdenekDrahos.Graph;
using ZdenekDrahos.Graph.ShortestPath;
using ZdenekDrahos.Graph.ShortestPath.Dijkstra;

namespace Tests
{
    
    [TestClass()]
    public class DijkstraAlgorithmTest
    {

        private IGraph<string, string, double> graph;
        private IShortestPath<string, string, double> shortestPath;

        public DijkstraAlgorithmTest()
        {
            graph = DefaultGraph.GetTestGraph();
            shortestPath = new DijkstraAlgorithm<string, string, double>(graph);
        }

        [TestMethod()]
        public void GetShortestPathToNodeTest()
        {
            ShortestPath[] shortestPaths = GraphShortestPaths.GetDijkstraShortestPaths();
            for (int i = 0; i < shortestPaths.GetLength(0); i++)
                AssertPaths(shortestPaths[i]);
        }

        private void AssertPaths(ShortestPath path)
        {
            Path<string> foundPath = shortestPath.GetShortestPath(path.Start, path.End);
            path.AssertPath(foundPath);
        }

        [TestMethod()]
        public void PathWithDisabledNodesTest()
        {
            NodesPathRestriction<string, string, double> limit = new NodesPathRestriction<string, string, double>();
            limit.DisabledNodes.Add("C");
            shortestPath.IsEdgeAccessible = limit.IsEdgeAccessible;
            AssertPaths(ShortestPath.Create("A", "B", 5));
            AssertPaths(ShortestPath.Create("A", "D", 8));
            AssertPaths(ShortestPath.Create("A", "B", "E", 7));
            AssertPaths(ShortestPath.Create("A", "B", "E", "F", 10));
            AssertPaths(ShortestPath.Create("A", "B", "E", "F", "G", 12));

            // end C is not accessible (same case as if node is isolated)
            AssertInvalidGetShortestPath("A", "C", typeof(PathNotFoundException));
            
            // input C is not accessible (same case as if node is isolated)
            AssertInvalidGetShortestPath("C", "G", typeof(PathNotFoundException));

            // problem in GUI
            limit.DisabledNodes.Clear();
            limit.DisabledNodes.Add("F");
            shortestPath.IsEdgeAccessible = limit.IsEdgeAccessible;
            AssertPaths(ShortestPath.Create("D", "C", "B", "E", "G", 18));
        }

        [TestMethod()]
        public void InvalidGetShortestPathTest()
        {
            // method CreateShortestPathsMatrix is not called so path cannot be reconstructed
            AssertInvalidGetShortestPath("", "A", typeof(KeyNotFoundException));

            // end key not exists
            AssertInvalidGetShortestPath("C", "UnexistingKeay", typeof(KeyNotFoundException));
            // start key not exists
            AssertInvalidGetShortestPath("UnexistingKey", "A", typeof(KeyNotFoundException));

            // start key is isolated
            AssertInvalidGetShortestPath("IsolatedKey", "A", typeof(PathNotFoundException));
            // end key is isolated
            AssertInvalidGetShortestPath("A", "IsolatedKey", typeof(PathNotFoundException));

            // graph is not set
            shortestPath.Graph = null;
            AssertInvalidGetShortestPath("C", "A", typeof(InvalidOperationException));
        }

        private void AssertInvalidGetShortestPath(string start, string end, Type exceptionType)
        {
            try
            {
                shortestPath.GetShortestPath(start, end);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == exceptionType);
            }
        }
    }
}
