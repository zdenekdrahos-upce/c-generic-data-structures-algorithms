﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Graph;

namespace Tests
{
    
    [TestClass()]
    public class GraphTest
    {
        private Graph<string, string, int> graph = new Graph<string, string, int>();

        [TestMethod()]
        public void AddEdgeTest()
        {
            graph.AddNode("m", "makov");
            graph.AddNode("l", "litomysl");

            graph.AddEdge("m", "l", 20);
            Edge<string, string, int> edge = graph.FindEdge("m", "l");
            Assert.AreEqual(20, edge.EdgeCost);

            // test existing edge
            try
            {
                graph.AddEdge("l", "m", 20);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentException);
            }            
            
        }

        [TestMethod()]
        public void AddNodeTest()
        {
            Assert.AreEqual(0, graph.Count);
            graph.AddNode("m", "makov");
            Assert.AreEqual(1, graph.Count);
            Assert.AreEqual("makov", graph.FindNode("m"));
        }

        [TestMethod()]
        public void ClearTest()
        {
            graph.AddNode("m", "makov");
            Assert.IsFalse(graph.IsEmpty());
            graph.Clear();
            Assert.IsTrue(graph.IsEmpty());
        }

        [TestMethod()]
        public void FindEdgeTest()
        {
            graph.AddNode("m", "makov");
            graph.AddNode("l", "litomysl");
            graph.AddEdge("m", "l", 20);
            Edge<string, string, int> edge = graph.FindEdge("m", "l");
            Assert.AreEqual(20, edge.EdgeCost);

            try
            {
                graph.FindEdge("makov", "l");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        [TestMethod()]
        public void FindNodeTest()
        {
            graph.AddNode("m", "makov");
            Assert.AreEqual(1, graph.Count);
            Assert.AreEqual("makov", graph.FindNode("m"));
            Assert.AreEqual(1, graph.Count);

            try
            {
                graph.FindNode("unexistingKey");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        [TestMethod()]
        public void IsEmptyTest()
        {
            Assert.IsTrue(graph.IsEmpty());
            graph.AddNode("m", "makov");
            Assert.IsFalse(graph.IsEmpty());
        }

        [TestMethod()]
        public void RemoveEdgeTest()
        {
            graph.AddNode("m", "makov");
            graph.AddNode("l", "litomysl");
            graph.AddEdge("m", "l", 20);

            Assert.IsTrue(graph.FindDevNode("m").ExistsEdge("l"));
            Assert.IsTrue(graph.FindDevNode("l").ExistsEdge("m"));

            Assert.AreEqual(20, graph.RemoveEdge("m", "l"));

            Assert.IsFalse(graph.FindDevNode("m").ExistsEdge("l"));
            Assert.IsFalse(graph.FindDevNode("l").ExistsEdge("m"));

            try
            {
                graph.RemoveEdge("m", "l");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }

        [TestMethod()]
        public void RemoveNodeTest()
        {
            graph.AddNode("m", "makov");
            Assert.AreEqual(1, graph.Count);
            Assert.AreEqual("makov", graph.RemoveNode("m"));
            Assert.AreEqual(0, graph.Count);

            try
            {
                graph.RemoveNode("unexistingKey");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is KeyNotFoundException);
            }
        }
    }
}
