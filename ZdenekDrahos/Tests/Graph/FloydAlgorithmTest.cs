﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tests.Helpers;
using ZdenekDrahos.Graph.ShortestPath;
using ZdenekDrahos.Graph.ShortestPath.Floyd;

namespace Tests
{
    
    [TestClass()]
    public class FloydAlgorithmTest
    {

        private IShortestPathsMatrix<string, string, double> shortestPath;

        public FloydAlgorithmTest()
        {
            shortestPath = new FloydAlgorithm<string, string, double>();
        }

        [TestMethod()]
        public void CreateShortestPathsMatrixTest()
        {
            ShortestPath[] shortestPaths = GraphShortestPaths.GetFloydShortestPaths();
            shortestPath.CreateShortestPathsMatrix(DefaultGraph.GetTestGraph());            
            for (int i = 0; i < shortestPaths.GetLength(0); i++)
                AssertPaths(shortestPaths[i]);
        }

        private void AssertPaths(ShortestPath path)
        {
            Path<string> foundPath = shortestPath.GetShortestPath(path.Start, path.End);
            path.AssertPath(foundPath);
        }

        [TestMethod()]
        public void InvalidGetShortestPathTest()
        {
            // method CreateShortestPathsMatrix is not called so path cannot be reconstructed
            AssertInvalidGetShortestPath("C", "A", typeof(InvalidOperationException));

            shortestPath.CreateShortestPathsMatrix(DefaultGraph.GetTestGraph());
            // end key not exists
            AssertInvalidGetShortestPath("C", "UnexistingKeay", typeof(KeyNotFoundException));               
            // start key not exists
            AssertInvalidGetShortestPath("UnexistingKey", "A", typeof(KeyNotFoundException));               

            // start key is isolated
            AssertInvalidGetShortestPath("IsolatedKey", "A", typeof(PathNotFoundException));               
            // end key is isolated
            AssertInvalidGetShortestPath("A", "IsolatedKey", typeof(PathNotFoundException));               

            // graph is not set
            shortestPath.Graph = null;
            AssertInvalidGetShortestPath("C", "A", typeof(InvalidOperationException));    
        }

        private void AssertInvalidGetShortestPath(string start, string end, Type exceptionType)
        {
            try
            {
                shortestPath.GetShortestPath(start, end);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == exceptionType);
            }
        }
    }
}
