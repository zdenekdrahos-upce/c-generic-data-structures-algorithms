﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Search.Array;

namespace Tests
{

    [TestClass()]
    public class ArraySearchTest
    {

        private ArraySearcher<int> searcherInt;
        private ArraySearcher<string> searcherString;
        private int[] intArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        public ArraySearchTest()
        {
            searcherInt = ComparableArraySearcherFactory<int>.BinarySearch();
            searcherString = ComparableArraySearcherFactory<string>.BinarySearch();
            searcherInt.SetSearchedArray(intArray);
        }

        [TestMethod()]
        public void SetBinarySearchTest()
        {
            searcherInt.SetBinarySearch();
            AssertSearchedValues();
        }

        [TestMethod()]
        public void SetInterpolationSearchTest()
        {
            searcherInt.SetInterpolationSearch((x) => (double)x);
            AssertSearchedValues();
        }

        private void AssertSearchedValues()
        {
            for (int i = 0; i < intArray.Length; i++)
                Assert.AreEqual(intArray[i], searcherInt.Search(intArray[i]));
        }

        [TestMethod()]
        public void EmptyValueTest()
        {
            AssertSearchedArrayWithEmptyValues(new string[] { "A", "B", null, null, null, "E", "F" });
            AssertSearchedArrayWithEmptyValues(new string[] { null, null, null, "E", "F" });
            AssertSearchedArrayWithEmptyValues(new string[] { "A", "B", null, null, null });
        }

        private void AssertSearchedArrayWithEmptyValues(string[] stringArray)
        {
            searcherString.SetSearchedArray(stringArray);
            searcherString.SetBinarySearch();
            AssertStringArray(stringArray);
            searcherString.SetInterpolationSearch((x) => (double)x[0]);
            AssertStringArray(stringArray);
        }

        private void AssertStringArray(string[] stringArray)
        {
            for (int i = 0; i < stringArray.Length; i++)
                if (stringArray[i] != null)
                    Assert.AreEqual(stringArray[i], searcherString.Search(stringArray[i]));
        }

        [TestMethod()]
        public void EmptyArrayTest()
        {
            string[] stringArray = { null, null, null };

            searcherString.SetBinarySearch();
            AssertEmptySearchedArray(stringArray);

            searcherString.SetInterpolationSearch((x) => (double)x[0]);
            AssertEmptySearchedArray(stringArray);
        }

        private void AssertEmptySearchedArray(string[] stringArray)
        {
            try
            {
                searcherString.SetSearchedArray(stringArray);
                searcherString.Search("A");
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentNullException);
            }
        }

    }
}
