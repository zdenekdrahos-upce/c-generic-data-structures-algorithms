﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Statistics.Median;

namespace Tests
{

    [TestClass()]
    public class MedianCalculatorTest
    {
        private IMedian<int> median;

        public MedianCalculatorTest()
        {
            median = new MedianCalculator<int>();
        }

        [TestMethod()]
        public void FindTest()
        {
            AssertMedianFromFind(2, new int[] { 1, 2, 3 });
            AssertMedianFromFind(2, new int[] { 56, 2, 600 });
            AssertMedianFromSortAndFind(56, new int[] { 56, 2, 600 });
        }

        private void AssertMedianFromFind(double expectedMedian, int[] array)
        {
            median.Find(array);
            Assert.AreEqual(expectedMedian, CalcMedian(median.Median));
        }

        [TestMethod()]
        public void CalculateTestEvenCount()
        {
            AssertMedianFromSortAndFind(16.5, new int[] { 15, 18, 11, 32 });
        }

        [TestMethod()]
        public void CalculateTestOddCount()
        {
            AssertMedianFromSortAndFind(8.0, new int[] { 5, 20, 7, 2, 8, 24, 6, 12, 15 });
        }

        private void AssertMedianFromSortAndFind(double expectedMedian, int[] array)
        {
            median.SortAndFind(array);
            Assert.AreEqual(expectedMedian, CalcMedian(median.Median));
        }

        private double CalcMedian(ICollection<int> medians)
        {
            double result = 0;
            foreach (int median in medians)
                result += median;
            return result / (double)medians.Count;
        }

        private double ConvertIntToDouble(int i)
        {
            return i;
        }
    }
}
