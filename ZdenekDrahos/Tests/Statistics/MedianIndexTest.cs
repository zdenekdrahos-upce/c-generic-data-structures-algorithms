﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Statistics.Median;

namespace Tests
{

    [TestClass()]
    public class MedianIndexTest
    {

        private IMedianIndex median = new MedianIndex();

        [TestMethod()]
        public void GetMedianIndexTest()
        {
            Assert.AreEqual(10, median.GetMedianIndex(20));
            Assert.AreEqual(9, median.GetMedianIndex(19));
            Assert.AreEqual(9, median.GetMedianIndex(18));

            Assert.AreEqual(5, median.GetMedianIndex(10));
            Assert.AreEqual(2, median.GetMedianIndex(5));
            Assert.AreEqual(1, median.GetMedianIndex(3));
            Assert.AreEqual(1, median.GetMedianIndex(2));
            Assert.AreEqual(0, median.GetMedianIndex(1));
        }

        [TestMethod()]
        public void NegativeSizeTest()
        {
            AssertNegativeIndex(0);
            AssertNegativeIndex(-10);
        }

        private void AssertNegativeIndex(int size)
        {
            try
            {
                median.GetMedianIndex(0);
                Assert.Fail("no exception thrown");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(ArgumentException));
            }
        }

    }
}
