﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.App.Model.Files;
using ZdenekDrahos.Files.Block.Blocks;
using ZdenekDrahos.Files.Block.Records;
using ZdenekDrahos.Files.Index;
using ZdenekDrahos.Tree.Tree23.RedBlack;

namespace Tests
{

    [TestClass()]
    public class IndexByteConverterTest
    {

        private IBlockByteConverter<CrossRoadKey, RedBlackTreeValue<KeyIndex>> converter = new IndexByteConverter();
        private ControlBlock controlBlock;
        private Block<CrossRoadKey, RedBlackTreeValue<KeyIndex>> block;

        public IndexByteConverterTest()
        {
            int recordsCount = 2;
            controlBlock = new ControlBlock(recordsCount, recordsCount * 18);
            block = new Block<CrossRoadKey, RedBlackTreeValue<KeyIndex>>(recordsCount);
            // first record
            block.FreeBlocks[0] = false;
            block.Records[0] = new Record<CrossRoadKey, RedBlackTreeValue<KeyIndex>>(
                new CrossRoadKey(68, 68),
                RedBlackTreeValue<KeyIndex>.CreateRed(new KeyIndex(68, 68))
            );
            // second records - empty
            block.FreeBlocks[1] = true;
            block.Records[1] = null;
        }

        [TestMethod()]
        public void CreateControlBlockTest()
        {
            int records = 5;
            ControlBlock block = converter.CreateControlBlock(records, 0);
            Assert.AreEqual(records, block.RecordsInBlockCount);
            Assert.AreEqual(16, block.ControlBlockSize);
            Assert.AreEqual(records * 18, block.BlockSize);
            Assert.AreEqual(0, block.AllocatedBlocksCount);
        }

        [TestMethod()]
        public void SerializeControlBlockTest()
        {
            byte[] bytes = converter.SerializeControlBlock(controlBlock);
            ControlBlock deserialized = converter.DeserializeControlBlock(bytes);
            Assert.AreEqual(controlBlock.RecordsInBlockCount, deserialized.RecordsInBlockCount);
            Assert.AreEqual(controlBlock.ControlBlockSize, deserialized.ControlBlockSize);
            Assert.AreEqual(controlBlock.BlockSize, deserialized.BlockSize);
            Assert.AreEqual(controlBlock.AllocatedBlocksCount, deserialized.AllocatedBlocksCount);
        }

        [TestMethod()]
        public void SerializeBlockTest()
        {
            byte[] bytes = converter.SerializeBlock(block, controlBlock);
            Block<CrossRoadKey, RedBlackTreeValue<KeyIndex>> deserialized = converter.DeserializeBlock(bytes);
            for (int i = 0; i < controlBlock.RecordsInBlockCount; i++)
            {
                Assert.AreEqual(block.FreeBlocks[i], deserialized.FreeBlocks[i]);
                if (block.FreeBlocks[i])
                    Assert.IsNull(deserialized.Records[i]);
                else
                {
                    Assert.IsTrue(block.Records[0].Key.CompareTo(deserialized.Records[i].Key) == 0);
                    Assert.AreEqual(block.Records[0].Value.IsRed, deserialized.Records[0].Value.IsRed);
                    Assert.AreEqual(block.Records[0].Value.Value.Block, deserialized.Records[0].Value.Value.Block);
                    Assert.AreEqual(block.Records[0].Value.Value.Record, deserialized.Records[0].Value.Value.Record);
                }
            }
        }

    }
}
