﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZdenekDrahos.Graph;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZdenekDrahos.Graph.ShortestPath;

namespace Tests.Helpers
{
    class ShortestPath
    {
        public readonly string Start;
        public readonly string End;
        public readonly string[] ExpectedPath;
        public readonly double Cost;

        public static ShortestPath Create(params object[] list)
        {
            return new ShortestPath(list);
        }

        private ShortestPath(object[] list)
        {
            Cost = double.Parse(list[list.Length - 1].ToString());
            Start = list[0].ToString();
            End = list[list.Length - 2].ToString();

            if (list.Length == 3 && Start == End)
            {
                ExpectedPath = new string[1] {Start};
            }
            else
            {
                ExpectedPath = new string[list.Length - 1];
                int index = 0;
                for (int i = 0; i < list.Length - 1; i++)
                    ExpectedPath[index++] = list[i].ToString();
            }
        }

        public void AssertPath(Path<string> foundPath)
        {
            Assert.AreEqual(ExpectedPath.Length, foundPath.Nodes.Count);
            int arrayIndex = 0;
            foreach (string node in foundPath)
                Assert.AreEqual(ExpectedPath[arrayIndex++], node);
            Assert.AreEqual(Cost, foundPath.Cost);
        }
    }
}
