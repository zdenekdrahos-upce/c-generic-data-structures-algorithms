﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Helpers
{
    class RangeTreePoints
    {

        public static int GetCoordinateX(Point point)
        {
            return point.X;
        }

        public static int GetCoordinateY(Point point)
        {
            return point.Y;
        }

        public static void AssertOnePointFound(Point expected, IList<Point> foundPoints)
        {
            Assert.AreEqual(1, foundPoints.Count);
            AssertPoints(expected, foundPoints.First());
        }

        public static void AssertMultiplePointsFound(IEnumerable<Point> expectedPoints, IList<Point> foundPoints)
        {
            Assert.AreEqual(expectedPoints.Count(), foundPoints.Count);
            for (int i = 0; i < foundPoints.Count; i++)
                AssertPoints(expectedPoints.ElementAt(i), foundPoints.ElementAt(i));
        }

        private static void AssertPoints(Point expected, Point found)
        {
            Assert.AreEqual(expected.X, found.X);
            Assert.AreEqual(expected.Y, found.Y);
        }

    }
}
