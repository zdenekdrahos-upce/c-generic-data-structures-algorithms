﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tests.Helpers
{
    class GraphShortestPaths
    {
        public static ShortestPath[] GetDijkstraShortestPaths()
        {
            ShortestPath[] paths = GetFloydShortestPaths();
            // Dijkstra finds F-E-B-C instead of C-F
            paths[36] = ShortestPath.Create("F", "E", "B", "C", "A", 8);
            paths[38] = ShortestPath.Create("F", "E", "B", "C", 7);
            paths[43] = ShortestPath.Create("G", "F", "E", "B", "C", "A", 10);
            paths[45] = ShortestPath.Create("G", "F", "E", "B", "C", 9);
            return paths;
        }

        public static ShortestPath[] GetFloydShortestPaths()
        {
            return new ShortestPath[] {                
                // paths from A
                ShortestPath.Create("A", "A", 0),
                ShortestPath.Create("A", "C", "B", 3),
                ShortestPath.Create("A", "C", 1),
                ShortestPath.Create("A", "C", "D", 5),
                ShortestPath.Create("A", "C", "B", "E", 5),
                ShortestPath.Create("A", "C","F", 8),
                ShortestPath.Create("A", "C", "F", "G", 10),

                // paths from B
                ShortestPath.Create("B", "B", 0),
                ShortestPath.Create("B", "C", "A", 3),
                ShortestPath.Create("B", "C", 2),
                ShortestPath.Create("B", "C", "D", 6), 
                ShortestPath.Create("B", "E", 2),
                ShortestPath.Create("B", "E", "F", 5),
                ShortestPath.Create("B", "E", "F", "G", 7),

                // paths from C
                ShortestPath.Create("C", "C", 0),
                ShortestPath.Create("C", "A", 1),
                ShortestPath.Create("C", "B", 2),
                ShortestPath.Create("C", "D", 4),
                ShortestPath.Create("C", "B", "E", 4),
                ShortestPath.Create("C", "F", 7),
                ShortestPath.Create("C", "F", "G", 9),

                // paths from D
                ShortestPath.Create("D", "D", 0),
                ShortestPath.Create("D", "C", "A", 5),
                ShortestPath.Create("D", "C", "B", 6),
                ShortestPath.Create("D", "C", 4), 
                ShortestPath.Create("D", "C", "B", "E", 8),
                ShortestPath.Create("D", "F", 8),
                ShortestPath.Create("D", "F", "G", 10),

                // paths from E
                ShortestPath.Create("E", "E", 0),
                ShortestPath.Create("E", "B", "C", "A", 5),
                ShortestPath.Create("E", "B", 2),
                ShortestPath.Create("E", "B", "C", 4),
                ShortestPath.Create("E", "B", "C", "D", 8),
                ShortestPath.Create("E", "F", 3),
                ShortestPath.Create("E", "F", "G", 5),

                // paths from F
                ShortestPath.Create("F", "F", 0),
                ShortestPath.Create("F", "C", "A", 8),
                ShortestPath.Create("F", "E", "B", 5),
                ShortestPath.Create("F", "C", 7),
                ShortestPath.Create("F", "D", 8),
                ShortestPath.Create("F", "E", 3),
                ShortestPath.Create("F", "G", 2),

                // paths from G
                ShortestPath.Create("G", "G", 0),
                ShortestPath.Create("G", "F", "C", "A", 10),
                ShortestPath.Create("G", "F", "E", "B", 7),
                ShortestPath.Create("G", "F", "C", 9),
                ShortestPath.Create("G", "F", "D", 10),
                ShortestPath.Create("G", "F", "E", 5),
                ShortestPath.Create("G", "F", 2),
            };
        }

    }
}
