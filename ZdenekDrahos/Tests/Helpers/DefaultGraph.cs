﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZdenekDrahos.Graph;

namespace Tests.Helpers
{
    class DefaultGraph
    {
        public static IGraph<string, string, double> GetTestGraph()
        {
            IGraph<string, string, double> graph = new Graph<string, string, double>();
            graph.AddNode("A", "A");
            graph.AddNode("B", "B");
            graph.AddNode("C", "C");
            graph.AddNode("D", "D");
            graph.AddNode("E", "E");
            graph.AddNode("F", "F");
            graph.AddNode("G", "G");
            graph.AddNode("IsolatedKey", "IsolatedKey");
            graph.AddEdge("A", "B", 5);
            graph.AddEdge("A", "C", 1);
            graph.AddEdge("A", "D", 8);
            graph.AddEdge("B", "C", 2);
            graph.AddEdge("B", "E", 2);
            graph.AddEdge("D", "C", 4);
            graph.AddEdge("D", "F", 8);
            graph.AddEdge("C", "E", 6);
            graph.AddEdge("C", "F", 7);
            graph.AddEdge("E", "F", 3);
            graph.AddEdge("E", "G", 10);
            graph.AddEdge("F", "G", 2);
            return graph;
        }

    }
}
