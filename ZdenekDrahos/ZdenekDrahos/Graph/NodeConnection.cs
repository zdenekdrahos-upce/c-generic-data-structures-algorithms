﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.Graph
{
    public class NodeConnection<TNodeKey, TNode, TEdge>
    {
        public Node<TNodeKey, TNode, TEdge> EndNode { get; private set; }
        public Edge<TNodeKey, TNode, TEdge> Edge { get; set; }

        public NodeConnection(Node<TNodeKey, TNode, TEdge> node, Edge<TNodeKey, TNode, TEdge> edge)
        {
            EndNode = node;
            Edge = edge;
        }
    }
}
