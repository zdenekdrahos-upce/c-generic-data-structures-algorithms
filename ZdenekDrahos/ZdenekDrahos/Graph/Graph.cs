﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Graph.Iterators;
using ZdenekDrahos.Table;

namespace ZdenekDrahos.Graph
{
    public class Graph<TNodeKey, TNode, TEdge> : IGraph<TNodeKey, TNode, TEdge>
    {
        private ITable<TNodeKey, Node<TNodeKey, TNode, TEdge>> nodes;

        public Graph()
        {
            nodes = new HashTable<TNodeKey, Node<TNodeKey, TNode, TEdge>>();
        }

        #region IGraph<TNodeKey,TNode,TEdge> Members

        public event GraphChangedEventHandler GraphChanged;

        public void Clear()
        {
            nodes.Clear();
        }

        public bool IsEmpty()
        {
            return nodes.IsEmpty();
        }

        public int Count
        {
            get { return nodes.Count; }
            private set {}
        }

        public void AddNode(TNodeKey keyNode, TNode node)
        {
            Node<TNodeKey, TNode, TEdge> newNode = new Node<TNodeKey, TNode, TEdge>(keyNode, node);
            nodes.Add(keyNode, newNode);
            OnGraphChanged();
        }

        public void AddEdge(TNodeKey startNode, TNodeKey endNode, TEdge edge)
        {
            Node<TNodeKey, TNode, TEdge> start = nodes.Find(startNode);
            Node<TNodeKey, TNode, TEdge> end = nodes.Find(endNode);
            start.AddEdge(end, edge);
            end.AddEdge(start, edge);
            OnGraphChanged();
        }

        public void ModifyNode(TNodeKey keyNode, TNode newNode) 
        {
            Node<TNodeKey, TNode, TEdge> updatedNode = nodes.Find(keyNode);
            updatedNode.UpdateNode(newNode);
            foreach (NodeConnection<TNodeKey, TNode, TEdge> neighbour in updatedNode)
            {
                updatedNode.ModifyEdge(neighbour.EndNode, neighbour.Edge.EdgeCost);
                neighbour.EndNode.ModifyEdge(updatedNode, neighbour.Edge.EdgeCost);
            }
            OnGraphChanged();
        }

        public void ModifyEdge(TNodeKey startNode, TNodeKey endNode, TEdge newEdge)
        {
            Node<TNodeKey, TNode, TEdge> start = nodes.Find(startNode);
            Node<TNodeKey, TNode, TEdge> end = nodes.Find(endNode);
            start.ModifyEdge(end, newEdge);
            end.ModifyEdge(start, newEdge);
            OnGraphChanged();
        }

        public TNode RemoveNode(TNodeKey keyNode)
        {
            Node<TNodeKey, TNode, TEdge> removedNode = nodes.Remove(keyNode);            
            foreach (NodeConnection<TNodeKey, TNode, TEdge> neighbour in removedNode)
                if (neighbour.EndNode.ExistsEdge(keyNode))
                    neighbour.EndNode.RemoveEdge(keyNode);
            removedNode.RemoveAllEdges();
            OnGraphChanged();
            return removedNode.Value;
        }

        public TEdge RemoveEdge(TNodeKey startNode, TNodeKey endNode)
        {
            TEdge removedEdge = nodes.Find(startNode).RemoveEdge(endNode);
            nodes.Find(endNode).RemoveEdge(startNode);
            OnGraphChanged();
            return removedEdge;
        }

        public TNode FindNode(TNodeKey keyNode)
        {
            return nodes.Find(keyNode).Value;
        }

        public Node<TNodeKey, TNode, TEdge> FindDevNode(TNodeKey keyNode)
        {
            return nodes.Find(keyNode);
        }

        public Edge<TNodeKey, TNode, TEdge> FindEdge(TNodeKey startNode, TNodeKey endNode)
        {
            Node<TNodeKey, TNode, TEdge> start = nodes.Find(startNode);
            return start.FindEdge(endNode).Edge;
        }

        public IEnumerable<TNode> BreadthFirstSearchNodes(TNodeKey startNodeKey)
        {
            return new BreadthFirstSearchNodes<TNodeKey, TNode, TEdge>(this, startNodeKey);
        }

        public IEnumerable<Edge<TNodeKey, TNode, TEdge>> EdgesIterator()
        {
            return new EdgesIterator<TNodeKey, TNode, TEdge>(this);
        }

        public IEnumerable<TNodeKey> NodeKeysIterator()
        {
            foreach (TNodeKey key in nodes.Keys)
                yield return key;
        }

        public IEnumerable<TNode> IncidenceNodes(TNodeKey node)
        {
            foreach (NodeConnection<TNodeKey, TNode, TEdge> item in nodes.Find(node))
                yield return item.EndNode.Value;
        }

        public IEnumerable<Node<TNodeKey, TNode, TEdge>> NodesDevIterator()
        {
            foreach (KeyValuePair<TNodeKey, Node<TNodeKey, TNode, TEdge>> node in nodes)
                yield return node.Value;
        }

        #endregion

        #region IEnumerable<TNode> Members

        public IEnumerator<TNode> GetEnumerator()
        {
            foreach (KeyValuePair<TNodeKey, Node<TNodeKey, TNode, TEdge>> item in nodes)
                yield return item.Value.Value;
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        protected virtual void OnGraphChanged()
        {
            GraphChangedEventHandler handler = this.GraphChanged;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
