﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;

namespace ZdenekDrahos.Graph
{
    public interface IGraph<TNodeKey, TNode, TEdge> : IEnumerable<TNode>
    {
        event GraphChangedEventHandler GraphChanged;

        void Clear();

        bool IsEmpty();

        int Count { get; }

        void AddNode(TNodeKey keyNode, TNode node);

        void AddEdge(TNodeKey startNode, TNodeKey endNode, TEdge edge);

        void ModifyNode(TNodeKey keyNode, TNode newNode);

        void ModifyEdge(TNodeKey startNode, TNodeKey endNode, TEdge newEdge);

        TNode RemoveNode(TNodeKey keyNode);

        TEdge RemoveEdge(TNodeKey startNode, TNodeKey endNode);

        TNode FindNode(TNodeKey keyNode);

        Edge<TNodeKey, TNode, TEdge> FindEdge(TNodeKey startNode, TNodeKey endNode);

        IEnumerable<TNode> BreadthFirstSearchNodes(TNodeKey startNodeKey);

        IEnumerable<Edge<TNodeKey, TNode, TEdge>> EdgesIterator();

        IEnumerable<TNodeKey> NodeKeysIterator();

        IEnumerable<TNode> IncidenceNodes(TNodeKey node);

        // Methods for advanced developers actions - iterators, shortest paths

        Node<TNodeKey, TNode, TEdge> FindDevNode(TNodeKey keyNode);

        IEnumerable<Node<TNodeKey, TNode, TEdge>> NodesDevIterator();
    }
}
