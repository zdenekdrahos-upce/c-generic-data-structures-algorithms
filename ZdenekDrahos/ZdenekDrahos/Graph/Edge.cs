﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.Graph
{
    public struct Edge<TNodeKey, TNode, TEdge>
    {
        public readonly TNode StartNode;
        public readonly TNode EndNode;
        public readonly TEdge EdgeCost;

        public Edge(Node<TNodeKey, TNode, TEdge> startNode, Node<TNodeKey, TNode, TEdge> endNode, TEdge cost)
        {
            StartNode = startNode.Value;
            EndNode = endNode.Value;
            EdgeCost = cost;
        }
    }
}
