﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;

namespace ZdenekDrahos.Graph.Iterators
{
    public class EdgesIterator<TNodeKey, TNode, TEdge> : IEnumerable<Edge<TNodeKey, TNode, TEdge>>
    {

        private IGraph<TNodeKey, TNode, TEdge> graph;
        private IList<KeyValuePair<TNodeKey, TNodeKey>> visitedEdges;

        public EdgesIterator(IGraph<TNodeKey, TNode, TEdge> graph)
        {
            this.graph = graph;
            visitedEdges = new List<KeyValuePair<TNodeKey, TNodeKey>>();
        }
        #region IEnumerable<TEdge> Members

        public IEnumerator<Edge<TNodeKey, TNode, TEdge>> GetEnumerator()
        {
            foreach (Node<TNodeKey, TNode, TEdge> node in graph.NodesDevIterator())
                foreach (NodeConnection<TNodeKey, TNode, TEdge> neighbour in node)
                    if (EdgeIsNotVisited(node.Key, neighbour.EndNode.Key))
                    {
                        AddEdge(node.Key, neighbour.EndNode.Key);
                        yield return neighbour.Edge;      
                    }
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        private void AddEdge(TNodeKey startNode, TNodeKey endNode)
        {
            visitedEdges.Add(new KeyValuePair<TNodeKey, TNodeKey>(startNode, endNode));
        }

        private bool EdgeIsNotVisited(TNodeKey startNode, TNodeKey endNode)
        {
            foreach (KeyValuePair<TNodeKey, TNodeKey> pair in visitedEdges)
            {
                if ((pair.Key.Equals(startNode) && pair.Value.Equals(endNode)) ||
                    (pair.Key.Equals(endNode) && pair.Value.Equals(startNode)))
                    return false;
            }
            return true;
        }
    }
}
