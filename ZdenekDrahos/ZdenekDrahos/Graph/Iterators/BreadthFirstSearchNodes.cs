﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;

namespace ZdenekDrahos.Graph.Iterators
{
    public class BreadthFirstSearchNodes<TNodeKey, TNode, TEdge> : IEnumerable<TNode>
    {

        public TNodeKey StartNodeKey { get; set; }
        public IGraph<TNodeKey, TNode, TEdge> Graph { get; set; }

        private Queue<Node<TNodeKey, TNode, TEdge>> queue;
        private IList<TNodeKey> visitedNodes;

        public BreadthFirstSearchNodes(IGraph<TNodeKey, TNode, TEdge> graph, TNodeKey startNodeKey)
        {
            Graph = graph;
            StartNodeKey = startNodeKey;
            queue = new Queue<Node<TNodeKey, TNode, TEdge>>();
            visitedNodes = new List<TNodeKey>();
        }

        #region IEnumerable<KeyValuePair<TNodeKey,TNode>> Members

        public IEnumerator<TNode> GetEnumerator()
        {
            AddNode(Graph.FindDevNode(StartNodeKey));
            while (queue.Count > 0) 
            {
                Node<TNodeKey, TNode, TEdge> removedNode = queue.Dequeue();
                foreach (NodeConnection<TNodeKey, TNode, TEdge> neighbour in removedNode)
                    if (!visitedNodes.Contains(neighbour.EndNode.Key))
                        AddNode(neighbour.EndNode);
                yield return removedNode.Value;
            }
            visitedNodes.Clear();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        private void AddNode(Node<TNodeKey, TNode, TEdge> node)
        {
            queue.Enqueue(node);
            visitedNodes.Add(node.Key);
        }
    }
}
