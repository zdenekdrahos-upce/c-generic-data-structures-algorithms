﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Table;

namespace ZdenekDrahos.Graph
{
    public class Node<TNodeKey, TNode, TEdge> : IEnumerable<NodeConnection<TNodeKey, TNode, TEdge>>
    {

        public TNodeKey Key { get; private set; }
        public TNode Value { get; private set; }
        private ITable<TNodeKey, NodeConnection<TNodeKey, TNode, TEdge>> edges;

        public Node(TNodeKey keyNode, TNode node)
        {
            Key = keyNode;
            Value = node;
            edges = new ListTable<TNodeKey, NodeConnection<TNodeKey, TNode, TEdge>>();
        }

        public int EdgesCount()
        {
            return edges.Count;
        }

        public void AddEdge(Node<TNodeKey, TNode, TEdge> endNode, TEdge edgeCost)
        {
            if (ExistsEdge(endNode.Key))
                throw new ArgumentException();
            Edge<TNodeKey, TNode, TEdge> edge = new Edge<TNodeKey, TNode, TEdge>(this, endNode, edgeCost);
            NodeConnection<TNodeKey, TNode, TEdge> connection = new NodeConnection<TNodeKey, TNode, TEdge>(endNode, edge);
            edges.Add(endNode.Key, connection);
        }

        public void ModifyEdge(Node<TNodeKey, TNode, TEdge> endNode, TEdge edgeCost)
        {
            Edge<TNodeKey, TNode, TEdge> edge = new Edge<TNodeKey, TNode, TEdge>(this, endNode, edgeCost);
            NodeConnection<TNodeKey, TNode, TEdge> connection = FindEdge(endNode.Key);
            connection.Edge = edge;
        }

        public TEdge RemoveEdge(TNodeKey nodeKey)
        {
            NodeConnection<TNodeKey, TNode, TEdge> edge = FindEdge(nodeKey);
            edges.Remove(nodeKey);
            return edge.Edge.EdgeCost;
        }

        public void RemoveAllEdges()
        {
            edges.Clear();
        }

        public NodeConnection<TNodeKey, TNode, TEdge> FindEdge(TNodeKey nodeKey)
        {
            foreach (KeyValuePair<TNodeKey, NodeConnection<TNodeKey, TNode, TEdge>> pair in edges)
                if (pair.Value.EndNode.Key.Equals(nodeKey))
                    return pair.Value;
            throw new KeyNotFoundException();
        }

        public bool ExistsEdge(TNodeKey nodeKey)
        {
            return edges.Keys.Contains(nodeKey);
        }

        public void UpdateNode(TNode newNode)
        {
            Value = newNode;
        }

        #region IEnumerable<NodeConnection<TNodeKey,TNode,TEdge>> Members

        public IEnumerator<NodeConnection<TNodeKey, TNode, TEdge>> GetEnumerator()
        {
            foreach (KeyValuePair<TNodeKey, NodeConnection<TNodeKey, TNode, TEdge>> neighbour in edges)
                yield return neighbour.Value;
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public override bool Equals(object obj)
        {
            Node<TNodeKey, TNode, TEdge> node = obj as Node<TNodeKey, TNode, TEdge>;
            if (node != null)
                return Key.Equals(node.Key);
            return false;
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }

    }
}
