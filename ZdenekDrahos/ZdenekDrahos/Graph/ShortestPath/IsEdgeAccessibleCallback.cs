﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.Graph.ShortestPath
{
    public delegate bool IsEdgeAccessibleCallback<TNodeKey, TNode, TEdge>(Edge<TNodeKey, TNode, TEdge> edge);


    public class DefaultEdgeAccesibility<TNodeKey, TNode, TEdge>
    {
        public static bool NoPathRestriction(Edge<TNodeKey, TNode, TEdge> edge)
        {
            return true;
        }
    }
}
