﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using ZdenekDrahos.PriorityQueue;
using ZdenekDrahos.PriorityQueue.PairingHeap;

namespace ZdenekDrahos.Graph.ShortestPath.Dijkstra
{
    class DijkstraNodes<TNodeKey, TNode, TEdge>
    {

        private IList<TNodeKey> visitedNodes;
        private DistancesComparer distancesComparer;
        private NodeRatings<TNodeKey, TNode, TEdge> nodeRatings;
        private IPriorityQueue<double, Node<TNodeKey, TNode, TEdge>> queue;

        public DijkstraNodes()
        {
            distancesComparer = new DistancesComparer();
            visitedNodes = new List<TNodeKey>();
            nodeRatings = new NodeRatings<TNodeKey, TNode, TEdge>();
            queue = new PairingHeap<double, Node<TNodeKey, TNode, TEdge>>();
            queue.PriorityProvider = nodeRatings.PriorityProvider;
        }

        public void Init(Node<TNodeKey, TNode, TEdge> startNode)
        {
            nodeRatings.Clear();
            visitedNodes.Clear();
            queue.Clear();
            AddNode(startNode);
        }

        public void AddInitialRatingToStartNode(TNodeKey key)
        {
            nodeRatings.AddRating(key, 0);
        }

        public void AddInitialRating(TNodeKey key)
        {
            nodeRatings.AddRating(key, double.MaxValue);
        }

        public double GetPathCost(TNodeKey endNode)
        {
            return nodeRatings[endNode];
        }

        public bool AreNodesInQueue()
        {
            return queue.Count > 0;
        }

        public Node<TNodeKey, TNode, TEdge> RemoveNodeWithSmallestDistance()
        {
            return queue.Dequeue();
        }

        public bool ExistsShorterPath(TNodeKey nodeKey)
        {
            return distancesComparer.ExistsShorterPathBetweenNodes();
        }

        public void AddOrUpdateNode(Node<TNodeKey, TNode, TEdge> node)
        {
            nodeRatings.AddRating(node.Key, distancesComparer.GetNewDistanceOfEndNode());
            if (!visitedNodes.Contains(node.Key))
                AddNode(node);
            else
                queue.ChangePriority(node);
        }

        private void AddNode(Node<TNodeKey, TNode, TEdge> node)
        {
            queue.Enqueue(node);
            visitedNodes.Add(node.Key);
        }

        public bool ExistsEdgeBetweenNodes(TNodeKey nodeKey, double distance)
        {
            SetStartEdgeToDistanceComparer(nodeKey, distance);
            return distancesComparer.ExistsEdgeBetweenNodes();
        }

        public void SetStartNodeToDistanceComparer()
        {
            distancesComparer.SetStartNodeDistance(nodeRatings[queue.Max().Key]);
        }

        public void SetEndNodeToDistanceComparer(TNodeKey key)
        {
            distancesComparer.SetEndNodeDistance(nodeRatings[key]);
        }

        public void SetEndEdgeToDistanceComparer(TNodeKey nodeKey, double distance)
        {            
            distancesComparer.SetEndEdge(nodeRatings[nodeKey], distance);
        }

        public void SetStartEdgeToDistanceComparer(TNodeKey nodeKey, double distance)
        {
            distancesComparer.SetStartEdge(nodeRatings[nodeKey], distance);
        }
    }
}
