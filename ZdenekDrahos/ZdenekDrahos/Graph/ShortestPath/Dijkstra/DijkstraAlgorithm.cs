﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Graph.ShortestPath.Helpers;

namespace ZdenekDrahos.Graph.ShortestPath.Dijkstra
{
    public class DijkstraAlgorithm<TNodeKey, TNode, TEdge> : IShortestPath<TNodeKey, TNode, TEdge>
    {
        private bool isOutdated;
        private TNodeKey recentStartNode;
        private DijkstraNodes<TNodeKey, TNode, TEdge> nodes;
        private PathEdges<TNodeKey, TNode, TEdge> outputPath;
        private IsEdgeAccessibleCallback<TNodeKey, TNode, TEdge> isEdgeAccessible;

        public DijkstraAlgorithm(IGraph<TNodeKey, TNode, TEdge> graph)
        {
            Graph = graph;
            isOutdated = false;
            nodes = new DijkstraNodes<TNodeKey, TNode, TEdge>();
            outputPath = new PathEdges<TNodeKey, TNode, TEdge>();
            IsEdgeAccessible = DefaultEdgeAccesibility<TNodeKey, TNode, TEdge>.NoPathRestriction;
            DistanceCalculator = DefaultDistanceCalculator<TNodeKey, TNode, TEdge>.ParseDoubleDistanceCalculator;
        }

        #region IShortestPath<TNodeKey,TNode,TEdge> Members

        public IGraph<TNodeKey, TNode, TEdge> Graph { get; set; }

        public IsEdgeAccessibleCallback<TNodeKey, TNode, TEdge> IsEdgeAccessible
        {
            get { return isEdgeAccessible; }
            set
            {
                isEdgeAccessible = value;
                OnGraphChanged(Graph, EventArgs.Empty);
            }
        }

        public DistanceCalculatorCallback<TNodeKey, TNode, TEdge> DistanceCalculator { get; set; }

        public Path<TNode> GetShortestPath(TNodeKey startNode, TNodeKey nodeEnd)
        {
            CheckObjectState();
            if (IsPathOutdated(startNode))
                FindShortestPathsFromNode(startNode);
            outputPath.Clear();
            ReconstructPathToNode(nodeEnd);
            return outputPath.GetPath(nodes.GetPathCost(nodeEnd));
        }

        public void OnGraphChanged(object sender, EventArgs e)
        {
            Graph = sender as IGraph<TNodeKey, TNode, TEdge>;
            isOutdated = true;
        }

        #endregion

        private void CheckObjectState()
        {
            if (Graph == null || IsEdgeAccessible == null || DistanceCalculator == null)
                throw new InvalidOperationException();
        }

        private bool IsPathOutdated(TNodeKey startNode)
        {
            return isOutdated || recentStartNode == null || !recentStartNode.Equals(startNode);
        }

        private void FindShortestPathsFromNode(TNodeKey startNode)
        {
            isOutdated = false;
            recentStartNode = startNode;
            InsertStartNode();
            RateNodes();
            FindPath();
        }

        private void InsertStartNode()
        {
            Node<TNodeKey, TNode, TEdge> startNode = Graph.FindDevNode(recentStartNode);
            nodes.Init(startNode);
        }

        private void RateNodes()
        {
            foreach (TNodeKey key in Graph.NodeKeysIterator())
                nodes.AddInitialRating(key);
            nodes.AddInitialRatingToStartNode(recentStartNode);
        }

        private void FindPath()
        {
            while (nodes.AreNodesInQueue())
                ProcessNodeWithSmallestDistance();
        }

        private void ProcessNodeWithSmallestDistance()
        {
            nodes.SetStartNodeToDistanceComparer();
            Node<TNodeKey, TNode, TEdge> node = nodes.RemoveNodeWithSmallestDistance();
            ProcessNodeNeighbours(node);
        }

        private void ProcessNodeNeighbours(Node<TNodeKey, TNode, TEdge> node)
        {
            foreach (NodeConnection<TNodeKey, TNode, TEdge> neighbour in node)
                if (IsEdgeAccessible(neighbour.Edge))
                    ProcessAccessibleNode(neighbour);
        }

        private void ProcessAccessibleNode(NodeConnection<TNodeKey, TNode, TEdge> connection)
        {
            nodes.SetEndEdgeToDistanceComparer(connection.EndNode.Key, DistanceCalculator(connection.Edge));
            if (nodes.ExistsShorterPath(connection.EndNode.Key))            
                nodes.AddOrUpdateNode(connection.EndNode);
        }

        private void ReconstructPathToNode(TNodeKey endNode)
        {
            Stack<Node<TNodeKey, TNode, TEdge>> path = new Stack<Node<TNodeKey, TNode, TEdge>>();
            Node<TNodeKey, TNode, TEdge> end = Graph.FindDevNode(endNode);
            path.Push(end);
            while (!path.Peek().Key.Equals(recentStartNode))
            {
                NodeConnection<TNodeKey, TNode, TEdge> neighbour = FindNeighbour(path.Peek());
                outputPath.AddFirst(neighbour.EndNode.Value);
                path.Push(neighbour.EndNode);
            }
            outputPath.AddLast(end.Value);
        }

        private NodeConnection<TNodeKey, TNode, TEdge> FindNeighbour(Node<TNodeKey, TNode, TEdge> node)
        {
            nodes.SetEndNodeToDistanceComparer(node.Key);
            foreach (NodeConnection<TNodeKey, TNode, TEdge> neighbour in node)
                if (IsNeighbourInPath(neighbour))
                    return neighbour;
            throw new PathNotFoundException();
        }

        private bool IsNeighbourInPath(NodeConnection<TNodeKey, TNode, TEdge> neighbour)
        {
            if (IsEdgeAccessible(neighbour.Edge))
                return nodes.ExistsEdgeBetweenNodes(neighbour.EndNode.Key, DistanceCalculator(neighbour.Edge));
            return false;
        }

    }
}
