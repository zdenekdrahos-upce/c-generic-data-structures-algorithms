﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.Graph.ShortestPath.Dijkstra
{
    public class NodeRatings<TNodeKey, TNode, TEdge>
    {

        private IDictionary<TNodeKey, double> ratings;

        public NodeRatings()
        {
            ratings = new Dictionary<TNodeKey, double>();
        }

        public void Clear()
        {
            ratings.Clear();
        }

        public void AddRating(TNodeKey key, double rating)
        {
            if (ratings.ContainsKey(key))
                ratings[key] = rating;
            else
                ratings.Add(key, rating);
        }

        public double this[TNodeKey key]
        {
            get { return ratings[key]; }
            set { throw new NotSupportedException(); }
        }

        public double PriorityProvider(Node<TNodeKey, TNode, TEdge> element)
        {
            return ratings.ContainsKey(element.Key) ? ratings[element.Key] : double.MaxValue;
        }
    }
}
