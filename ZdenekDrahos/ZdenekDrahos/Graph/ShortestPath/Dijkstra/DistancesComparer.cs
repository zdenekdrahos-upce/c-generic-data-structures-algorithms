﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.Graph.ShortestPath.Dijkstra
{
    class DistancesComparer
    {

        private double start, end, edge;

        public DistancesComparer()
        {
            start = 0;
            end = 0;
            edge = 0;
        }

        public void SetStartNodeDistance(double startNodeDistance)
        {
            start = startNodeDistance;
        }

        public void SetEndNodeDistance(double endNodeDistance)
        {
            end = endNodeDistance;
        }

        public void SetStartEdge(double startNodeDistance, double edgeDistance)
        {
            start = startNodeDistance;
            edge = edgeDistance;
        }

        public void SetEndEdge(double endNodeDistance, double edgeDistance)
        {
            end = endNodeDistance;
            edge = edgeDistance;
        }

        public bool ExistsEdgeBetweenNodes()
        {
            return edge == end - start;
        }

        public bool ExistsShorterPathBetweenNodes()
        {
            return end - start > edge;
        }

        public double GetNewDistanceOfEndNode()
        {
            return start + edge;
        }

    }
}
