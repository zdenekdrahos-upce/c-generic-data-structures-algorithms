﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.Graph.ShortestPath
{

    public delegate double DistanceCalculatorCallback<TNodeKey, TNode, TEdge>(Edge<TNodeKey, TNode, TEdge> edge);


    public class DefaultDistanceCalculator<TNodeKey, TNode, TEdge>
    {

        public static double ParseDoubleDistanceCalculator(Edge<TNodeKey, TNode, TEdge> edge)
        {
            double result = double.MaxValue;
            double.TryParse(edge.EdgeCost.ToString(), out result);
            return result;
        }

    }

}
