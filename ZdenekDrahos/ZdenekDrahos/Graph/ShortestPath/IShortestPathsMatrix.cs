﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using ZdenekDrahos.Graph.ShortestPath.Floyd;

namespace ZdenekDrahos.Graph.ShortestPath
{
    public interface IShortestPathsMatrix<TNodeKey, TNode, TEdge> : IShortestPath<TNodeKey, TNode, TEdge>
    {

        PathMatrix<TNodeKey, TNode, TEdge> PathMatrix { get; set; }

        void CreateShortestPathsMatrix(IGraph<TNodeKey, TNode, TEdge> graph);

    }
}
