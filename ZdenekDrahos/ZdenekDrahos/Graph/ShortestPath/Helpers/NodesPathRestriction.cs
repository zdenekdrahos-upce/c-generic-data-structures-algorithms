﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZdenekDrahos.Graph.ShortestPath
{
    public class NodesPathRestriction<TNodeKey, TNode, TEdge>
    {
        public IList<TNode> DisabledNodes { get; set; }

        public NodesPathRestriction()
        {
            DisabledNodes = new List<TNode>();
        }

        public bool IsEdgeAccessible(Edge<TNodeKey, TNode, TEdge> edge)
        {
            if (IsRestrictionActive())
                return IsNodeAccessible(edge.StartNode) && IsNodeAccessible(edge.EndNode);
            else
                return true;
        }

        private bool IsRestrictionActive()
        {
            return DisabledNodes != null && DisabledNodes.Count > 0;
        }

        private bool IsNodeAccessible(TNode node)
        {
            return !DisabledNodes.Contains(node);
        }
    }
}
