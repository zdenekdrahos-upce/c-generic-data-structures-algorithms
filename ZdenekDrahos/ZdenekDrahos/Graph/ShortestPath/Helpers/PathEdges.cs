﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZdenekDrahos.Graph.ShortestPath.Helpers
{
    class PathEdges<TNodeKey, TNode, TEdge>
    {

        private IList<TNode> path;

        public void Clear()
        {
            path = new List<TNode>();
        }

        public void AddFirst(TNode node)
        {
            path.Insert(0, node);
        }

        public void AddLast(TNode node)
        {
            path.Add(node);
        }

        public Path<TNode> GetPath(double cost)
        {
            return new Path<TNode>(cost, path);
        }

    }
}
