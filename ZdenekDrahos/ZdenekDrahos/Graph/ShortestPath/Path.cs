﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;

namespace ZdenekDrahos.Graph.ShortestPath
{
    public struct Path<TNode> : IEnumerable<TNode>
    {

        public readonly double Cost;
        public readonly IList<TNode> Nodes;

        public Path(double cost, IList<TNode> nodes)
        {
            Cost = cost;
            Nodes = nodes;
        }


        #region IEnumerable<TNode> Members

        public IEnumerator<TNode> GetEnumerator()
        {
            if (Nodes != null)
                foreach (TNode node in Nodes)
                    yield return node;
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
