﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZdenekDrahos.Graph.ShortestPath.Helpers;

namespace ZdenekDrahos.Graph.ShortestPath.Floyd
{
    public class FloydAlgorithm<TNodeKey, TNode, TEdge> : IShortestPathsMatrix<TNodeKey, TNode, TEdge>
    {
        private double pathCost;
        private PathMatrix<TNodeKey, TNode, TEdge> pathMatrix;
        private PathReconstructionHelper pathReconstruction;
        private PathEdges<TNodeKey, TNode, TEdge> outputPath;   
        private AdjacencyMatrixBuilder<TNodeKey, TNode, TEdge> adjacencyMatrixBuilder;

        public FloydAlgorithm()
        {
            pathReconstruction = new PathReconstructionHelper();
            outputPath = new PathEdges<TNodeKey, TNode, TEdge>();
            adjacencyMatrixBuilder = new AdjacencyMatrixBuilder<TNodeKey, TNode, TEdge>();
            DistanceCalculator = DefaultDistanceCalculator<TNodeKey, TNode, TEdge>.ParseDoubleDistanceCalculator;
        }

        #region IShortestPathsMatrix<TNodeKey,TNode,TEdge> Members

        public PathMatrix<TNodeKey, TNode, TEdge> PathMatrix {
            get
            {
                return pathMatrix;
            }
            set
            {
                pathMatrix = value;
                pathReconstruction.SetMatrices(value.DistanceMatrix, value.PredecessorMatrix);
            }
        }

        public IGraph<TNodeKey, TNode, TEdge> Graph { get; set; }

        public IsEdgeAccessibleCallback<TNodeKey, TNode, TEdge> IsEdgeAccessible {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        public DistanceCalculatorCallback<TNodeKey, TNode, TEdge> DistanceCalculator
        {
            get { return adjacencyMatrixBuilder.DistanceCalculator; }
            set { adjacencyMatrixBuilder.DistanceCalculator = value; }
        }

        public void CreateShortestPathsMatrix(IGraph<TNodeKey, TNode, TEdge> graph)
        {
            Graph = graph;
            BuildAdjacencyMatrix();
            CheckObjectState();
            BuildDistanceMatrix();
        }

        public Path<TNode> GetShortestPath(TNodeKey startNode, TNodeKey endNode)
        {
            CheckObjectState();
            ReconstructPath(startNode, endNode);
            outputPath.Clear();
            BuildPath(startNode);
            return outputPath.GetPath(pathCost);
        }

        public void OnGraphChanged(object sender, EventArgs e)
        {
            throw new NotSupportedException();
        }

        #endregion

        private void CheckObjectState()
        {
            if (Graph == null || DistanceCalculator == null || PathMatrix == null)
                throw new InvalidOperationException();
        }

        private void BuildAdjacencyMatrix()
        {
            adjacencyMatrixBuilder.LoadGraphDistanceMatrix(Graph);
            PathMatrix = new PathMatrix<TNodeKey, TNode, TEdge>(adjacencyMatrixBuilder.AdjacencyMatrix, adjacencyMatrixBuilder.IndexKeyMap);
        }

        private void BuildDistanceMatrix()
        {
            int nodesCount = Graph.Count;
            for (int node = 0; node < nodesCount; node++)
                for (int row = 0; row < nodesCount; row++)
                    for (int column = 0; column < nodesCount; column++)
                        PathMatrix.MinimizeDistance(node, row, column);
        }

        private void ReconstructPath(TNodeKey startNode, TNodeKey endNode)
        {
            int indexStart = PathMatrix.GetIndexOfKey(startNode);
            int indexEnd = PathMatrix.GetIndexOfKey(endNode);            
            pathReconstruction.ReconstructPath(indexStart, indexEnd);
            pathCost = PathMatrix.DistanceMatrix[indexStart, indexEnd];
        }

        private void BuildPath(TNodeKey startNodeKey)
        {
            if (pathReconstruction.ExistsNonEmptyPath())
                try
                {
                    BuildOutputPath(startNodeKey);
                }
                catch (KeyNotFoundException) // isolated key
                {                    
                    throw new PathNotFoundException();
                }
        }

        private void BuildOutputPath(TNodeKey startNodeKey)
        {
            TNode node;
            foreach (int index in pathReconstruction.NodesInPath)
            {
                node = Graph.FindNode(PathMatrix.IndexKeyMap[index]);
                outputPath.AddLast(node);
            }
        }
    }
}
