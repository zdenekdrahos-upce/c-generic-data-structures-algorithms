﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZdenekDrahos.Graph.ShortestPath.Floyd
{
    public class PathMatrix<TNodeKey, TNode, TEdge>
    {
        public double[,] DistanceMatrix { get; private set; }
        public double[,] PredecessorMatrix { get; private set; }
        public IDictionary<int, TNodeKey> IndexKeyMap { get; private set; }

        public PathMatrix(double[,] adjacencyMatrix, IDictionary<int, TNodeKey> indexKeyMap)
        {
            IndexKeyMap = indexKeyMap;
            InitRouteMatrices(adjacencyMatrix);
        }

        public PathMatrix(double[,] distanceMatrix, double[,] predecessorMatrix, IDictionary<int, TNodeKey> indexKeyMap)
        {
            DistanceMatrix = distanceMatrix;
            PredecessorMatrix = predecessorMatrix;
            IndexKeyMap = indexKeyMap;
        }

        private void InitRouteMatrices(double[,] adjacencyMatrix)
        {
            int nodesCount = adjacencyMatrix.GetLength(0);
            DistanceMatrix = new double[nodesCount,nodesCount];
            PredecessorMatrix = new double[nodesCount, nodesCount];
            for (int row = 0; row < nodesCount; row++)
            {
                for (int column = 0; column < nodesCount; column++)
                {
                    DistanceMatrix[row,column] = adjacencyMatrix[row,column];
                    PredecessorMatrix[row,column] = -1;
                }
            }
        }

        public void MinimizeDistance(int node, int row, int column)
        {
            if (DistanceMatrix[row,node] == double.MaxValue || DistanceMatrix[node,column] == double.MaxValue)
                return;
            if (DistanceMatrix[row,node] + DistanceMatrix[node,column] < DistanceMatrix[row,column])
            {
                DistanceMatrix[row,column] = DistanceMatrix[row,node] + DistanceMatrix[node,column];
                PredecessorMatrix[row,column] = node;
            }
        }

        public int GetIndexOfKey(TNodeKey key)
        {
            if (IndexKeyMap.Values.Any(x => x.Equals(key)))
                return IndexKeyMap.Where(pair => pair.Value.Equals(key)).Select(pair => pair.Key).First();
            else
                throw new KeyNotFoundException();
        }

    }
}
