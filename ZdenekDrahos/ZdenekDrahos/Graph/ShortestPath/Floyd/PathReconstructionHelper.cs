﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZdenekDrahos.Graph.ShortestPath.Floyd
{
    class PathReconstructionHelper
    {
        public IList<int> NodesInPath {get; private set;}
        private double[,] distanceMatrix, predecessorMatrix;

        public PathReconstructionHelper()
        {
            NodesInPath = new List<int>();
        }

        public void SetMatrices(double[,] distanceMatrix, double[,] predecessorMatrix)
        {
            this.distanceMatrix = distanceMatrix;
            this.predecessorMatrix = predecessorMatrix;
        }

        public bool ExistsNonEmptyPath()
        {
            return NodesInPath.Count > 0;
        }

        public void ReconstructPath(int indexStart, int indexEnd)
        {
            NodesInPath.Clear();
            NodesInPath.Add(indexStart);
            if (indexStart != indexEnd)
                BuildPath(indexStart, indexEnd);
        }

        private void BuildPath(int indexStart, int indexEnd)
        {
            string path = FindPath(indexStart, indexEnd);
            ParsePath(path, indexEnd);
        }

        private String FindPath(int indexStart, int indexEnd)
        {
            if (EdgeNotExists(indexStart, indexEnd))
                throw new PathNotFoundException();
            else
                return FindEdge(indexStart, indexEnd);   
        }

        private bool EdgeNotExists(int indexStart, int indexEnd)
        {
            return distanceMatrix[indexStart,indexEnd] == double.MaxValue;
        }

        private string FindEdge(int indexStart, int indexEnd)
        {
            double intermediate = predecessorMatrix[indexStart,indexEnd];
            if (intermediate == -1)
                return " ";// nodes are neighbours connected by edge
            else
                return FindPath(indexStart, (int)intermediate) + intermediate + FindPath((int)intermediate, indexEnd);
        }

        private void ParsePath(string path, int indexEnd)
        {
            foreach (string node in path.Split(' '))
                if (!string.IsNullOrEmpty(node))
                    NodesInPath.Add(int.Parse(node));
            NodesInPath.Add(indexEnd);
        }


    }
}
