﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZdenekDrahos.Graph.ShortestPath.Floyd
{
    class AdjacencyMatrixBuilder<TNodeKey, TNode, TEdge>
    {
        public double[,] AdjacencyMatrix { get; private set; }
        public IDictionary<int, TNodeKey> IndexKeyMap { get; private set; }
        public DistanceCalculatorCallback<TNodeKey, TNode, TEdge> DistanceCalculator { get; set; }

        private IGraph<TNodeKey, TNode, TEdge> graph;
        private int nodesCount;

        public void LoadGraphDistanceMatrix(IGraph<TNodeKey, TNode, TEdge> graph)
        {
            LoadGraph(graph);
            InitKeyIndexMap();
            InitAdjacencyMatrix();
            LoadDirectDistances();
        }

        private void LoadGraph(IGraph<TNodeKey, TNode, TEdge> graph)
        {
            if (graph == null)
                throw new ArgumentException();
            this.graph = graph;
            nodesCount = graph.Count;
        }

        private void InitKeyIndexMap()
        {
            IndexKeyMap = new Dictionary<int, TNodeKey>(graph.Count);
            int index = 0;
            foreach (TNodeKey key in graph.NodeKeysIterator())
                IndexKeyMap.Add(index++, key);
        }

        private void InitAdjacencyMatrix()
        {
            AdjacencyMatrix = new double[nodesCount,nodesCount];
        }

        private void LoadDirectDistances()
        {
            for (int row = 0; row < nodesCount; row++)
                for (int column = 0; column < nodesCount; column++)
                    AdjacencyMatrix[row,column] = GetDistanceBetweenNodes(row, column);
        }

        private double GetDistanceBetweenNodes(int startNode, int endNode)
        {
            if (startNode == endNode)
                return 0;
            try
            {
                Node<TNodeKey, TNode, TEdge> start = graph.FindDevNode(IndexKeyMap[startNode]);
                NodeConnection<TNodeKey, TNode, TEdge> neigh = start.FindEdge(IndexKeyMap[endNode]);
                return DistanceCalculator(neigh.Edge);
            }
            catch (KeyNotFoundException)
            {
                return double.MaxValue; //nodes are not connected
            }
        }

    }
}
