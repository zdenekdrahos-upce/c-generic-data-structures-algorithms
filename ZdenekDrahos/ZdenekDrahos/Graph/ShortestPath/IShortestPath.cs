﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Graph.ShortestPath
{
    public interface IShortestPath<TNodeKey, TNode, TEdge>
    {
        IGraph<TNodeKey, TNode, TEdge> Graph { get; set; }
        IsEdgeAccessibleCallback<TNodeKey, TNode, TEdge> IsEdgeAccessible { get; set; }
        DistanceCalculatorCallback<TNodeKey, TNode, TEdge> DistanceCalculator { get; set; }

        Path<TNode> GetShortestPath(TNodeKey startNode, TNodeKey endNode);

        void OnGraphChanged(object sender, EventArgs e);

    }
}
