﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;

namespace ZdenekDrahos.Table
{
    public interface ITable<TKey, TElement> : IEnumerable<KeyValuePair<TKey, TElement>>
    {
        ICollection<TKey> Keys { get; }

        void Clear();

        bool IsEmpty();

        int Count { get; }

        void Add(TKey key, TElement element);

        TElement Remove(TKey key);

        TElement Find(TKey key);

    }
}
