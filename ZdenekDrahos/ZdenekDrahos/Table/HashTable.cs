﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.Table
{
    public class HashTable<K, E> : ITable<K, E>
    {
        private IDictionary<K, E> hashTable;

        public HashTable()
        {
            hashTable = new Dictionary<K, E>();
        }

        #region ITable<K,E> Members

        public ICollection<K> Keys {
            get { return hashTable.Keys; }
            private set { }
        }

        public void Clear()
        {
            hashTable.Clear();
        }

        public bool IsEmpty()
        {
            return Count == 0;
        }

        public int Count
        {
            get { return hashTable.Count; }
            private set { }
        }

        public void Add(K key, E element)
        {
            if (hashTable.ContainsKey(key))
                throw new ArgumentException("key");
            hashTable.Add(key, element);
        }

        public E Remove(K key)
        {
            if (!hashTable.ContainsKey(key))
                throw new KeyNotFoundException();
            E removedElement = hashTable[key];
            if (hashTable.Remove(key))
                return removedElement;
            else
                throw new Exception("Element was not removed.");
        }

        public E Find(K key)
        {
            if (!hashTable.ContainsKey(key))
                throw new KeyNotFoundException();
            return hashTable[key];
        }

        #endregion

        #region IEnumerable<KeyValuePair<K,E>> Members

        public IEnumerator<KeyValuePair<K, E>> GetEnumerator()
        {
            foreach (KeyValuePair<K, E> item in hashTable)
                yield return item;
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
