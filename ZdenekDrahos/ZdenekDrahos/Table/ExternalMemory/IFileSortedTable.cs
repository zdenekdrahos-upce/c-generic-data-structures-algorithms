﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Files.Block.Sorted;

namespace ZdenekDrahos.Table.ExternalMemory
{
    // TKey & TValue must be serializable
    public interface IFileSortedTable<TKey, TValue> where TKey : IComparable<TKey>
    {

        SortedBlockFile<TKey, TValue> File { get; }

        void Clear();

        bool IsEmpty();

        int Count { get; }

        int BlockSize { get; } 

        int RecordsInBlockCount { get; }

        Func<TKey, double> KeyNumberConverter { get; set; }

        void Build(IEnumerable<KeyValuePair<TKey, TValue>> values);

        TValue Remove(TKey key);

        TValue FindBinarySearch(TKey key);

        TValue FindInterpolationSearch(TKey key);

    }
}
