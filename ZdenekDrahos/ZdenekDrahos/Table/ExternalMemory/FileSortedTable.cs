﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Files.Block.Sorted;

namespace ZdenekDrahos.Table.ExternalMemory
{
    class FileSortedTable<TKey, TValue> : IFileSortedTable<TKey, TValue> where TKey : IComparable<TKey>
    {

        public FileSortedTable(string pathToBinaryFile, int recordsInBlock, int blockSize)
        {
            Count = 0;
            File = new SortedBlockFile<TKey, TValue>(pathToBinaryFile, recordsInBlock, blockSize);
        }

        public FileSortedTable(SortedBlockFile<TKey, TValue> file)
        {
            Count = file.GetNumberOfRecords();
            File = file;
        }

        #region IFileSortedTable<TKey,TValue> Members

        public SortedBlockFile<TKey, TValue> File { get; private set; }

        public void Clear()
        {
            File.BlockFile.RemoveAllBlocks();
            Count = 0;
        }

        public bool IsEmpty()
        {
            return Count == 0;
        }

        public int Count { get; private set; }

        public int BlockSize
        {
            get { return File.BlockFile.ControlBlock.BlockSize; }
            private set { }
        }

        public int RecordsInBlockCount
        {
            get { return File.BlockFile.ControlBlock.RecordsInBlockCount; }
            private set { }
        }

        public Func<TKey, double> KeyNumberConverter
        {
            get { return File.KeyNumberConverter; }
            set { File.KeyNumberConverter = value; }
        }

        public void Build(IEnumerable<KeyValuePair<TKey, TValue>> values)
        {
            Count = File.Build(values);
        }

        public TValue Remove(TKey key)
        {
            TValue removed = File.Remove(key);
            Count--;
            return removed;
        }

        public TValue FindBinarySearch(TKey key)
        {
            return File.BinarySearch(key);
        }

        public TValue FindInterpolationSearch(TKey key)
        {
            return File.InterpolationSearch(key);
        }

        #endregion
    }
}
