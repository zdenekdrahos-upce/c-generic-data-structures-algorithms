﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.Table
{
    public class ListTable<K, E> : ITable<K, E>
    {
        private LinkedList<KeyValuePair<K, E>> list;
        private ICollection<K> keys;

        public ListTable()
        {
            list = new LinkedList<KeyValuePair<K, E>>();
            keys = new List<K>();
        }

        #region ITable<K,E> Members

        public ICollection<K> Keys
        {
            get { return keys; }
            private set { }
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool IsEmpty()
        {
            return Count == 0;
        }

        public int Count
        {
            get {
                return list.Count;
            }
            private set {}
        }

        public void Add(K key, E element)
        {
            if (Keys.Contains(key))
                throw new ArgumentException("key");
            if (list.Count > 0)
                list.AddAfter(list.Last, new KeyValuePair<K, E>(key, element));
            else
                list.AddFirst(new KeyValuePair<K, E>(key, element));
            keys.Add(key);
        }

        public E Remove(K key)
        {
            KeyValuePair<K, E> element = FindElement(key);
            list.Remove(element);
            keys.Remove(key);
            return element.Value;
        }

        public E Find(K key)
        {
            KeyValuePair<K, E> element = FindElement(key);
            return element.Value;
        }

        #endregion

        private KeyValuePair<K, E> FindElement(K key)
        {
            foreach (KeyValuePair<K, E> item in list)
                if (item.Key.Equals(key))
                    return item;
            throw new KeyNotFoundException();
        }

        #region IEnumerable<KeyValuePair<K,E>> Members

        public IEnumerator<KeyValuePair<K, E>> GetEnumerator()
        {
            foreach (KeyValuePair<K, E> item in list)
            {
                KeyValuePair<K, E> pair = new KeyValuePair<K, E>(item.Key, item.Value);
                yield return pair;
            }
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
