﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Search.SearchAlgorithms;

namespace ZdenekDrahos.Search.Array
{
    class ArraySearcher<TValue>
    {

        private TValue[] searchedArray;
        private Searcher<TValue> searcher;
        private Comparison<TValue> comparison;

        public ArraySearcher(Comparison<TValue> comparison)
        {
            this.comparison = comparison;
            searcher = new Searcher<TValue>();
            searcher.IsIndexEmpty = this.IsIndexEmpty;
            searcher.IsItemFound = this.IsItemFound;
            searcher.ExistsSmallerKey = this.ExistsSmallerKey;
            searcher.ExistsLargerKey = this.ExistsLargerKey;
        }

        public void SetSearchedArray(TValue[] array)
        {
            searchedArray = array;
            CheckArray();
            searcher.MinIndex = 0;
            searcher.MaxIndex = array.Length - 1;
        }

        public void SetBinarySearch()
        {
            ISearchAlgorithm<TValue> binarySearch = new BinarySearch<TValue>();
            SetSearchAlgorithm(binarySearch);
        }

        public void SetInterpolationSearch(Func<TValue, double> keyNumberConverter)
        {
            InterpolationSearch<TValue> interpolation = new InterpolationSearch<TValue>();
            interpolation.KeyNumberConverter = (x) => x != null ? keyNumberConverter(x) : 0;
            interpolation.GetMinKey = (minIndex) => searchedArray[minIndex];
            interpolation.GetMaxKey = (maxIndex) => searchedArray[maxIndex];
            SetSearchAlgorithm(interpolation);
        }

        public void SetSearchAlgorithm(ISearchAlgorithm<TValue> algorithm)
        {
            searcher.GetIndex = algorithm.GetIndex;
        }

        public TValue Search(TValue key)
        {
            int index = FindIndex(key);
            return searchedArray[index];
        }

        public int FindIndex(TValue key)
        {
            return searcher.Search(key);
        }

        private bool IsIndexEmpty(int index)
        {
            return searchedArray[index] == null;
        }

        private bool IsItemFound(TValue key, int index)
        {
            return comparison(key, searchedArray[index]) == 0;
        }

        private bool ExistsSmallerKey(TValue key, int index)
        {
            return comparison(key, searchedArray[index]) == -1;
        }

        private bool ExistsLargerKey(TValue key, int index)
        {
            return comparison(key, searchedArray[index]) == 1;
        }

        private void CheckArray()
        {
            if (searchedArray != null)
            {
                for (int i = 0; i < searchedArray.Length; i++)
                    if (!IsIndexEmpty(i))
                        return;
            }
            throw new ArgumentNullException("Searched array is empty");
        }

    }
}
