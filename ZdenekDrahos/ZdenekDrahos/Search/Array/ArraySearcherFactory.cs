﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Search.Array
{
    class ArraySearcherFactory<TValue>
    {

        public static ArraySearcher<TValue> BinarySearch(Comparison<TValue> comparison)
        {
            ArraySearcher<TValue> searcher = new ArraySearcher<TValue>(comparison);
            searcher.SetBinarySearch();
            return searcher;
        }

    }

}
