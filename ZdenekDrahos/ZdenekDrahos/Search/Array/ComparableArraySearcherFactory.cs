﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Search.Array
{
    class ComparableArraySearcherFactory<TValue> where TValue : IComparable<TValue> 
    {

        public static ArraySearcher<TValue> BinarySearch()
        {
            Comparison<TValue> comp = (x, y) => x.CompareTo(y);
            return ArraySearcherFactory<TValue>.BinarySearch(comp);
        }

    }

}
