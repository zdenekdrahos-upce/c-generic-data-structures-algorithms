﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Search.SearchAlgorithms
{
    interface ISearchAlgorithm<TKey>
    {

        int GetIndex(TKey searchedKey, int minIndex, int maxIndex);

    }
}
