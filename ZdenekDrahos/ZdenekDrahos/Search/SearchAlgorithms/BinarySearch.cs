﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Statistics.Median;

namespace ZdenekDrahos.Search.SearchAlgorithms
{
    class BinarySearch<TKey> : ISearchAlgorithm<TKey>
    {

        private MedianIndex median = new MedianIndex();

        #region ISearchAlgorithm<TKey> Members

        public int GetIndex(TKey searchedKey, int minIndex, int maxIndex)
        {
            median.StartIndex = minIndex;
            return median.GetMedianIndex(maxIndex - minIndex);
        }

        #endregion
    }
}
