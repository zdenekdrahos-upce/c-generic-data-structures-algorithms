﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Search.SearchAlgorithms
{
    class InterpolationSearch<TKey> : ISearchAlgorithm<TKey>
    {

        public InterpolationSearch() {}

        public InterpolationSearch(Func<TKey, double> converter, Func<int, TKey> minKey, Func<int, TKey> maxKey)
        {
            KeyNumberConverter = converter;
            GetMinKey = minKey;
            GetMaxKey = maxKey;
        }

        public Func<TKey, double> KeyNumberConverter { get; set; }
        public Func<int, TKey> GetMinKey { get; set; }
        public Func<int, TKey> GetMaxKey { get; set; }

        #region ISearchAlgorithm<TKey> Members

        public int GetIndex(TKey searchedKey, int minIndex, int maxIndex)
        {
            double distance = EstimateDistance(searchedKey, minIndex, maxIndex);
            double block = minIndex + (maxIndex - minIndex) * distance;
            return (int)Math.Floor(block);
        }

        #endregion

        private double EstimateDistance(TKey key, int minIndex, int maxIndex)
        {
            double lowestKey = GetLowestKey(minIndex);
            double highestKey = GetHighestKey(maxIndex);
            double distance = (KeyNumberConverter(key) - lowestKey) / (highestKey - lowestKey);
            return NormalizeDistance(distance);
        }

        private double GetLowestKey(int minIndex)
        {
            return GetKeyValue(GetMinKey, minIndex);
        }

        private double GetHighestKey(int maxIndex)
        {
            return GetKeyValue(GetMaxKey, maxIndex); ;
        }

        private double GetKeyValue(Func<int, TKey> getKey, int index)
        {
            TKey key = getKey(index);
            return KeyNumberConverter(key);
        }

        private double NormalizeDistance(double distance)
        {
            if (distance <= 0)
                return 0;
            else if (distance >= 1)
                return 1;
            else
                return distance;
        }
    }
}
