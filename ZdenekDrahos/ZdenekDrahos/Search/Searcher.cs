﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.Search
{
    class Searcher<TValue>
    {
        private int foundIndex;
        private bool isRangeSearched;

        public Searcher()
        {
            MinIndex = 0;
            MaxIndex = 0;
        }

        public int MinIndex { get; set; }
        public int MaxIndex { get; set; }

        public Func<int, bool> IsIndexEmpty;
        public Func<TValue, int, bool> IsItemFound;
        public Func<TValue, int, bool> ExistsSmallerKey;
        public Func<TValue, int, bool> ExistsLargerKey;
        public Func<TValue, int, int, int> GetIndex;

        public int Search(TValue key)
        {
            return Search(key, MinIndex, MaxIndex);
        }

        private int Search(TValue key, int min, int max)
        {
            CheckInputRange(min, max);
            FindIndex(key, min, max);
            CheckFoundIndex(min, max);
            if (IsItemFound(key, foundIndex))
                return foundIndex;
            else if (MoveToLeft(key))
                return Search(key, min, foundIndex - 1);
            else if (MoveToRight(key))
                return Search(key, foundIndex + 1, max);
            else
                throw new KeyNotFoundException();
        }

        private void CheckInputRange(int min, int max)
        {
            isRangeSearched = min != max;
            if (min < MinIndex || max > MaxIndex || max < min)
                throw new KeyNotFoundException();
        }

        private void FindIndex(TValue key, int min, int max)
        {
            if (isRangeSearched)
                foundIndex = GetIndex(key, min, max);
            else
                foundIndex = min;
        }

        private void CheckFoundIndex(int min, int max)
        {
            if (IsIndexEmpty(foundIndex))
            {
                if (!isRangeSearched)
                    throw new KeyNotFoundException();

                int currentIndex = foundIndex;
                while (ExistsLeftArea() && foundIndex > min)
                {
                    foundIndex--;
                    if (!IsIndexEmpty(foundIndex))
                        return;
                }
                foundIndex = currentIndex;
                while (ExistsRightArea() && foundIndex < max)
                {
                    foundIndex++;
                    if (!IsIndexEmpty(foundIndex))
                        return;
                }
                throw new ArgumentNullException("Searched structure is empty");
            }
        }

        private bool MoveToLeft(TValue key)
        {
            return ExistsLeftArea() && isRangeSearched && ExistsSmallerKey(key, foundIndex);
        }

        private bool ExistsLeftArea()
        {
            return (foundIndex - 1) >= MinIndex;
        }

        private bool MoveToRight(TValue key)
        {
            return ExistsRightArea() && isRangeSearched && ExistsLargerKey(key, foundIndex);
        }

        private bool ExistsRightArea()
        {
            return (foundIndex + 1) <= MaxIndex;
        }

    }
}
