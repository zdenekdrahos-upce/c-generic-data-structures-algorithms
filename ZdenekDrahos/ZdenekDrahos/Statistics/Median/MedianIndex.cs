﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Statistics.Median
{
    public class MedianIndex : IMedianIndex
    {

        public MedianIndex()
        {
            StartIndex = 0;
        }

        #region IMedianIndex Members

        public int StartIndex
        {
            get;
            set;
        }

        public int GetMedianIndex(int size)
        {
            if (size <= 0)
                throw new ArgumentException();
            int middleIndex = size / 2;
            return middleIndex + StartIndex;
        }

        #endregion
    }
}
