﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.Statistics.Median
{
    // values smaller than median have index < medianIndex, value larger than median have index >= medianIndex
    public interface IMedianIndex
    {

        int StartIndex { get; }

        int GetMedianIndex(int size);

    }
}
