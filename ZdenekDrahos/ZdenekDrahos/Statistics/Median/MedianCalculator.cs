﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace ZdenekDrahos.Statistics.Median
{
    public class MedianCalculator<T> : IMedian<T> where T : IComparable<T>
    {
        private int middleIndex;
        private List<T> listOfElements;

        #region IMedian<T> Members

        public IList<T> Median { get; private set; }

        public void SortAndFind(IEnumerable<T> elements)
        {
            LoadElements(elements);
            SortElements();
            CalculateMedian();
        }

        public void Find(IEnumerable<T> sortedElements)
        {
            LoadElements(sortedElements);
            CalculateMedian();
        }

        #endregion

        private void LoadElements(IEnumerable<T> elements)
        {
            listOfElements = new List<T>(elements);
            LoadMiddleIndex();
        }

        private void LoadMiddleIndex()
        {
            middleIndex = listOfElements.Count / 2;
        }

        private void SortElements()
        {
            listOfElements.Sort();
        }

        private void CalculateMedian()
        {
            Median = new List<T>(this.GetMedianSize());
            if (IsCountEven())
                Median.Add(GetElementBeforeMiddle());
            Median.Add(GetMiddleElement());
        }

        private int GetMedianSize()
        {
            return IsCountEven() ? 2 : 1;
        }

        private bool IsCountEven()
        {
            return listOfElements.Count % 2 == 0;
        }

        private T GetMiddleElement()
        {
            return listOfElements.ElementAt(middleIndex);
        }

        private T GetElementBeforeMiddle()
        {
            return listOfElements.ElementAt(middleIndex - 1);
        }

    }
}
