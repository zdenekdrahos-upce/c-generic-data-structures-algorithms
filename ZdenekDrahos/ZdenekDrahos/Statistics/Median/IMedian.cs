﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.Statistics.Median
{
    interface IMedian<T> where T : IComparable<T>
    {

        IList<T> Median { get; }

        void SortAndFind(IEnumerable<T> elements);

        void Find(IEnumerable<T> sortedElements);
    
    }
}
