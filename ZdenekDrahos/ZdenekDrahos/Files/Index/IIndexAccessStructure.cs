﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Files.Block;
using ZdenekDrahos.Tree.Tree23;

namespace ZdenekDrahos.Files.Index
{
    interface IIndexAccessStructure<TKey, TValue> where TKey : IComparable<TKey>
    {

        ITree23<TKey, KeyIndex> Index { get; }

        BlockFile<TKey, TValue> BlockFile { get; }

        event EventHandler ObsoleteValueInIndex;

        void Add(TKey key, TValue value);

        TValue Find(TKey key);

        TValue Remove(TKey key);

    }
}
