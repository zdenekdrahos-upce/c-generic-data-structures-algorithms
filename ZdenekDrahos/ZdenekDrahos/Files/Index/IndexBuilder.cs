﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Files.Block;
using ZdenekDrahos.Files.Block.Blocks;
using ZdenekDrahos.Tree.Tree23;
using ZdenekDrahos.Tree.Tree23.RedBlack;

namespace ZdenekDrahos.Files.Index
{
    class IndexBuilder<TKey, TValue> where TKey : IComparable<TKey>
    {

        public IIndexAccessStructure<TKey, TValue> BuildFileIndex(BlockFile<TKey, TValue> file)
        {
            List<Block<TKey, TValue>> blocks = file.ReadAll();
            ITree23<TKey, KeyIndex> redBlackTree = new RedBlackTree<TKey, KeyIndex>();

            for (int block = 0; block < blocks.Count; block++)
                for (int record = 0; record < file.ControlBlock.RecordsInBlockCount; record++)
                    if (blocks[block].FreeBlocks[record] == false)
                    {
                        TKey key = blocks[block].Records[record].Key;
                        KeyIndex value = new KeyIndex(block, record);
                        redBlackTree.Add(key, value);
                    }

            return BuildFileIndex(file, redBlackTree);
        }

        public IIndexAccessStructure<TKey, TValue> BuildFileIndex(BlockFile<TKey, TValue> file, ITree23<TKey, KeyIndex> tree23)
        {
            return new IndexAccessStructure<TKey, TValue>(file, tree23);
        }

    }
}
