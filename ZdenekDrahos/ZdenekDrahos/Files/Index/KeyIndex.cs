﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Files.Index
{
    [Serializable]
    public class KeyIndex
    {

        private int block;
        private int record;

        public KeyIndex(int block, int record)
        {
            Block = block;
            Record = record;
        }

        public int Block
        {
            get { return block; }
            set { block = value; }
        }

        public int Record
        {
            get { return record; }
            set { record = value; }
        }   

    }
}
