﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Files.Block;
using ZdenekDrahos.Files.Block.Records;
using ZdenekDrahos.Tree.Tree23;

namespace ZdenekDrahos.Files.Index
{
    class IndexAccessStructure<TKey, TValue> : IIndexAccessStructure<TKey, TValue> where TKey : IComparable<TKey>
    {

        public IndexAccessStructure(BlockFile<TKey, TValue> file, ITree23<TKey, KeyIndex> tree23)
        {
            Index = tree23;
            BlockFile = file;
        }

        public ITree23<TKey, KeyIndex> Index { get; private set; }
        public BlockFile<TKey, TValue> BlockFile { get; private set; }
        public event EventHandler ObsoleteValueInIndex;

        public void Add(TKey key, TValue value)
        {
            KeyIndex index = FindLocationForRecord();
            Index.Add(key, index);
            SaveRecord(index, key, value);
        }

        public TValue Find(TKey key)
        {
            return LoadRecord(key);
        }

        public TValue Remove(TKey key)
        {
            TValue removedValue = LoadRecord(key);
            BlockFile.Buffer.RemoveRecord(key);
            BlockFile.Save();
            Index.Remove(key);
            return removedValue;
        }

        private TValue LoadRecord(TKey key)
        {
            KeyIndex index = Index.Find(key);
            BlockFile.TryRead(index.Block);
            CheckObsoleteIndex(key, index);
            return BlockFile.Buffer[index.Record].Value;
        }

        private void CheckObsoleteIndex(TKey key, KeyIndex index)
        {
            if (IsRecordObsolete(key, index))
            {
                OnObsoleteIndexKey();
                index.Record = TryFindRecordInBlock(key);
            }
        }

        private bool IsRecordObsolete(TKey key, KeyIndex index)
        {
            return BlockFile.Buffer.Block.FreeBlocks[index.Record] ||
                    BlockFile.Buffer[index.Record].Key.CompareTo(key) != 0;
        }

        private int TryFindRecordInBlock(TKey key)
        {
            int newRecordIndex = BlockFile.Buffer.GetIndexOfKey(key);
            if (newRecordIndex == -1)
                throw new KeyNotFoundException();
            return newRecordIndex;
        }

        private KeyIndex FindLocationForRecord()
        {
            int currentBlock = BlockFile.Buffer.BlockIndex;
            KeyIndex keyIndex = FindLocationInExistingBlock(currentBlock);
            if (keyIndex == null)
            {
                int lastBlock = BlockFile.ControlBlock.AllocatedBlocksCount - 1;
                keyIndex = FindLocationInExistingBlock(lastBlock);
                if (keyIndex == null)
                    keyIndex = GetLocationInNewBlock();
            }
            return keyIndex;
        }

        private KeyIndex FindLocationInExistingBlock(int blockIndex)
        {
            try
            {
                BlockFile.TryRead(blockIndex);
                int recordIndex = BlockFile.Buffer.GetIndexOfFirstFreeBlock();
                return new KeyIndex(blockIndex, recordIndex);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        private KeyIndex GetLocationInNewBlock()
        {
            int blockIndex = BlockFile.ControlBlock.AllocatedBlocksCount;
            int recordIndex = 0;
            return new KeyIndex(blockIndex, recordIndex);
        }

        private void SaveRecord(KeyIndex index, TKey key, TValue value)
        {
            if (index.Block == BlockFile.ControlBlock.AllocatedBlocksCount)
            {
                BlockFile.AppendEmptyBlock();
                BlockFile.Buffer[index.Record] = new Record<TKey, TValue>(key, value);
                BlockFile.Write();
            }
            else
            {
                BlockFile.Buffer[index.Record] = new Record<TKey, TValue>(key, value);
                BlockFile.Modify();
            }
        }

        protected void OnObsoleteIndexKey()
        {
            EventHandler handler = ObsoleteValueInIndex;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

    }
}
