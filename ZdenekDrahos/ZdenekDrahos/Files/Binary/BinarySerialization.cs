﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ZdenekDrahos.Files.Binary
{
    class BinarySerialization
    {
        private BinaryFormatter binaryFormatter = new BinaryFormatter();

        public object Deserialize(byte[] bytes, object emptyObject)
        {
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                return binaryFormatter.Deserialize(ms);
            }
        }

        public byte[] Serialize(object serializedObject, int size)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                try
                {
                    byte[] byteArray = new byte[size];
                    if (serializedObject != null)
                    {
                        binaryFormatter.Serialize(ms, serializedObject);
                        byte[] serializedArray = ms.ToArray();
                        if (serializedArray.Length > size)
                            throw new ArgumentException(serializedArray.Length.ToString()); // record is too large
                        else 
                            Array.Copy(serializedArray, 0, byteArray, 0, serializedArray.Length);
                    }
                    return byteArray;
                }
                catch (SerializationException)
                {
                    throw new ArgumentException("Record cannot be serialized");
                }
            }
        }

        public int GetSizeOfSerializedObject(object serializedObject)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                try
                {
                    binaryFormatter.Serialize(ms, serializedObject);
                    return ms.ToArray().Length;
                }
                catch (SerializationException)
                {
                    throw new ArgumentException("Record cannot be serialized");
                }
            }
        }
    }
}
