﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.IO;
using System.Text;

namespace ZdenekDrahos.Files.Binary
{
    public class BinaryFile
    {

        public BinaryFile(string pathToBinaryFile)
        {
            PathToFile = pathToBinaryFile;
        }

        public string PathToFile { get; private set; }

        public bool Exists()
        {
            return File.Exists(PathToFile);
        }

        public void Delete()
        {
            File.Delete(PathToFile);
        }

        public byte[] Read(int position, int length)
        {
            using (FileStream fileStream = GetReadStream())
            {
                using (BinaryReader binaryReader = GetBinaryReader(fileStream))
                {
                    binaryReader.BaseStream.Position = position;
                    return Read(binaryReader, length);
                }
            }
        }

        public byte[] Read(BinaryReader reader, int length)
        {
            byte[] bytes = new byte[length];
            reader.Read(bytes, 0, length);
            return bytes;
        }

        public BinaryReader GetBinaryReader(FileStream fileStream)
        {
            return new BinaryReader(fileStream, Encoding.UTF8);
        }

        public FileStream GetReadStream()
        {
            return File.OpenRead(PathToFile);
        }

        public void Write(int position, byte[] bytes)
        {
            using (FileStream fileStream = GetWriteStream())
            {
                using (BinaryWriter binaryWriter = GetBinaryWriter(fileStream))
                {
                    binaryWriter.Seek(position, SeekOrigin.Begin);
                    binaryWriter.Write(bytes);
                }
            }
        }

        public BinaryWriter GetBinaryWriter(FileStream fileStream)
        {
            return new BinaryWriter(fileStream, Encoding.UTF8);
        }

        public FileStream GetWriteStream()
        {
            return File.Open(PathToFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
        }

    }
}
