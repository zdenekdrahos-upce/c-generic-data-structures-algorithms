﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Files.Block.Blocks
{
    public delegate void BlockReadEventHandler(object sender, BlockReadEventArgs e);

    public class BlockReadEventArgs : EventArgs
    {

        public BlockReadEventArgs(int block)
        {
            Block = block;
        }

        public int Block { get; private set; }

    }
}
