﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Files.Block.Records;

namespace ZdenekDrahos.Files.Block.Blocks
{
    [Serializable]
    public class Block<TKey, TValue>
    {

        public bool[] FreeBlocks;
        public Record<TKey, TValue>[] Records;

        public Block()
        {
            FreeBlocks = null;
            Records = null;
        }

        public Block(int recordsInBlockCount)
        {
            Records = new Record<TKey, TValue>[recordsInBlockCount];
            FreeBlocks = new bool[recordsInBlockCount];
            InitFreeBlocks();
        }

        private void InitFreeBlocks()
        {
            for (int i = 0; i < FreeBlocks.Length; i++)
                FreeBlocks[i] = true;
        }

    }
}
