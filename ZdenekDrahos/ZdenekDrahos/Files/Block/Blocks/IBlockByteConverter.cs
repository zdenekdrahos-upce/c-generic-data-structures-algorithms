﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Files.Block.Blocks
{
    public interface IBlockByteConverter<TKey, TValue>
    {
        ControlBlock CreateControlBlock(int recordsInBlock, int blockSize);

        Block<TKey, TValue> DeserializeBlock(byte[] bytes);

        ControlBlock DeserializeControlBlock(byte[] bytes);

        byte[] SerializeBlock(Block<TKey, TValue> block, ControlBlock controlBlock);

        byte[] SerializeControlBlock(ControlBlock controlBlock);
    }
}
