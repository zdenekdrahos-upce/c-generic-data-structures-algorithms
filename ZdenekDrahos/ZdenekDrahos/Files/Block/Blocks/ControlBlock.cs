﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Files.Block.Blocks
{
    [Serializable]
    public class ControlBlock
    {

        public int AllocatedBlocksCount;
        public int RecordsInBlockCount;
        public int BlockSize;
        public int ControlBlockSize;

        public ControlBlock() { }

        public ControlBlock(int recordsInBlock, int blockSize)
        {
            RecordsInBlockCount = recordsInBlock;
            BlockSize = blockSize;
            AllocatedBlocksCount = 0;
        }

    }
}
