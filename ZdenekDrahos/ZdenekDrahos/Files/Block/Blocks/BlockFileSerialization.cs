﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using ZdenekDrahos.Files.Binary;

namespace ZdenekDrahos.Files.Block.Blocks
{
    class BlockFileSerialization<TKey, TValue> : IBlockByteConverter<TKey, TValue>
    {

        private BinarySerialization serialization = new BinarySerialization();

        public byte[] SerializeBlock(Block<TKey, TValue> block, ControlBlock controlBlock)
        {
            return serialization.Serialize(block, controlBlock.BlockSize);
        }

        public byte[] SerializeControlBlock(ControlBlock controlBlock)
        {
            return serialization.Serialize(controlBlock, controlBlock.ControlBlockSize);
        }

        public Block<TKey, TValue> DeserializeBlock(byte[] bytes)
        {
            return (Block<TKey, TValue>)serialization.Deserialize(bytes, new Block<TKey, TValue>());
        }

        public ControlBlock DeserializeControlBlock(byte[] bytes)
        {
            return (ControlBlock)serialization.Deserialize(bytes, new ControlBlock());
        }

        public ControlBlock CreateControlBlock(int recordsInBlock, int blockSize)
        {
            ControlBlock controlBlock = new ControlBlock(recordsInBlock, blockSize);
            int emptyBlockSize = serialization.GetSizeOfSerializedObject(new Block<int, int>(recordsInBlock));
            controlBlock.ControlBlockSize = serialization.GetSizeOfSerializedObject(controlBlock);
            return controlBlock;
        }

    }
}
