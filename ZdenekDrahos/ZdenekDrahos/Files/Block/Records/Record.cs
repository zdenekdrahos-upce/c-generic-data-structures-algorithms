﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Files.Block.Records
{
    [Serializable]
    public class Record<TKey, TValue>
    {

        public TKey Key;
        public TValue Value;

        public Record() 
        {
            Key = default(TKey);
            Value = default(TValue);
        }

        public Record(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

    }
}
