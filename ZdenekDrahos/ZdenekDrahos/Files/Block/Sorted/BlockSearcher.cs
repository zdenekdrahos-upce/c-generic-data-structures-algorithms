﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Files.Block.Records;
using ZdenekDrahos.Search;
using ZdenekDrahos.Search.Array;
using ZdenekDrahos.Search.SearchAlgorithms;

namespace ZdenekDrahos.Files.Block.Sorted
{
    class BlockSearcher<TKey, TValue> where TKey : IComparable<TKey>
    {

        private int indexRecord;
        private Searcher<TKey> blockSearcher;
        private SortedBlockFile<TKey, TValue> searchedFile;
        private ArraySearcher<Record<TKey, TValue>> recordSearcher;

        public BlockSearcher()
        {
            InitBlockSearcher();
            InitRecordSearcher();
        }

        private void InitBlockSearcher()
        {
            blockSearcher = new Searcher<TKey>();
            blockSearcher.IsIndexEmpty = this.IsIndexEmpty;
            blockSearcher.IsItemFound = this.IsItemFound;
            blockSearcher.ExistsSmallerKey = this.ExistsSmallerKey;
            blockSearcher.ExistsLargerKey = this.ExistsLargerKey;
        }

        private void InitRecordSearcher()
        {
            Comparison<Record<TKey, TValue>> comp = (x, y) => x.Key.CompareTo(y.Key);
            recordSearcher = ArraySearcherFactory<Record<TKey, TValue>>.BinarySearch(comp);
        }

        public void SetSearchedFile(SortedBlockFile<TKey, TValue> file)
        {
            searchedFile = file;
            blockSearcher.MinIndex = 0;
            UpdateMaxIndex();
        }

        public void SetBinarySearch()
        {
            ISearchAlgorithm<TKey> binarySearch = new BinarySearch<TKey>();
            SetSearchAlgorithm(binarySearch);
        }

        public void SetInterpolationSearch(Func<TKey, double> keyNumberConverter)
        {
            InterpolationSearch<TKey> interpolation = new InterpolationSearch<TKey>();
            interpolation.KeyNumberConverter = keyNumberConverter;
            interpolation.GetMinKey = this.FindMinKey;
            interpolation.GetMaxKey = this.FindMaxKey;
            SetSearchAlgorithm(interpolation);
        }

        public void SetSearchAlgorithm(ISearchAlgorithm<TKey> algorithm)
        {
            blockSearcher.GetIndex = algorithm.GetIndex;
        }

        public Record<TKey, TValue> Search(TKey key)
        {
            UpdateMaxIndex();
            if (!FindIndexOfRecordInBuffer(key))
                blockSearcher.Search(key); // returns id block, but the block is already in buffer (read in IsItemFound)
            return searchedFile.BlockFile.Buffer[indexRecord];
        }

        private void UpdateMaxIndex()
        {
            blockSearcher.MaxIndex = searchedFile.BlockFile.ControlBlock.AllocatedBlocksCount - 1;
        }

        private bool IsIndexEmpty(int index)
        {
            return false;// block always exists
        }

        private bool IsItemFound(TKey key, int index)
        {
            searchedFile.BlockFile.TryRead(index);
            return FindIndexOfRecordInBuffer(key);
        }

        private bool FindIndexOfRecordInBuffer(TKey key)
        {
            indexRecord = -1;
            if (!searchedFile.BlockFile.Buffer.IsEmpty())
            {
                try
                {
                    recordSearcher.SetSearchedArray(searchedFile.BlockFile.Buffer.Block.Records);
                    indexRecord = recordSearcher.FindIndex(new Record<TKey, TValue>(key, default(TValue)));
                }
                catch (KeyNotFoundException) { }
            }
            return IsRecordFound();
        }

        private bool IsRecordFound()
        {
            return indexRecord != -1;
        }

        private bool ExistsSmallerKey(TKey key, int index)
        {
            TKey min = searchedFile.BlockFile.Buffer.FindFirstRecord().Key;
            return key.CompareTo(min) == -1;
        }

        private bool ExistsLargerKey(TKey key, int index)
        {
            TKey max = searchedFile.BlockFile.Buffer.FindLastRecord().Key;
            return key.CompareTo(max) == 1;
        }

        private TKey FindMinKey(int minIndex)
        {
            searchedFile.BlockFile.Read(minIndex);
            return searchedFile.BlockFile.Buffer.FindFirstRecord().Key;
        }

        private TKey FindMaxKey(int maxIndex)
        {
            searchedFile.BlockFile.Read(maxIndex);
            return searchedFile.BlockFile.Buffer.FindLastRecord().Key;
        }

    }
}
