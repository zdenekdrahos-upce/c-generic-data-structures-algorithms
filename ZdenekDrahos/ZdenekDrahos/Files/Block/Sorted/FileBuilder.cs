﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.IO;

namespace ZdenekDrahos.Files.Block.Sorted
{
    class FileBuilder<TKey, TValue> where TKey : IComparable<TKey>
    {
        private SortedList<TKey, TValue> sortedList;
        private BlockFile<TKey, TValue> file;

        public BlockFile<TKey, TValue> Build(IEnumerable<KeyValuePair<TKey, TValue>> values, BlockFile<TKey, TValue> file)
        {
            this.file = file;
            LoadSortedList(values);
            file.RemoveAllBlocks();
            WriteBlocks();
            file.WriteControlBlock();
            return file;
        }

        public int GetNumberOfImportedElements()
        {
            return sortedList.Count;
        }

        private void LoadSortedList(IEnumerable<KeyValuePair<TKey, TValue>> values)
        {
            sortedList = new SortedList<TKey, TValue>();
            foreach (KeyValuePair<TKey, TValue> item in values)
                sortedList.Add(item.Key, item.Value);
        }

        private void WriteBlocks()
        {
            using (FileStream fileStream = file.BinaryFile.GetWriteStream())
            {
                using (BinaryWriter binaryWriter = file.BinaryFile.GetBinaryWriter(fileStream))
                {
                    int index = 0;
                    SeekToFirstBlock(binaryWriter);
                    while (index < sortedList.Count)
                    {
                        file.AppendEmptyBlock();
                        for (int i = 0; i < file.ControlBlock.RecordsInBlockCount; i++)
                        {
                            AddRecordToBuffer(index);
                            if (++index >= sortedList.Count)
                                break;
                        }
                        WriteBlock(binaryWriter);
                    }
                }
            }
        }

        private void SeekToFirstBlock(BinaryWriter binaryWriter)
        {
            binaryWriter.Seek(file.ControlBlock.ControlBlockSize, SeekOrigin.Begin);
        }

        private void WriteBlock(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(file.SerializeBuffer());
            file.ControlBlock.AllocatedBlocksCount++;
        }

        private void AddRecordToBuffer(int index)
        {
            file.Buffer.AddRecord(sortedList.Keys[index], sortedList.Values[index]);
        }

    }
}
