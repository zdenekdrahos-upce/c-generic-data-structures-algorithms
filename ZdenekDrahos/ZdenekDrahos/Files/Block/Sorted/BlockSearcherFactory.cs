﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Files.Block.Sorted
{
    class BlockSearcherFactory<TKey, TValue> where TKey : IComparable<TKey>
    {

        public BlockSearcher<TKey, TValue> Create(SortedBlockFile<TKey, TValue> file)
        {
            BlockSearcher<TKey, TValue> binarySearcher = new BlockSearcher<TKey, TValue>();
            binarySearcher.SetBinarySearch();
            binarySearcher.SetSearchedFile(file);
            return binarySearcher;
        }

    }
}
