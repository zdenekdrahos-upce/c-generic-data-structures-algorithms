﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.Files.Block.Sorted
{
    public class SortedBlockFile<TKey, TValue> where TKey : IComparable<TKey>
    {
        private FileBuilder<TKey, TValue> builder;
        private BlockSearcher<TKey, TValue> binarySearcher;
        private BlockSearcher<TKey, TValue> interpolationSearcher;

        public SortedBlockFile(string pathToBinaryFile, int recordsInBlock, int blockSize)
        {
            builder = new FileBuilder<TKey, TValue>();
            BlockFile = new BlockFile<TKey, TValue>(pathToBinaryFile, recordsInBlock, blockSize);
            BlockSearcherFactory<TKey, TValue> searcherFactory = new BlockSearcherFactory<TKey, TValue>();
            binarySearcher = searcherFactory.Create(this);
            interpolationSearcher = searcherFactory.Create(this);
        }

        public BlockFile<TKey, TValue> BlockFile { get; private set; }

        public Func<TKey, double> KeyNumberConverter
        {
            get { throw new NotSupportedException(); }
            set { interpolationSearcher.SetInterpolationSearch(value); }
        }

        public int Build(IEnumerable<KeyValuePair<TKey, TValue>> values)
        {
            BlockFile = builder.Build(values, BlockFile);
            return builder.GetNumberOfImportedElements();
        }

        public TValue BinarySearch(TKey key)
        {            
            return binarySearcher.Search(key).Value;
        }

        public TValue InterpolationSearch(TKey key)
        {
            return interpolationSearcher.Search(key).Value;
        }

        public int GetNumberOfRecords()
        {
            int count = 0;
            for (int i = 0; i < BlockFile.ControlBlock.AllocatedBlocksCount; i++)
            {
                BlockFile.Read(i);
                for (int j = 0; j < BlockFile.ControlBlock.RecordsInBlockCount; j++)
                    if (BlockFile.Buffer.Block.FreeBlocks[j] == false)
                        count++;
            }
            return count;
        }

        public TValue Remove(TKey key)
        {
            TValue removedRecord = InterpolationSearch(key); // block with key is now in buffer
            BlockFile.Buffer.RemoveRecord(key);
            BlockFile.Save();
            return removedRecord;
        }

    }
}
