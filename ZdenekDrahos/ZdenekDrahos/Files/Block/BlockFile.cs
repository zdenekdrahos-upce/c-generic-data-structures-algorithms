﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.IO;
using ZdenekDrahos.Files.Binary;
using ZdenekDrahos.Files.Block.Blocks;

namespace ZdenekDrahos.Files.Block
{
    public class BlockFile<TKey, TValue>
    {

        public BlockFile(string pathToBinaryFile, int recordsInBlock, int blockSize)
            : this(pathToBinaryFile, recordsInBlock, blockSize, new BlockFileSerialization<TKey, TValue>()) { }

        public BlockFile(string pathToBinaryFile, int recordsInBlock, int blockSize, IBlockByteConverter<TKey, TValue> byteConverter)
        {
            ByteConverter = byteConverter;
            CreateControlBlock(recordsInBlock, blockSize);
            InitFileStructures(pathToBinaryFile);
            if (BinaryFile.Exists())
                LoadControlBlock();
            else
                WriteControlBlock();
        }

        public ControlBlock ControlBlock { get; private set; }
        public Buffer<TKey, TValue> Buffer { get; private set; }
        public BinaryFile BinaryFile { get; private set; }
        public IBlockByteConverter<TKey, TValue> ByteConverter { get; set; }
        public event BlockReadEventHandler BlockRead;

        public void AppendEmptyBlock()
        {
            Buffer.BlockIndex = ControlBlock.AllocatedBlocksCount;
            Buffer.Block = new Block<TKey, TValue>(ControlBlock.RecordsInBlockCount);
        }

        public void TryRead(int blockIndex)
        {
            if (Buffer.BlockIndex != blockIndex)
                Read(blockIndex);
        }

        public void Read(int blockIndex)
        {
            byte[] bytes = BinaryFile.Read(GetBlockOffset(blockIndex), ControlBlock.BlockSize);
            Buffer.Block = ByteConverter.DeserializeBlock(bytes);
            Buffer.BlockIndex = blockIndex;
            OnBlockRead();
        }

        public List<Block<TKey, TValue>> ReadAll()
        {
            List<Block<TKey, TValue>> blocks = new List<Block<TKey, TValue>>();
            if (ControlBlock.AllocatedBlocksCount > 0)
            {
                using (FileStream fileStream = BinaryFile.GetReadStream())
                {
                    using (BinaryReader reader = BinaryFile.GetBinaryReader(fileStream))
                    {
                        reader.BaseStream.Position = GetBlockOffset(0);
                        for (int blockIndex = 0; blockIndex < ControlBlock.AllocatedBlocksCount; blockIndex++)
                        {
                            byte[] bytes = BinaryFile.Read(reader, ControlBlock.BlockSize);
                            Block<TKey, TValue> block = ByteConverter.DeserializeBlock(bytes);
                            blocks.Add(block);
                        }
                    }
                }
            }
            return blocks;
        }

        public void Save()
        {
            if (Buffer.IsEmpty())
                Remove();
            else
                Modify();
        }

        public void Write()
        {
            WriteBlock(ControlBlock.AllocatedBlocksCount);
            ControlBlock.AllocatedBlocksCount++;
            WriteControlBlock();
        }

        public void Remove()
        {
            if (CanBeBlockModified() && Buffer.IsEmpty())
            {

                byte[] nextBlocks = null;
                if ((Buffer.BlockIndex + 1) < ControlBlock.AllocatedBlocksCount)
                {
                    using (FileStream fileStream = BinaryFile.GetReadStream())
                    {
                        using (BinaryReader reader = BinaryFile.GetBinaryReader(fileStream))
                        {
                            reader.BaseStream.Position = GetBlockOffset(Buffer.BlockIndex + 1);
                            long length = reader.BaseStream.Length - reader.BaseStream.Position;
                            nextBlocks = BinaryFile.Read(reader, (int)length);
                        }
                    }
                }
                using (FileStream fileStream = BinaryFile.GetWriteStream())
                {
                    using (BinaryWriter binaryWriter = BinaryFile.GetBinaryWriter(fileStream))
                    {
                        binaryWriter.Seek(GetBlockOffset(Buffer.BlockIndex), SeekOrigin.Begin);
                        if (nextBlocks != null)
                            binaryWriter.Write(nextBlocks);
                        // resize file                        
                        long newLength = binaryWriter.BaseStream.Position;
                        binaryWriter.BaseStream.SetLength(newLength);
                    }
                }
                ControlBlock.AllocatedBlocksCount--;
                WriteControlBlock();
                Buffer.Clear();
            }
        }

        public void RemoveAllBlocks()
        {
            using (FileStream fileStream = BinaryFile.GetWriteStream())
            {
                using (BinaryWriter binaryWriter = BinaryFile.GetBinaryWriter(fileStream))
                {
                    ControlBlock.AllocatedBlocksCount = 0;
                    long newLength = ControlBlock.ControlBlockSize;
                    binaryWriter.BaseStream.SetLength(newLength);
                }
            }
            WriteControlBlock();
        }

        public void Modify()
        {
            if (CanBeBlockModified() && !Buffer.IsEmpty())
                WriteBlock(Buffer.BlockIndex);
            else
                throw new InvalidOperationException("Invalid index of current block in buffer");
        }

        private bool CanBeBlockModified()
        {
            return Buffer.BlockIndex >= 0 && Buffer.BlockIndex < ControlBlock.AllocatedBlocksCount;
        }

        private void WriteBlock(int blockIndex)
        {
            if (Buffer.IsEmpty())
                throw new ArgumentNullException("Buffer is empty");
            byte[] bytes = SerializeBuffer();
            BinaryFile.Write(GetBlockOffset(blockIndex), bytes);
        }

        public byte[] SerializeBuffer()
        {
            return ByteConverter.SerializeBlock(Buffer.Block, ControlBlock);
        }

        private int GetBlockOffset(int blockIndex)
        {
            return ControlBlock.ControlBlockSize + blockIndex * ControlBlock.BlockSize;
        }

        public void WriteControlBlock()
        {
            byte[] bytes = ByteConverter.SerializeControlBlock(ControlBlock);
            BinaryFile.Write(0, bytes);
        }

        private void InitFileStructures(string pathToBinaryFile)
        {
            Buffer = new Buffer<TKey, TValue>();
            BinaryFile = new BinaryFile(pathToBinaryFile);
        }

        private void CreateControlBlock(int recordsInBlock, int blockSize)
        {
            ControlBlock = ByteConverter.CreateControlBlock(recordsInBlock, blockSize);
        }

        private void LoadControlBlock()
        {
            byte[] bytes = BinaryFile.Read(0, ControlBlock.ControlBlockSize);
            ControlBlock = ByteConverter.DeserializeControlBlock(bytes);
        }

        protected virtual void OnBlockRead()
        {
            BlockReadEventHandler handler = this.BlockRead;
            if (handler != null)
                handler(this, new BlockReadEventArgs(Buffer.BlockIndex));
        }

    }
}
