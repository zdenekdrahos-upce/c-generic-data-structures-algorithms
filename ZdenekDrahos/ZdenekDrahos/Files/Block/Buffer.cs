﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Files.Block.Blocks;
using ZdenekDrahos.Files.Block.Records;

namespace ZdenekDrahos.Files.Block
{
    public class Buffer<TKey, TValue>
    {

        public Block<TKey, TValue> Block { get; set; }
        public int BlockIndex { get; set; }

        public bool IsEmpty()
        {
            if (Block != null)
            {
                for (int i = 0; i < Block.FreeBlocks.Length; i++)
                    if (Block.FreeBlocks[i] == false)
                        return false;
            }
            return true;
        }

        public Record<TKey, TValue> this[TKey key]
        {
            get { return this[FindIndexOfRecord(key)]; }
            private set { }
        }

        public void AddRecord(TKey key, TValue value)
        {
            Record<TKey, TValue> record = new Record<TKey, TValue>(key, value);
            AddRecord(record);
        }

        public void AddRecord(Record<TKey, TValue> record)
        {
            int index = GetIndexOfFirstFreeBlock();
            this[index] = record;
        }

        public Record<TKey, TValue> this[int i]
        {
            get
            {
                return Block.Records[i];
            }
            set
            {
                Block.Records[i] = value;
                Block.FreeBlocks[i] = value == null;
            }
        }

        public void RemoveRecord(TKey key)
        {
            int index = FindIndexOfRecord(key);
            this[index] = null;
        }

        public void Clear()
        {
            Block = null;
            BlockIndex = -1;
        }

        public Record<TKey, TValue> FindFirstRecord()
        {
            if (Block.FreeBlocks[0] == false)
                return this[0];
            for (int i = 1; i < Block.FreeBlocks.Length; i++)
                if (Block.FreeBlocks[i] == false)
                    return this[i];
            throw new InvalidOperationException("Block is empty");
        }

        public Record<TKey, TValue> FindLastRecord()
        {
            for (int i = Block.FreeBlocks.Length - 1; i >= 0; i--)
                if (Block.FreeBlocks[i] == false)
                    return this[i];
            throw new InvalidOperationException("Block is empty");
        }

        public int GetIndexOfKey(TKey key)
        {
            if (Block != null)
                for (int i = 0; i < Block.FreeBlocks.Length; i++)
                    if (Block.FreeBlocks[i] == false && Block.Records[i].Key.Equals(key))
                        return i;
            return -1;
        }

        private int FindIndexOfRecord(TKey key)
        {
            int index = GetIndexOfKey(key);
            if (index != -1)
                return index;
            else
                throw new KeyNotFoundException();
        }

        public int GetIndexOfFirstFreeBlock()
        {
            if (Block != null)
                for (int i = 0; i < Block.FreeBlocks.Length; i++)
                    if (Block.FreeBlocks[i])
                        return i;
            throw new InvalidOperationException("Block is full");
        }

    }
}
