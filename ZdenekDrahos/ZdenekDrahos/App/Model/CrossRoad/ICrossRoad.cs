﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;

namespace ZdenekDrahos.App.Model
{

    public interface ICrossRoad : IPoint
    {
        string ID { get; }
        Point Point { get; }
        bool IsBusStop { get; }
        bool IsRestArea { get; }

        void SetCrossRoad();

        void SetBusStop();

        void SetRestArea();

        bool Equals(object obj);

        int GetHashCode();
    }
}
