﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Drawing;

namespace ZdenekDrahos.App.Model
{
    [Serializable]
    public class CrossRoad : ICrossRoad
    {
        private string id;
        private int x, y;
        private bool isBusStop, isRestArea;

        public CrossRoad() {}

        public CrossRoad(string id, Point point, bool isBusStop, bool isRestArea)
        {
            ID = id;
            x = point.X;
            y = point.Y;
            Point = point;
            IsBusStop = isBusStop;
            IsRestArea = isRestArea;
        }

        #region ICrossRoad Members

        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        public Point Point { get; private set; }

        public bool IsBusStop
        {
            get { return isBusStop; }
            set { isBusStop = value; }
        }

        public bool IsRestArea
        {
            get { return isRestArea; }
            set { isRestArea = value; }
        }

        public void SetCrossRoad()
        {
            IsBusStop = false;
            IsRestArea = false;
        }

        public void SetBusStop()
        {
            IsBusStop = true;
            IsRestArea = false;
        }

        public void SetRestArea()
        {
            IsBusStop = false;
            IsRestArea = true;
        }

        public override bool Equals(object obj)
        {
            CrossRoad crossRoad = obj as CrossRoad;
            if (crossRoad != null)
                return ID.Equals(crossRoad.ID);
            return false;
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        #endregion

        #region IPoint Members

        public int X
        {
            get { return Point.X; }
            private set { }
        }

        public int Y
        {
            get { return Point.Y; }
            private set { }
        }

        #endregion

        public override string ToString()
        {
            if (IsBusStop)
                return string.Format("Z ({0})", ID);
            if (IsRestArea)
                return string.Format("O ({0})", ID);
            else
                return ID;
        }
    }
}
