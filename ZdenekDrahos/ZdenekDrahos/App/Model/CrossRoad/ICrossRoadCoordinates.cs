﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.App.Model
{
    static class ICrossRoadCoordinates
    {

        public static int GetCoordinateX(ICrossRoad crossRoad)
        {
            return crossRoad.Point.X;
        }

        public static int GetCoordinateY(ICrossRoad crossRoad)
        {
            return crossRoad.Point.Y;
        }

    }
}
