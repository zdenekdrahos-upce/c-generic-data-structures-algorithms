﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.App.Model
{
    public interface IPoint
    {

        int X { get; }
        int Y { get; }

    }
}
