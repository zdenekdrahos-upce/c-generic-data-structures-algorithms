﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;

namespace ZdenekDrahos.App.Model
{
    public class CrossRoadFactory
    {

        public ICrossRoad CreateBustStop(string id, Point point)
        {
            return Create(id, point, true, false);
        }

        public ICrossRoad CreateRestArea(string id, Point point)
        {
            return Create(id, point, false, true);
        }

        public ICrossRoad CreateCrossRoad(string id, Point point)
        {
            return Create(id, point, false, false);
        }

        public ICrossRoad Create(string id, Point point, bool isBusStop, bool isRestArea)
        {
            return new CrossRoad(id, point, isBusStop, isRestArea);
        }

    }
}
