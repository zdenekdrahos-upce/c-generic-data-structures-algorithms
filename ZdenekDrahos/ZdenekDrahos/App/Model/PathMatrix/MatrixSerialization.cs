﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ZdenekDrahos.Graph.ShortestPath.Floyd;

namespace ZdenekDrahos.App.Model.PathMatrix
{
    [Serializable]
    class MatrixSerialization
    {

        public double[,] DistanceMatrix;
        public double[,] PredecessorMatrix;
        public int[] Indexes;
        public string[] Ids;

        public MatrixSerialization(PathMatrix<string, ICrossRoad, double> matrix)
        {
            DistanceMatrix = matrix.DistanceMatrix;
            PredecessorMatrix = matrix.PredecessorMatrix;
            Indexes = matrix.IndexKeyMap.Keys.ToArray();
            Ids = matrix.IndexKeyMap.Values.ToArray();
        }

        public PathMatrix<string, ICrossRoad, double> TransformToPathMatrix()
        {
            IDictionary<int, string> indexKeyMap = GetIndexKeyMap();
            return new PathMatrix<string, ICrossRoad, double>(DistanceMatrix, PredecessorMatrix, indexKeyMap);
        }

        private IDictionary<int, string> GetIndexKeyMap()
        {
            IDictionary<int, string> dict = new Dictionary<int, string>();
            for (int i = 0; i < Indexes.Length; i++)
                dict.Add(Indexes[i], Ids[i]);
            return dict;
        }

    }
}
