﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ZdenekDrahos.Graph.ShortestPath.Floyd;

namespace ZdenekDrahos.App.Model.PathMatrix
{
    public class FloydDataMapper : IFloydDataMapper
    {

        private MatrixSerialization serialization;

        #region IFloydDataMapper Members

        public void SavePathMatrix(PathMatrix<string, ICrossRoad, double> matrix, string pathToFile)
        {
            serialization = new MatrixSerialization(matrix);
            Serialize(pathToFile);
        }

        public PathMatrix<string, ICrossRoad, double> LoadPathMatrix(string pathToFile)
        {
            Deserialize(pathToFile);
            return serialization.TransformToPathMatrix();
        }

        #endregion

        private void Serialize(string pathToFile)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream writer = new FileStream(pathToFile, FileMode.Create))
            {
                bf.Serialize(writer, serialization);
            }
        }

        private void Deserialize(string pathToFile)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream reader = new FileStream(pathToFile, FileMode.Open))
            {
                serialization = (MatrixSerialization)bf.Deserialize(reader);                
            }
        }
    }
}
