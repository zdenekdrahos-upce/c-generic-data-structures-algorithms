﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using ZdenekDrahos.Graph.ShortestPath.Floyd;

namespace ZdenekDrahos.App.Model.PathMatrix
{
    public interface IFloydDataMapper
    {

        void SavePathMatrix(PathMatrix<string, ICrossRoad, double> matrix, string pathToFile);

        PathMatrix<string, ICrossRoad, double> LoadPathMatrix(string pathToFile);

    }
}
