﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.App.Model.Files
{
    [Serializable]
    public class CrossRoadKey : IComparable<CrossRoadKey>
    {

        private int x;
        private int y;

        public CrossRoadKey(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        #region IComparable<CrossRoadKey> Members

        public int CompareTo(CrossRoadKey other)
        {
            int compareX = x.CompareTo(other.x);
            if (compareX != 0)
                return compareX;
            else 
                return y.CompareTo(other.y);
        }

        #endregion

        public override bool Equals(object obj)
        {
            CrossRoadKey other = obj as CrossRoadKey;
            if (other == null)
                return false;
            else
                return CompareTo(other) == 0;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
