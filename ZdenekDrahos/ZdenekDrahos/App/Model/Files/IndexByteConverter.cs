﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Files.Block.Blocks;
using ZdenekDrahos.Files.Block.Records;
using ZdenekDrahos.Files.Index;
using ZdenekDrahos.Tree.Tree23.RedBlack;

namespace ZdenekDrahos.App.Model.Files
{
    public class IndexByteConverter : IBlockByteConverter<CrossRoadKey, RedBlackTreeValue<KeyIndex>>
    {

        private const int controlBlockSize = 16;
        private const int recordSize = 18;

        #region IBlockByteConverter<CrossRoadKey,RedBlackTreeValue<KeyIndex>> Members

        public ControlBlock CreateControlBlock(int recordsInBlock, int blockSize)
        {
            ControlBlock controlBlock = new ControlBlock(recordsInBlock, recordsInBlock * recordSize);
            controlBlock.ControlBlockSize = controlBlockSize;
            return controlBlock;
        }

        public byte[] SerializeControlBlock(ControlBlock controlBlock)
        {
            List<byte> bytes = new List<byte>();
            bytes.AddRange(ConvertIntToByte(controlBlock.RecordsInBlockCount));
            bytes.AddRange(ConvertIntToByte(controlBlock.BlockSize));
            bytes.AddRange(ConvertIntToByte(controlBlock.AllocatedBlocksCount));
            bytes.AddRange(ConvertIntToByte(controlBlock.ControlBlockSize));
            return bytes.ToArray();
        }

        public ControlBlock DeserializeControlBlock(byte[] bytes)
        {
            int[] intValues = ConvertByteArray(bytes, 0, 4);
            ControlBlock block = new ControlBlock(intValues[0], intValues[1]);
            block.AllocatedBlocksCount = intValues[2];
            block.ControlBlockSize = intValues[3];
            return block;
        }

        public byte[] SerializeBlock(Block<CrossRoadKey, RedBlackTreeValue<KeyIndex>> block, ControlBlock controlBlock)
        {
            List<byte> bytes = new List<byte>();
            for (int i = 0; i < controlBlock.RecordsInBlockCount; i++)
            {
                Record<CrossRoadKey, RedBlackTreeValue<KeyIndex>> rec = block.Records[i];
                bytes.Add(ConvertBoolToByte(block.FreeBlocks[i]));
                if (block.FreeBlocks[i])
                    bytes.AddRange(new byte[17]);
                else
                {
                    bytes.AddRange(ConvertIntToByte(rec.Key.X));
                    bytes.AddRange(ConvertIntToByte(rec.Key.Y));
                    bytes.AddRange(ConvertIntToByte(rec.Value.Value.Block));
                    bytes.AddRange(ConvertIntToByte(rec.Value.Value.Record));
                    bytes.Add(ConvertBoolToByte(rec.Value.IsRed));
                }
            }
            return bytes.ToArray();
        }

        public Block<CrossRoadKey, RedBlackTreeValue<KeyIndex>> DeserializeBlock(byte[] bytes)
        {
            int recordsInBlockCount = bytes.Length / 18;
            Block<CrossRoadKey, RedBlackTreeValue<KeyIndex>> block = new Block<CrossRoadKey, RedBlackTreeValue<KeyIndex>>(recordsInBlockCount);

            int start = 0, recordIndex = 0;

            while (start < bytes.Length)
            {
                block.FreeBlocks[recordIndex] = ConvertByteToBool(bytes[start]);
                if (block.FreeBlocks[recordIndex])
                    block.Records[recordIndex] = null;
                else
                {
                    int[] intValues = ConvertByteArray(bytes, start + 1, 4);
                    bool isRed = ConvertByteToBool(bytes[start + 17]);

                    Record<CrossRoadKey, RedBlackTreeValue<KeyIndex>> record = new Record<CrossRoadKey, RedBlackTreeValue<KeyIndex>>();
                    record.Key = new CrossRoadKey(intValues[0], intValues[1]);
                    KeyIndex keyIndex = new KeyIndex(intValues[2], intValues[3]);
                    record.Value = new RedBlackTreeValue<KeyIndex>(keyIndex, isRed);
                    block.Records[recordIndex] = record;
                }
                start += recordSize;
                recordIndex++;
            }
            return block;
        }

        #endregion

        private int[] ConvertByteArray(byte[] bytes, int startIndex, int length)
        {
            byte[] intArray = new byte[4];
            int[] intValues = new int[length];
            for (int i = 0; i < length; i++)
            {
                Array.Copy(bytes, startIndex + i * 4, intArray, 0, 4);
                intValues[i] = ConvertByteToInt(intArray);
            }
            return intValues;
        }

        private byte[] ConvertIntToByte(int number)
        {
            return BitConverter.GetBytes(number);
        }

        private int ConvertByteToInt(byte[] bytes)
        {
            return BitConverter.ToInt32(bytes, 0);
        }

        private byte ConvertBoolToByte(bool boolean)
        {
            return boolean ? (byte)1 : (byte)0;
        }

        private bool ConvertByteToBool(byte boolean)
        {
            return boolean != 0;
        }
    }
}
