﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;

namespace ZdenekDrahos.App.Model.Files
{
    class CrossRoadsGenerator
    {

        private int actualValue;
        private Random generator = new Random();

        public CrossRoadsGenerator()
        {
            actualValue = 1;
        }

        public List<KeyValuePair<CrossRoadKey, CrossRoad>> Generate(int recordsCount)
        {
            List<KeyValuePair<CrossRoadKey, CrossRoad>> input = new List<KeyValuePair<CrossRoadKey, CrossRoad>>(recordsCount);
            int maxRecords = actualValue + recordsCount;
            for (; actualValue < maxRecords; actualValue++)
                input.Add(Generate());
            return input;
        }

        public CrossRoad GenerateCrossRoad()
        {
            actualValue++;
            return GetCrossRoad();
        }

        private KeyValuePair<CrossRoadKey, CrossRoad> Generate()
        {
            CrossRoadKey key = new CrossRoadKey(actualValue, actualValue);
            CrossRoad value = GetCrossRoad();
            return new KeyValuePair<CrossRoadKey, CrossRoad>(key, value);
        }

        private CrossRoad GetCrossRoad()
        {
            return new CrossRoad(actualValue.ToString(), new Point(actualValue, actualValue), IsSomething(), IsSomething());
        }

        private bool IsSomething()
        {
            return generator.Next(100) % 2 == 0;
        }

    }
}
