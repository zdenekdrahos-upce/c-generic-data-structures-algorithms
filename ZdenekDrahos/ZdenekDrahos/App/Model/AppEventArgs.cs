﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.App.Model
{
    public class AppEventArgs<T> : EventArgs
    {
        public T Data { get; private set; }

        public AppEventArgs(T data)
        {
            Data = data;
        }

    }
}
