﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Drawing;
using System.IO;
using ZdenekDrahos.Graph;

namespace ZdenekDrahos.App.Model
{
    public class ForestDataMapper : IForestDataMapper
    {

        public ForestDataMapper(CrossRoadFactory factory)
        {
            CrossRoadFactory = factory;
        }

        #region IForestDataMapper Members

        public IForestArea Forest { get; private set; }
        public CrossRoadFactory CrossRoadFactory { get; private set;  }

        public void SaveForest(IForestArea forest, string pathToFile)
        {
            AssignForest(forest);            
            using (StreamWriter writer = new StreamWriter(File.Open(pathToFile, FileMode.Create, FileAccess.Write)))
            {
                WriteNodes(writer);
                WriteEdges(writer);
            }
        }

        public IForestArea LoadForest(string pathToFile)
        {
            Forest = new ForestArea();
            using (StreamReader reader = new StreamReader(File.OpenRead(pathToFile)))
            {
                while (!reader.EndOfStream)
                    ParseLine(reader.ReadLine());                
            }
            Forest.SetCrossRoadFactory(CrossRoadFactory);
            Forest.UpdateNextID();
            return Forest;
        }

        #endregion

        private void AssignForest(IForestArea forest)
        {
            if (forest == null)
                throw new ArgumentNullException();
            Forest = forest;
        }

        private void WriteNodes(StreamWriter writer)
        {
            foreach (ICrossRoad node in Forest.CrossRoadIterator())            
                writer.WriteLine("{0};{1};{2};{3};{4}", node.ID, node.Point.X, node.Point.Y, node.IsBusStop, node.IsRestArea);
        }

        private void WriteEdges(StreamWriter writer)
        {
            foreach (Edge<string, ICrossRoad, double> edge in Forest.EdgesIterator())
                writer.WriteLine("{0};{1};{2}", edge.StartNode.ID, edge.EndNode.ID, edge.EdgeCost);
        }

        private void ParseLine(string line)
        {
            string[] lineItems = line.Split(';');
            if (lineItems.Length == 5)
                LoadNode(lineItems);
            else if (lineItems.Length == 3)
                LoadEdge(lineItems);
            else
                throw new InvalidDataException();
        }

        private void LoadNode(string[] lineItems)
        {
            string id = lineItems[0];
            Point point = new Point(int.Parse(lineItems[1]), int.Parse(lineItems[2]));
            bool isBusStop = lineItems[3].Equals("True");
            bool isRestArea = lineItems[4].Equals("True");
            Forest.AddNewCrossRoad(CrossRoadFactory.Create(id, point, isBusStop, isRestArea));
        }

        private void LoadEdge(string[] lineItems)
        {
            string start = lineItems[0];
            string end = lineItems[1];
            double cost = double.Parse(lineItems[2]);
            Forest.AddEdge(start, end, cost);
        }

    }
}
