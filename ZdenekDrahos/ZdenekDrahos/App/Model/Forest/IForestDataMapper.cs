﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.App.Model
{
    public interface IForestDataMapper
    {
        IForestArea Forest { get; }
        CrossRoadFactory CrossRoadFactory { get; }

        void SaveForest(IForestArea forest, string pathToFile);

        IForestArea LoadForest(string pathToFile);

    }
}
