﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using System.Drawing;
using ZdenekDrahos.Graph;
using ZdenekDrahos.Graph.ShortestPath;
using ZdenekDrahos.Graph.ShortestPath.Floyd;

namespace ZdenekDrahos.App.Model
{
    public interface IForestArea
    {

        void SetCrossRoadFactory(CrossRoadFactory factory);

        void AddCrossRoad(Point point);

        void AddBusStop(Point point);

        void AddRestArea(Point point);

        void AddNewCrossRoad(ICrossRoad newCrossRoad);

        void RemoveCrossRoad(string idCrossroad);

        void RemoveEdge(string start, string end);

        void AddNeighboursToLastCrossRoad(IList<KeyValuePair<string, double>> neighbours);

        void AddCrossRoadNeighbours(string idCrossRoad, IList<KeyValuePair<string, double>> neighbours);

        void AddEdge(string start, string end, double cost);

        ICrossRoad FindCrossRoad(string id);

        Edge<string, ICrossRoad, double> FindEdge(string start, string end);

        void ModifyCrossRoad(string id, Point newPosition);

        void ModifyEdge(string start, string end, double newCost);

        void MoveEdge(string start, string end, KeyValuePair<Point, Point> newLinePoints);

        void MarkCrossRoadAsBusStop(string idCrossRoad);

        void MarkCrossRoadAsRestArea(string idCrossRoad);

        void MarkCrossRoadAsBasic(string idCrossRoad);

        void SetPathRestriction(IsEdgeAccessibleCallback<string, ICrossRoad, double> edgeAccessibility);

        Path<ICrossRoad> FindDijkstraShortestPaths(string startNode, string endNode);

        Path<ICrossRoad> FindFloydShortestPaths(string startNode, string endNode);

        PathMatrix<string, ICrossRoad, double> FloydMatrix { get; set; }

        void ReloadFloydMatrix();

        void BuildRangeTree();

        ICrossRoad BasicSearchInRangeTree(Point point);

        IList<ICrossRoad> IntervalSearchInRangeTree(Rectangle rect);

        IEnumerable<ICrossRoad> CrossRoadIterator();

        IEnumerable<ICrossRoad> BusStopIterator();

        IEnumerable<ICrossRoad> RestAreaIterator();

        IEnumerable<ICrossRoad> IncidenceNodesIterator(string id);

        IEnumerable<Edge<string, ICrossRoad, double>> EdgesIterator();

        IEnumerable<string> CrossRoadKeysIterator();

        void UpdateNextID();
    }
}
