﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using System.Drawing;
using ZdenekDrahos.Graph;
using ZdenekDrahos.Graph.ShortestPath;
using ZdenekDrahos.Graph.ShortestPath.Dijkstra;
using ZdenekDrahos.Graph.ShortestPath.Floyd;
using ZdenekDrahos.Tree.RangeTree._2D;

namespace ZdenekDrahos.App.Model
{
    public class ForestArea : IForestArea
    {
        private int idOfNextNode;
        private CrossRoadFactory crossRoadFactory;
        private IGraph<string, ICrossRoad, double> graph;
        private IShortestPath<string, ICrossRoad, double> dijkstra;
        private IShortestPathsMatrix<string, ICrossRoad, double> floyd;
        private IRangeTree2D<int, ICrossRoad> rangeTree;

        public ForestArea() : this(new Graph<string, ICrossRoad, double>()) {}

        public ForestArea(IGraph<string, ICrossRoad, double> graph)
        {
            this.graph = graph;
            idOfNextNode = graph.Count + 1;
            dijkstra = new DijkstraAlgorithm<string, ICrossRoad, double>(graph);
            floyd = new FloydAlgorithm<string, ICrossRoad, double>();
            graph.GraphChanged += dijkstra.OnGraphChanged;
            // floyd is not informed, because matrix calculation has O(n^3)
            rangeTree = new RangeTree2D<int, ICrossRoad>(ICrossRoadCoordinates.GetCoordinateX, ICrossRoadCoordinates.GetCoordinateY);
        }

        public void SetCrossRoadFactory(CrossRoadFactory factory)
        {
            crossRoadFactory = factory;
        }

        public void AddCrossRoad(Point point)
        {
            ICrossRoad newCrossRoad = crossRoadFactory.CreateCrossRoad(GetIDOfNextCrossRoad(), point);
            AddNewCrossRoad(newCrossRoad);
        }

        public void AddBusStop(Point point)
        {
            ICrossRoad newCrossRoad = crossRoadFactory.CreateBustStop(GetIDOfNextCrossRoad(), point);
            AddNewCrossRoad(newCrossRoad);
        }

        public void AddRestArea(Point point)
        {
            ICrossRoad newCrossRoad = crossRoadFactory.CreateRestArea(GetIDOfNextCrossRoad(), point);
            AddNewCrossRoad(newCrossRoad);
        }

        private string GetIDOfNextCrossRoad()
        {
            return string.Format("K{0}", idOfNextNode++);
        }

        public void AddNewCrossRoad(ICrossRoad newCrossRoad)
        {
            graph.AddNode(newCrossRoad.ID, newCrossRoad);
        }

        public void RemoveCrossRoad(string idCrossroad)
        {
            graph.RemoveNode(idCrossroad);
        }

        public void RemoveEdge(string start, string end)
        {
            graph.RemoveEdge(start, end);
        }

        public void AddNeighboursToLastCrossRoad(IList<KeyValuePair<string, double>> neighbours)
        {
            string idCrossRoad = string.Format("K{0}", idOfNextNode - 1);
            AddCrossRoadNeighbours(idCrossRoad, neighbours);
        }

        public void AddCrossRoadNeighbours(string idCrossRoad, IList<KeyValuePair<string, double>> neighbours)
        {
            if (neighbours != null)
                foreach (KeyValuePair<string, double> neighbour in neighbours)
                    AddEdge(idCrossRoad, neighbour.Key, neighbour.Value);
        }

        public void AddEdge(string start, string end, double cost)
        {
            graph.AddEdge(start, end, cost);
        }

        public ICrossRoad FindCrossRoad(string id)
        {
            return graph.FindNode(id);
        }

        public Edge<string, ICrossRoad, double> FindEdge(string start, string end)
        {
            return graph.FindEdge(start, end);
        }

        public void ModifyCrossRoad(string id, Point newPosition)
        {
            ICrossRoad current = graph.FindNode(id);
            graph.ModifyNode(id, crossRoadFactory.Create(id, newPosition, current.IsBusStop, current.IsRestArea));
        }

        public void ModifyEdge(string start, string end, double newCost)
        {
            graph.ModifyEdge(start, end, newCost);
        }

        public void MoveEdge(string start, string end, KeyValuePair<Point, Point> newLinePoints)
        {
            ModifyCrossRoad(start, newLinePoints.Key);
            ModifyCrossRoad(end, newLinePoints.Value);
        }

        public void MarkCrossRoadAsBusStop(string idCrossRoad)
        {
            graph.FindNode(idCrossRoad).SetBusStop();
        }

        public void MarkCrossRoadAsRestArea(string idCrossRoad)
        {
            graph.FindNode(idCrossRoad).SetRestArea();
        }

        public void MarkCrossRoadAsBasic(string idCrossRoad)
        {
            graph.FindNode(idCrossRoad).SetCrossRoad();
        }

        public void SetPathRestriction(IsEdgeAccessibleCallback<string, ICrossRoad, double> edgeAccessibility)
        {
            dijkstra.IsEdgeAccessible = edgeAccessibility;
        }

        public Path<ICrossRoad> FindDijkstraShortestPaths(string startNode, string endNode)
        {
            return dijkstra.GetShortestPath(startNode, endNode);
        }

        public Path<ICrossRoad> FindFloydShortestPaths(string startNode, string endNode)
        {
            return floyd.GetShortestPath(startNode, endNode);
        }

        public PathMatrix<string, ICrossRoad, double> FloydMatrix {
            get { 
                return floyd.PathMatrix; 
            }
            set {
                floyd.Graph = graph;
                floyd.PathMatrix = value;  
            } 
        }

        public void ReloadFloydMatrix()
        {
            floyd.CreateShortestPathsMatrix(graph);
        }

        public void BuildRangeTree()
        {
            if (graph.Count > 0)
                rangeTree.Build(CrossRoadIterator());
        }

        public ICrossRoad BasicSearchInRangeTree(Point point)
        {
            IList<ICrossRoad> list = rangeTree.Find(point.X, point.Y);
            return list[0];
        }

        public IList<ICrossRoad> IntervalSearchInRangeTree(Rectangle rect)
        {
            return rangeTree.IntervalSearch(rect.X, rect.Y, rect.X + rect.Width, rect.Y + rect.Height);
        }

        public IEnumerable<ICrossRoad> CrossRoadIterator()
        {
            return graph;
        }

        public IEnumerable<ICrossRoad> BusStopIterator()
        {
            foreach (ICrossRoad crossRoad in graph)
                if (crossRoad.IsBusStop)
                    yield return crossRoad;
        }

        public IEnumerable<ICrossRoad> RestAreaIterator()
        {
            foreach (ICrossRoad crossRoad in graph)
                if (crossRoad.IsRestArea)
                    yield return crossRoad;
        }

        public IEnumerable<ICrossRoad> IncidenceNodesIterator(string id)
        {
            return graph.IncidenceNodes(id);
        }

        public IEnumerable<Edge<string, ICrossRoad, double>> EdgesIterator()
        {
            return graph.EdgesIterator();
        }

        public IEnumerable<string> CrossRoadKeysIterator()
        {
            return graph.NodeKeysIterator();
        }

        public void UpdateNextID()
        {
            idOfNextNode = graph.Count + 1;
        }
    }
}
