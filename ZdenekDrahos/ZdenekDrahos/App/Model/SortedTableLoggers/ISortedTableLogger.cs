﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.App.Model.Files;
using ZdenekDrahos.Files.Block.Blocks;

namespace ZdenekDrahos.App.Model.SortedTableLoggers
{
    public interface ISortedTableLogger
    {
        string Text { get; }

        event EventHandler LogAction;

        void LogBuild(int tableCount, ControlBlock controlBlock);

        void LogInvalidBuild(int recommendedBlockSize);

        void LogSearch(string type, CrossRoadKey key, CrossRoad crossRoad, int blockIndex);

        void LogRemoveRecord(CrossRoadKey key, CrossRoad crossRoad, int blockIndex);

        void LogAddRecord(CrossRoadKey key, int blockIndex, int recordIndex);

        void LogReloadIndex();

        void LogBlockRead(int blockIndex);

        void LogObsoleteIndex();

        void LogClearBuffer(int blockIndex);
    }
}
