﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.App.Model.Files;
using ZdenekDrahos.Files.Block.Blocks;

namespace ZdenekDrahos.App.Model.SortedTableLoggers
{
    class NullSortedTableLogger : ISortedTableLogger
    {

        public static ISortedTableLogger Instance { get; private set; }

        static NullSortedTableLogger()
        {
            Instance = new NullSortedTableLogger();
        }

        private NullSortedTableLogger() { }

        #region ISortedTableLogger Members

        public string Text
        {
            get { return ""; }
        }

        public event EventHandler LogAction;

        public void LogBuild(int tableCount, ControlBlock controlBlock)
        {

        }

        public void LogInvalidBuild(int recommendedBlockSize)
        {

        }

        public void LogSearch(string type, CrossRoadKey key, CrossRoad crossRoad, int blockIndex)
        {

        }

        public void LogRemoveRecord(CrossRoadKey key, CrossRoad crossRoad, int blockIndex)
        {

        }

        public void LogAddRecord(CrossRoadKey key, int blockIndex, int recordIndex)
        {

        }

        public void LogReloadIndex()
        {
        }

        public void LogBlockRead(int blockIndex)
        {

        }

        public void LogObsoleteIndex()
        {
        }

        public void LogClearBuffer(int blockIndex)
        {

        }

        #endregion
    }
}
