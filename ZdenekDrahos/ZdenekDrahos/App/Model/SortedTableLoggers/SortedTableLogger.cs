﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Text;
using ZdenekDrahos.App.Model.Files;
using ZdenekDrahos.Files.Block.Blocks;

namespace ZdenekDrahos.App.Model.SortedTableLoggers
{
    class SortedTableLogger : ISortedTableLogger
    {

        private StringBuilder textBuilder = new StringBuilder();

        public string Text { get; private set; }
        public event EventHandler LogAction;

        public void LogBuild(int tableCount, ControlBlock controlBlock)
        {
            textBuilder.AppendLine("Vybudování stromu:");
            textBuilder.AppendLine(string.Format("Počet záznamů: {0}", tableCount));
            textBuilder.AppendLine(string.Format("Počet záznamů v bloku: {0}", controlBlock.RecordsInBlockCount));
            textBuilder.AppendLine(string.Format("Velikost řídícího bloku: {0}", controlBlock.ControlBlockSize));
            textBuilder.AppendLine(string.Format("Velikost bloku: {0}", controlBlock.BlockSize));
            textBuilder.AppendLine(string.Format("Počet bloků: {0}", controlBlock.AllocatedBlocksCount));
            OnLogAction();
        }

        public void LogInvalidBuild(int recommendedBlockSize)
        {
            textBuilder.AppendLine("Vybudování stromu:");
            textBuilder.AppendLine(string.Format("Moc malá velikost bloku, velikost musí být minimálně {0}", recommendedBlockSize));
            OnLogAction();
        }

        public void LogSearch(string type, CrossRoadKey key, CrossRoad crossRoad, int blockIndex)
        {
            textBuilder.AppendLine(string.Format("{0}: [{1}, {2}]", type, key.X, key.Y));
            if (crossRoad != null)
            {
                textBuilder.AppendLine(string.Format(
                    "Nalezená křižovatka: id = {0}, je zastávka = {1}, je odpočívadlo = {2}",
                    crossRoad.ID, crossRoad.IsBusStop, crossRoad.IsRestArea
                ));
                textBuilder.AppendLine(string.Format("Nalezeno v bloku: {0}", blockIndex));
            }
            else
            {
                textBuilder.AppendLine("Bod s danými souřadnicemi se v souboru nenachází");
            }
            OnLogAction();
        }

        public void LogRemoveRecord(CrossRoadKey key, CrossRoad crossRoad, int blockIndex)
        {
            textBuilder.AppendLine(string.Format("Odstranění záznamu: [{0}, {1}]", key.X, key.Y));
            if (crossRoad != null)
            {
                textBuilder.AppendLine(string.Format(
                    "Odstraněná křižovatka: id = {0}, je zastávka = {1}, je odpočívadlo = {2}",
                    crossRoad.ID, crossRoad.IsBusStop, crossRoad.IsRestArea
                ));
                textBuilder.AppendLine(string.Format("Odebrána z bloku: {0}", blockIndex));
            }
            else
            {
                textBuilder.AppendLine("Bod s danými souřadnicemi se v souboru nenachází");
            }
            OnLogAction();
        }

        public void LogAddRecord(CrossRoadKey key, int blockIndex, int recordIndex)
        {
            textBuilder.AppendLine(string.Format("Přidání záznamu: [{0}, {1}]", key.X, key.Y));
            if (recordIndex != -1)
                textBuilder.AppendLine(string.Format("Úspěšně zařazeno do bloku {0} jako {1}.záznam", blockIndex, recordIndex));
            else
                textBuilder.AppendLine("Chyba: duplicitní křižovatka");
            OnLogAction();
        }

        public void LogReloadIndex()
        {
            textBuilder.AppendLine("Znovu jsem načetl index, který byl vytvořen po spuštění aplikace (může obsahovat smazané vrcholy)");
            OnLogAction();
        }

        public void LogBlockRead(int blockIndex)
        {
            textBuilder.AppendLine(string.Format("Načten blok {0}", blockIndex));
        }

        public void LogObsoleteIndex()
        {
            textBuilder.AppendLine("Uložená hodnota v indexu je zastaralá");
        }

        public void LogClearBuffer(int blockIndex)
        {
            textBuilder.AppendLine("Vymazání bufferu:");
            textBuilder.AppendLine(string.Format("Z operační paměti odebrán blok {0}", blockIndex));
            OnLogAction();
        }

        protected virtual void OnLogAction()
        {
            Text = textBuilder.ToString();
            textBuilder.Clear();
            EventHandler handler = this.LogAction;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

    }
}
