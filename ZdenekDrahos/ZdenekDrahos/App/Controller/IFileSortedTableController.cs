﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.App.Model.Files;
using ZdenekDrahos.App.Model.SortedTableLoggers;

namespace ZdenekDrahos.App.Controller
{
    public interface IFileSortedTableController
    {

        ISortedTableLogger Logger { get; set; }

        Func<int, List<KeyValuePair<CrossRoadKey, CrossRoad>>> Generator { get; set; }

        void Build(int recordsCount, int recordsInBlock, int blockSize);

        void BinarySearch(CrossRoadKey key);

        void InterpolationSearch(CrossRoadKey key);

        void IndexSearch(CrossRoadKey key);

        void RemoveRecord(CrossRoadKey key);

        void IndexRemoveRecord(CrossRoadKey key);

        void IndexAdd(CrossRoadKey key, CrossRoad crossRoad);

        void ReloadIndex();

        void ClearBuffer();

    }
}
