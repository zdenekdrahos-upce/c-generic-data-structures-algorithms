﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.App.Model.PathMatrix;
using ZdenekDrahos.Graph;
using ZdenekDrahos.Graph.ShortestPath;
using ZdenekDrahos.Graph.ShortestPath.Floyd;

namespace ZdenekDrahos.App.Controller
{
    class ForestController : IForestController
    {
        private IForestArea forestArea;
        private NodesPathRestriction<string, ICrossRoad, double> pathRestriction;
        private IForestDataMapper dataMapper;
        private IFloydDataMapper floydMapper;

        public ForestController()
        {
            CrossRoadFactory factory = new CrossRoadFactory();
            dataMapper = new ForestDataMapper(factory);
            floydMapper = new FloydDataMapper();            
            forestArea = new ForestArea();
            forestArea.SetCrossRoadFactory(factory);
        }

        #region IForestController Members
        
        public event ForestChangedEventHandler ForestChanged;

        public void AddCrossRoad(Point point, IList<KeyValuePair<string, double>> neighbours)
        {
            forestArea.AddCrossRoad(point);
            forestArea.AddNeighboursToLastCrossRoad(neighbours);
            OnForestChanged();
        }

        public void AddBusStop(Point point, IList<KeyValuePair<string, double>> neighbours)
        {
            forestArea.AddBusStop(point);
            forestArea.AddNeighboursToLastCrossRoad(neighbours);
            OnForestChanged();
        }

        public void AddRestArea(Point point, IList<KeyValuePair<string, double>> neighbours)
        {
            forestArea.AddRestArea(point);
            forestArea.AddNeighboursToLastCrossRoad(neighbours);
            OnForestChanged();
        }

        public void RemoveCrossRoad(string idCrossroad)
        {
            forestArea.RemoveCrossRoad(idCrossroad);
            OnForestChanged();
        }

        public Path<ICrossRoad> FindDijkstraShortestPaths(string startNode, string endNode)
        {
            return forestArea.FindDijkstraShortestPaths(startNode, endNode);
        }

        public Path<ICrossRoad> FindFloydShortestPaths(string startNode, string endNode)
        {
            return forestArea.FindFloydShortestPaths(startNode, endNode);
        }

        public PathMatrix<string, ICrossRoad, double> GetFloydMatrix()
        {
            return forestArea.FloydMatrix;
        }

        public IEnumerable<ICrossRoad> CrossRoadIterator()
        {
            return forestArea.CrossRoadIterator();
        }

        public IEnumerable<ICrossRoad> BusStopIterator()
        {
            return forestArea.BusStopIterator();
        }

        public IEnumerable<ICrossRoad> RestAreaIterator()
        {
            return forestArea.RestAreaIterator();
        }

        public IEnumerable<Edge<string, ICrossRoad, double>> EdgesIterator()
        {
            return forestArea.EdgesIterator();
        }

        public IEnumerable<string> CrossRoadKeysIterator()
        {
            return forestArea.CrossRoadKeysIterator();
        }

        public NodesPathRestriction<string, ICrossRoad, double> PathRestriction
        {
            get { return pathRestriction; }
            set { SetPathRestriction(value); OnForestChanged(); }
        }

        public bool SaveForest(string pathToFile)
        {
            try
            {
                dataMapper.SaveForest(forestArea, pathToFile);
                return true;
            }
            catch (Exception)
            {
                return false;
            }     
        }

        public bool LoadForest(string pathToFile)
        {
            try
            {
                forestArea = dataMapper.LoadForest(pathToFile);
                LoadFloydMatrix();
                OnForestChanged();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ReloadAndSaveFloydMatrix()
        {
            try
            {
                forestArea.ReloadFloydMatrix();
                floydMapper.SavePathMatrix(forestArea.FloydMatrix, "matrix.bin");
                return true;
            }
            catch (Exception)
            {
                return false;
            }                        
        }

        public bool LoadFloydMatrix()
        {
            try
            {
                forestArea.FloydMatrix = floydMapper.LoadPathMatrix("matrix.bin");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void BuildRangeTree()
        {
            forestArea.BuildRangeTree();
        }

        public ICrossRoad BasicSearchInRangeTree(Point point)
        {
            return forestArea.BasicSearchInRangeTree(point);
        }

        public void UpdateDisabledNodeViaIntervalSearch(Rectangle rect) 
        {
            NodesPathRestriction<string, ICrossRoad, double> restriction = new NodesPathRestriction<string, ICrossRoad, double>();
            restriction.DisabledNodes = IntervalSearchInRangeTree(rect);
            PathRestriction = restriction;
        }

        public IList<ICrossRoad> IntervalSearchInRangeTree(Rectangle rect)
        {
            return forestArea.IntervalSearchInRangeTree(rect);
        }

        #endregion

        private void SetPathRestriction(NodesPathRestriction<string, ICrossRoad, double> restriction)
        {
            if (restriction == null)
                forestArea.SetPathRestriction(null);
            else
            {
                pathRestriction = restriction;
                forestArea.SetPathRestriction(restriction.IsEdgeAccessible);
            }
        }

        protected virtual void OnForestChanged()
        {
            ForestChangedEventHandler handler = this.ForestChanged;
            if (handler != null) 
                handler(this, EventArgs.Empty);
        }
    }
}
