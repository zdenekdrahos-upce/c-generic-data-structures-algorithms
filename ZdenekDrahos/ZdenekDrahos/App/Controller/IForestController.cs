﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using System.Drawing;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph;
using ZdenekDrahos.Graph.ShortestPath;
using ZdenekDrahos.Graph.ShortestPath.Floyd;

namespace ZdenekDrahos.App.Controller
{
    public interface IForestController
    {

        event ForestChangedEventHandler ForestChanged;

        void AddCrossRoad(Point point, IList<KeyValuePair<string, double>> neighbours);

        void AddBusStop(Point point, IList<KeyValuePair<string, double>> neighbours);

        void AddRestArea(Point point, IList<KeyValuePair<string, double>> neighbours);

        void RemoveCrossRoad(string idCrossroad);

        Path<ICrossRoad> FindDijkstraShortestPaths(string startNode, string endNode);

        Path<ICrossRoad> FindFloydShortestPaths(string startNode, string endNode);

        PathMatrix<string, ICrossRoad, double> GetFloydMatrix();

        IEnumerable<ICrossRoad> CrossRoadIterator();

        IEnumerable<ICrossRoad> BusStopIterator();

        IEnumerable<ICrossRoad> RestAreaIterator();

        IEnumerable<Edge<string, ICrossRoad, double>> EdgesIterator();

        IEnumerable<string> CrossRoadKeysIterator();

        NodesPathRestriction<string, ICrossRoad, double> PathRestriction { get; set; }

        bool SaveForest(string pathToFile);

        bool LoadForest(string pathToFile);

        bool ReloadAndSaveFloydMatrix();

        bool LoadFloydMatrix();

        void BuildRangeTree();

        ICrossRoad BasicSearchInRangeTree(Point point);

        void UpdateDisabledNodeViaIntervalSearch(Rectangle rect);

        IList<ICrossRoad> IntervalSearchInRangeTree(Rectangle rect);

    }
}
