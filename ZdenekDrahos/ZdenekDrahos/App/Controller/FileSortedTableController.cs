﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.IO;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.App.Model.Files;
using ZdenekDrahos.App.Model.SortedTableLoggers;
using ZdenekDrahos.Files.Block;
using ZdenekDrahos.Files.Block.Blocks;
using ZdenekDrahos.Files.Index;
using ZdenekDrahos.Table.ExternalMemory;
using ZdenekDrahos.Tree.Tree23.RedBlack;
using ZdenekDrahos.Tree.Tree23.RedBlack.Export;

namespace ZdenekDrahos.App.Controller
{
    public class FileSortedTableController : IFileSortedTableController
    {

        private string filename;
        private ISortedTableLogger logger;
        private IFileSortedTable<CrossRoadKey, CrossRoad> table;
        private IndexBuilder<CrossRoadKey, CrossRoad> indexBuilder;
        private IIndexAccessStructure<CrossRoadKey, CrossRoad> index;
        private IRedBlackTreeDataMapper<CrossRoadKey, KeyIndex> indexDataMapper;

        public FileSortedTableController(string filename)
        {
            this.filename = filename;
            logger = NullSortedTableLogger.Instance;
            indexBuilder = new IndexBuilder<CrossRoadKey, CrossRoad>();
            indexDataMapper = new RedBlackTreeDataMapper<CrossRoadKey, KeyIndex>(GetIndexFile());
        }

        ~FileSortedTableController()
        {
            SaveIndex();
        }

        #region IFileSortedTableController Members

        public ISortedTableLogger Logger
        {
            get { return logger; }
            set { logger = value ?? NullSortedTableLogger.Instance; }
        }

        public Func<int, List<KeyValuePair<CrossRoadKey, CrossRoad>>> Generator { get; set; }

        public void Build(int recordsCount, int recordsInBlock, int blockSize)
        {
            if (File.Exists(filename))
                File.Delete(filename);
            table = new FileSortedTable<CrossRoadKey, CrossRoad>(filename, recordsInBlock, blockSize);
            table.File.BlockFile.BlockRead += this.OnBlockRead;
            table.KeyNumberConverter = (key) => (double)(key.X);
            try
            {
                table.Build(Generator(recordsCount));
                index = indexBuilder.BuildFileIndex(table.File.BlockFile);
                AddIndexObsoleteHandler();
                SaveIndex();
                logger.LogBuild(table.Count, table.File.BlockFile.ControlBlock);
            }
            catch (ArgumentException exception)
            {
                logger.LogInvalidBuild(int.Parse(exception.Message));
            }
        }

        public void BinarySearch(CrossRoadKey key)
        {
            SearchAndLog(key, table.FindBinarySearch, "Binární vyhledávání");
        }

        public void InterpolationSearch(CrossRoadKey key)
        {
            SearchAndLog(key, table.FindInterpolationSearch, "Interpolační vyhledávání");
        }

        public void IndexSearch(CrossRoadKey key)
        {
            SearchAndLog(key, index.Find, "Hledání přes RBT index");
        }

        public void RemoveRecord(CrossRoadKey key)
        {
            RemoveAndLog(key, table.Remove);
        }

        public void IndexRemoveRecord(CrossRoadKey key)
        {
            RemoveAndLog(key, index.Remove);
        }

        public void IndexAdd(CrossRoadKey key, CrossRoad crossRoad)
        {
            int recordIndex = -1;
            try
            {
                index.Add(key, crossRoad);
                recordIndex = table.File.BlockFile.Buffer.GetIndexOfKey(key);
            }
            catch (ArgumentException) { } // duplicate key
            logger.LogAddRecord(key, table.File.BlockFile.Buffer.BlockIndex, recordIndex);
        }

        public void ClearBuffer()
        {
            logger.LogClearBuffer(table.File.BlockFile.Buffer.BlockIndex);
            table.File.BlockFile.Buffer.Clear();
        }

        public void ReloadIndex()
        {
            index = indexBuilder.BuildFileIndex(table.File.BlockFile, indexDataMapper.Load());
            AddIndexObsoleteHandler();
            logger.LogReloadIndex();
        }

        #endregion

        public void SearchAndLog(CrossRoadKey key, Func<CrossRoadKey, CrossRoad> searchMethod, string type)
        {
            CrossRoad crossRoad = null;
            try
            {
                crossRoad = searchMethod(key);
            }
            catch (KeyNotFoundException) { }
            logger.LogSearch(type, key, crossRoad, table.File.BlockFile.Buffer.BlockIndex);
        }

        private void RemoveAndLog(CrossRoadKey key, Func<CrossRoadKey, CrossRoad> removeMethod)
        {
            CrossRoad crossRoad = null;
            try
            {
                crossRoad = removeMethod(key);
            }
            catch (KeyNotFoundException) { }
            logger.LogRemoveRecord(key, crossRoad, table.File.BlockFile.Buffer.BlockIndex);
        }

        private void OnBlockRead(object sender, BlockReadEventArgs e)
        {
            logger.LogBlockRead(e.Block);
        }

        private void OnObsoleteValue(object sender, EventArgs e)
        {
            logger.LogObsoleteIndex();
        }

        private void SaveIndex()
        {
            indexDataMapper.Save(index.Index);
        }

        private void AddIndexObsoleteHandler()
        {
            index.ObsoleteValueInIndex += this.OnObsoleteValue;
        }

        private BlockFile<CrossRoadKey, RedBlackTreeValue<KeyIndex>> GetIndexFile()
        {
            string indexFilename = "index_" + filename;
            if (File.Exists(indexFilename))
                File.Delete(indexFilename);
            int indexRecordsInBlock = 1000;
            IBlockByteConverter<CrossRoadKey, RedBlackTreeValue<KeyIndex>> conv = new IndexByteConverter();
            return new BlockFile<CrossRoadKey, RedBlackTreeValue<KeyIndex>>(indexFilename, indexRecordsInBlock, 0, conv);
        }

    }
}
