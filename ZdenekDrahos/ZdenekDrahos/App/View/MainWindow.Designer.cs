﻿namespace ZdenekDrahos
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.načístToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uložitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.konecProgramuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cestyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.najítNejkratšíCestuDijkstraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maticeSměrováníFloydToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editaceGrafuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nováKřižovatkaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odstranitKřižovatkuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editovatKřižovatkuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.správaZakázanýchCestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.b2DRangeTreeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vybudováníToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jednoduchéVyhledáváníToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intervalSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.cSouboryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.demonstraceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.souborToolStripMenuItem,
            this.cestyToolStripMenuItem,
            this.editaceGrafuToolStripMenuItem,
            this.b2DRangeTreeToolStripMenuItem,
            this.cSouboryToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(844, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // souborToolStripMenuItem
            // 
            this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.načístToolStripMenuItem,
            this.uložitToolStripMenuItem,
            this.toolStripSeparator1,
            this.konecProgramuToolStripMenuItem});
            this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            this.souborToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.souborToolStripMenuItem.Text = "Soubor";
            // 
            // načístToolStripMenuItem
            // 
            this.načístToolStripMenuItem.Name = "načístToolStripMenuItem";
            this.načístToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.načístToolStripMenuItem.Text = "Načíst...";
            this.načístToolStripMenuItem.Click += new System.EventHandler(this.načístToolStripMenuItem_Click);
            // 
            // uložitToolStripMenuItem
            // 
            this.uložitToolStripMenuItem.Name = "uložitToolStripMenuItem";
            this.uložitToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.uložitToolStripMenuItem.Text = "Uložit...";
            this.uložitToolStripMenuItem.Click += new System.EventHandler(this.uložitToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(160, 6);
            // 
            // konecProgramuToolStripMenuItem
            // 
            this.konecProgramuToolStripMenuItem.Name = "konecProgramuToolStripMenuItem";
            this.konecProgramuToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.konecProgramuToolStripMenuItem.Text = "Konec programu";
            this.konecProgramuToolStripMenuItem.Click += new System.EventHandler(this.konecProgramuToolStripMenuItem_Click);
            // 
            // cestyToolStripMenuItem
            // 
            this.cestyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.najítNejkratšíCestuDijkstraToolStripMenuItem,
            this.maticeSměrováníFloydToolStripMenuItem});
            this.cestyToolStripMenuItem.Name = "cestyToolStripMenuItem";
            this.cestyToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.cestyToolStripMenuItem.Text = "Cesty";
            // 
            // najítNejkratšíCestuDijkstraToolStripMenuItem
            // 
            this.najítNejkratšíCestuDijkstraToolStripMenuItem.Name = "najítNejkratšíCestuDijkstraToolStripMenuItem";
            this.najítNejkratšíCestuDijkstraToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.najítNejkratšíCestuDijkstraToolStripMenuItem.Text = "Najít nejkratší cestu (Dijkstra)...";
            this.najítNejkratšíCestuDijkstraToolStripMenuItem.Click += new System.EventHandler(this.najítNejkratšíCestuDijkstraToolStripMenuItem_Click);
            // 
            // maticeSměrováníFloydToolStripMenuItem
            // 
            this.maticeSměrováníFloydToolStripMenuItem.Name = "maticeSměrováníFloydToolStripMenuItem";
            this.maticeSměrováníFloydToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.maticeSměrováníFloydToolStripMenuItem.Text = "Matice směrování (Floyd)...";
            this.maticeSměrováníFloydToolStripMenuItem.Click += new System.EventHandler(this.maticeSměrováníFloydToolStripMenuItem_Click);
            // 
            // editaceGrafuToolStripMenuItem
            // 
            this.editaceGrafuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nováKřižovatkaToolStripMenuItem,
            this.odstranitKřižovatkuToolStripMenuItem,
            this.editovatKřižovatkuToolStripMenuItem,
            this.správaZakázanýchCestToolStripMenuItem});
            this.editaceGrafuToolStripMenuItem.Name = "editaceGrafuToolStripMenuItem";
            this.editaceGrafuToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.editaceGrafuToolStripMenuItem.Text = "Editace grafu";
            // 
            // nováKřižovatkaToolStripMenuItem
            // 
            this.nováKřižovatkaToolStripMenuItem.Name = "nováKřižovatkaToolStripMenuItem";
            this.nováKřižovatkaToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.nováKřižovatkaToolStripMenuItem.Text = "Nová křižovatka";
            this.nováKřižovatkaToolStripMenuItem.Click += new System.EventHandler(this.nováKřižovatkaToolStripMenuItem_Click);
            // 
            // odstranitKřižovatkuToolStripMenuItem
            // 
            this.odstranitKřižovatkuToolStripMenuItem.Name = "odstranitKřižovatkuToolStripMenuItem";
            this.odstranitKřižovatkuToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.odstranitKřižovatkuToolStripMenuItem.Text = "Odstranit křižovatku";
            this.odstranitKřižovatkuToolStripMenuItem.Click += new System.EventHandler(this.odstranitKřižovatkuToolStripMenuItem_Click);
            // 
            // editovatKřižovatkuToolStripMenuItem
            // 
            this.editovatKřižovatkuToolStripMenuItem.Name = "editovatKřižovatkuToolStripMenuItem";
            this.editovatKřižovatkuToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.editovatKřižovatkuToolStripMenuItem.Text = "Přehled křižovatek";
            this.editovatKřižovatkuToolStripMenuItem.Click += new System.EventHandler(this.editovatKřižovatkuToolStripMenuItem_Click);
            // 
            // správaZakázanýchCestToolStripMenuItem
            // 
            this.správaZakázanýchCestToolStripMenuItem.Name = "správaZakázanýchCestToolStripMenuItem";
            this.správaZakázanýchCestToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.správaZakázanýchCestToolStripMenuItem.Text = "Správa zakázaných cest (výběr ze seznamu)";
            this.správaZakázanýchCestToolStripMenuItem.Click += new System.EventHandler(this.správaZakázanýchCestToolStripMenuItem_Click);
            // 
            // b2DRangeTreeToolStripMenuItem
            // 
            this.b2DRangeTreeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vybudováníToolStripMenuItem,
            this.jednoduchéVyhledáváníToolStripMenuItem,
            this.intervalSearchToolStripMenuItem});
            this.b2DRangeTreeToolStripMenuItem.Name = "b2DRangeTreeToolStripMenuItem";
            this.b2DRangeTreeToolStripMenuItem.Size = new System.Drawing.Size(107, 20);
            this.b2DRangeTreeToolStripMenuItem.Text = "B - 2D range tree";
            // 
            // vybudováníToolStripMenuItem
            // 
            this.vybudováníToolStripMenuItem.Name = "vybudováníToolStripMenuItem";
            this.vybudováníToolStripMenuItem.Size = new System.Drawing.Size(338, 22);
            this.vybudováníToolStripMenuItem.Text = "Vybudování";
            this.vybudováníToolStripMenuItem.Click += new System.EventHandler(this.vybudováníToolStripMenuItem_Click);
            // 
            // jednoduchéVyhledáváníToolStripMenuItem
            // 
            this.jednoduchéVyhledáváníToolStripMenuItem.Name = "jednoduchéVyhledáváníToolStripMenuItem";
            this.jednoduchéVyhledáváníToolStripMenuItem.Size = new System.Drawing.Size(338, 22);
            this.jednoduchéVyhledáváníToolStripMenuItem.Text = "Jednoduché vyhledávání";
            this.jednoduchéVyhledáváníToolStripMenuItem.Click += new System.EventHandler(this.jednoduchéVyhledáváníToolStripMenuItem_Click);
            // 
            // intervalSearchToolStripMenuItem
            // 
            this.intervalSearchToolStripMenuItem.Name = "intervalSearchToolStripMenuItem";
            this.intervalSearchToolStripMenuItem.Size = new System.Drawing.Size(338, 22);
            this.intervalSearchToolStripMenuItem.Text = "Intervalové vyhledávání pro výběr zakázaných cest";
            this.intervalSearchToolStripMenuItem.Click += new System.EventHandler(this.intervalovéVyhledáváníProVýběrZakázanýchCestToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(0, 24);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(844, 498);
            this.textBox1.TabIndex = 1;
            this.textBox1.Visible = false;
            // 
            // mainPanel
            // 
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 24);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(844, 498);
            this.mainPanel.TabIndex = 2;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "forest";
            this.openFileDialog1.Filter = "TXT file|*.txt";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "forest";
            this.saveFileDialog1.Filter = "TXT file|*.txt";
            // 
            // cSouboryToolStripMenuItem
            // 
            this.cSouboryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.demonstraceToolStripMenuItem});
            this.cSouboryToolStripMenuItem.Name = "cSouboryToolStripMenuItem";
            this.cSouboryToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.cSouboryToolStripMenuItem.Text = "C - soubory";
            // 
            // demonstraceToolStripMenuItem
            // 
            this.demonstraceToolStripMenuItem.Name = "demonstraceToolStripMenuItem";
            this.demonstraceToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.demonstraceToolStripMenuItem.Text = "Demonstrace";
            this.demonstraceToolStripMenuItem.Click += new System.EventHandler(this.demonstraceToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 522);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zdeněk Drahoš - INDSA, INPG2";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.MainWindow_HelpButtonClicked);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem načístToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uložitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cestyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem najítNejkratšíCestuDijkstraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maticeSměrováníFloydToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editaceGrafuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editovatKřižovatkuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem správaZakázanýchCestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem konecProgramuToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem nováKřižovatkaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem odstranitKřižovatkuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem b2DRangeTreeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vybudováníToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jednoduchéVyhledáváníToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intervalSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cSouboryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem demonstraceToolStripMenuItem;
    }
}

