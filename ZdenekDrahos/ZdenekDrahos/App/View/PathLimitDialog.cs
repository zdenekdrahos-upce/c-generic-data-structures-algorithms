﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using System.Windows.Forms;
using ZdenekDrahos.App.Controller;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph.ShortestPath;

namespace ZdenekDrahos.App.View
{
    public delegate void PathLimitEventHandler(object sender, AppEventArgs<NodesPathRestriction<string, ICrossRoad, double>> e);

    public partial class PathLimitDialog : Form
    {
        public event PathLimitEventHandler PathLimit;

        private NodesPathRestriction<string, ICrossRoad, double> restriction;

        public PathLimitDialog()
        {
            restriction = new NodesPathRestriction<string, ICrossRoad, double>();
            InitializeComponent();
        }

        public void Setup(IForestController controller)
        {
            LoadRestriction(controller.PathRestriction);
            LoadCheckBoxList(controller.CrossRoadIterator());
        }

        private void LoadRestriction(NodesPathRestriction<string, ICrossRoad, double> nodesPathRestriction)
        {
            if (nodesPathRestriction != null)
                restriction = nodesPathRestriction;
        }

        private void LoadCheckBoxList(IEnumerable<ICrossRoad> crossRoadIterator)
        {
            try
            {
                checkedListBox.BeginUpdate();
                checkedListBox.Items.Clear();
                bool isChecked;
                foreach (ICrossRoad cr in crossRoadIterator)
                {
                    isChecked = restriction != null ? restriction.DisabledNodes.Contains(cr) : false;
                    checkedListBox.Items.Add(cr, isChecked);
                }
            }
            finally
            {
                checkedListBox.EndUpdate();
            }  
        }

        private void PathLimitDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                restriction.DisabledNodes.Clear();
                foreach (ICrossRoad item in checkedListBox.CheckedItems)
                    restriction.DisabledNodes.Add(item);
                OnDeleteCrossRoad(new AppEventArgs<NodesPathRestriction<string, ICrossRoad, double>>(restriction));
            }
        }

        protected virtual void OnDeleteCrossRoad(AppEventArgs<NodesPathRestriction<string, ICrossRoad, double>> e)
        {
            PathLimitEventHandler handler = this.PathLimit;
            if (handler != null)
                handler(this, e);
        }  

    }
}
