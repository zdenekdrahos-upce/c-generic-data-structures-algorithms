﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Data;
using System.Windows.Forms;
using ZdenekDrahos.App.Controller;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph.ShortestPath;
using ZdenekDrahos.Graph.ShortestPath.Floyd;

namespace ZdenekDrahos.App.View
{
    public partial class ShortestPathMatrixDialog : Form
    {
        public event DrawPathEventHandler DrawPath;

        private IForestController controller;
        private PathMatrix<string, ICrossRoad, double> pathMatrix;

        public ShortestPathMatrixDialog(IForestController controller)
        {
            this.controller = controller;
            InitializeComponent();
        }

        private void ShortestPathMatrixDialog_Load(object sender, EventArgs e)
        {
            pathMatrix = controller.GetFloydMatrix();
            ShortestPathPanel shPanel = new ShortestPathPanel();
            shPanel.LoadBusStops(controller.BusStopIterator());
            shPanel.LoadRestAreas(controller.RestAreaIterator());
            shPanel.FoundPath = this.FindShortestPaths;
            panelSearch.Controls.Clear();
            panelSearch.Controls.Add(shPanel);
            if (pathMatrix != null)
            {
                DisplayMatrix(ref dataGridViewDistance, pathMatrix.DistanceMatrix, true);
                DisplayMatrix(ref dataGridViewPredecessor, pathMatrix.PredecessorMatrix, false);
            }
        }

        public Path<ICrossRoad> FindShortestPaths(string startNode, string endNode)
        {
            Path<ICrossRoad> path;
            try
            {
                path = controller.FindFloydShortestPaths(startNode, endNode);
            }
            catch (Exception)
            {
                path = new Path<ICrossRoad>();
            }
            OnPathFound(path);
            return path;
        }

        private void DisplayMatrix(ref DataGridView datagrid, double[,] matrix, bool isRowNumber)
        {
            int size = pathMatrix.IndexKeyMap.Count;        
            DataTable dataTable = new DataTable();
            // columns
            for (int i = 0; i < size; i++)
                dataTable.Columns.Add(new DataColumn(pathMatrix.IndexKeyMap[i], typeof(string)));
            // rows
            DataRow row;
            for (int i = 0; i < size; i++)
            {
                row = dataTable.NewRow();
                for (int j = 0; j < size; j++)
                    if (isRowNumber)
                        row[j] = matrix[i,j] != double.MaxValue ? matrix[i,j].ToString() : "?";
                    else
                        row[j] = matrix[i,j] != -1 ? pathMatrix.IndexKeyMap[(int) matrix[i,j]] : "-";
                dataTable.Rows.Add(row);
            }
            // set & rows header
            datagrid.DataSource = dataTable;
            int rowNumber = 0;
            foreach (DataGridViewRow dgRow in datagrid.Rows)
                dgRow.HeaderCell.Value = pathMatrix.IndexKeyMap[rowNumber++];
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pathMatrix != null)
            {
                int rowNumber = 0;
                foreach (DataGridViewRow dgRow in dataGridViewPredecessor.Rows)
                    dgRow.HeaderCell.Value = pathMatrix.IndexKeyMap[rowNumber++];
            }
        }

        protected virtual void OnPathFound(Path<ICrossRoad> e)
        {
            DrawPathEventHandler handler = this.DrawPath;
            if (handler != null)
                handler(this, new AppEventArgs<Path<ICrossRoad>>(e));
        }

        private void buttonReloadSave_Click(object sender, EventArgs e)
        {
            if (controller.ReloadAndSaveFloydMatrix())
            {
                ShortestPathMatrixDialog_Load(sender, e);
                MessageBox.Show("Matice byly přepočítány a aktuální verze byla uložena");
            }
            else
                MessageBox.Show("Matice se nepodařilo uložit :(");
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            if (controller.LoadFloydMatrix())
            {
                ShortestPathMatrixDialog_Load(sender, e);
                MessageBox.Show("Matice byla úspěšně načtena ze souboru");
            }
            else
                MessageBox.Show("Matice se nepodařilo načíst, asi ještě nebyla uložena");
        }   

    }
}
