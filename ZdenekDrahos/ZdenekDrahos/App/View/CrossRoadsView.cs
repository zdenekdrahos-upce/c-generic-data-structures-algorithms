﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Data;
using System.Windows.Forms;
using ZdenekDrahos.App.Controller;
using ZdenekDrahos.App.Model;

namespace ZdenekDrahos.App.View
{
    public partial class CrossRoadsView : Form
    {

        private IForestController controller;

        public CrossRoadsView(IForestController controller)
        {
            this.controller = controller;
            InitializeComponent();
        }

        private void CrossRoadsView_Load(object sender, EventArgs e)
        {
            DataTable dataTable = new DataTable();
            // columns
            dataTable.Columns.Add(new DataColumn("ID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Bod.X", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Bod.Y", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Zastávka", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Odpočívadlo", typeof(bool)));
            // rows
            DataRow row;
            foreach (ICrossRoad node in controller.CrossRoadIterator())
            {
                row = dataTable.NewRow();
                row[0] = node.ID;
                row[1] = node.X;
                row[2] = node.Y;
                row[3] = node.IsBusStop;
                row[4] = node.IsRestArea;
                dataTable.Rows.Add(row);
            }
            // set & rows header
            dataGridView1.DataSource = dataTable;
            dataGridView1.Columns[0].ReadOnly = true;
        }
    }
}
