﻿namespace ZdenekDrahos.App.View
{
    partial class ForestVisualizationPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ForestVisualizationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.DoubleBuffered = true;
            this.Name = "ForestVisualizationPanel";
            this.Size = new System.Drawing.Size(850, 550);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.panelForest_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelForest_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelForest_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelForest_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion


    }
}
