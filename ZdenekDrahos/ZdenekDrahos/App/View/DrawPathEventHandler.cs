﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph.ShortestPath;

namespace ZdenekDrahos.App.View
{

    public delegate void DrawPathEventHandler(object sender, AppEventArgs<Path<ICrossRoad>> e);

}
