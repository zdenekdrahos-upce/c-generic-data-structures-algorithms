﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using ZdenekDrahos.App.Model;

namespace ZdenekDrahos.App.View
{
    public delegate void CreateBusStopEventHandler(object sender, AppEventArgs<KeyValuePair<Point, IList<KeyValuePair<string, double>>>> e);
    public delegate void CreateRestAreaEventHandler(object sender, AppEventArgs<KeyValuePair<Point, IList<KeyValuePair<string, double>>>> e);
    public delegate void CreateCrossRoadEventHandler(object sender, AppEventArgs<KeyValuePair<Point, IList<KeyValuePair<string, double>>>> e);

    public partial class CreateCrossRoadForm : Form
    {
        public event CreateBusStopEventHandler CreateBusStop;
        public event CreateRestAreaEventHandler CreateRestArea;
        public event CreateCrossRoadEventHandler CreateCrossRoad;

        private Point point;
        private IList<KeyValuePair<string, double>> neighbours;

        public CreateCrossRoadForm(IEnumerable<ICrossRoad> crossRoads)
        {
            InitializeComponent();
            LoadNeighbours(crossRoads);
        }

        private void LoadNeighbours(IEnumerable<ICrossRoad> crossRoads)
        {
            DataTable dataTable = new DataTable();
            // columns
            dataTable.Columns.Add(new DataColumn("Křižovatka", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Je sousední?", typeof(bool)));            
            dataTable.Columns.Add(new DataColumn("Ohodnocení", typeof(double)));
            // rows
            DataRow row;
            foreach (ICrossRoad node in crossRoads)
            {
                row = dataTable.NewRow();
                row[0] = node.ID;
                row[1] = false;
                row[2] = 5;
                dataTable.Rows.Add(row);
            }
            // set & rows header
            dataGridView1.DataSource = dataTable;
            dataGridView1.Columns[0].ReadOnly = true;
        }

        private void CreateCrossRoadForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.Cancel || e.Cancel)
                return;
            e.Cancel = true;
            int integer;
            if (!int.TryParse(textBoxX.Text, out integer))
            {
                MessageBox.Show("X musí být integer");
                return;
            }
            if (!int.TryParse(textBoxY.Text, out integer))
            {
                MessageBox.Show("Y musí být integer");
                return;
            }
            e.Cancel = false;
        }

        private void CreateCrossRoadForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                point = new Point(int.Parse(textBoxX.Text), int.Parse(textBoxY.Text));
                neighbours = new List<KeyValuePair<string, double>>();
                DataTable table = (DataTable)dataGridView1.DataSource;
                double cost;
                foreach (DataRow row in table.Rows)
                    if (row[1].Equals(true) && double.TryParse(row[2].ToString(), out cost))
                        neighbours.Add(new KeyValuePair<string, double>(row[0].ToString(), cost));

                if (checkBoxBus.Checked)
                    OnCreateBusStop();
                else if (checkBoxRest.Checked)
                    OnCreateRestArea();
                else
                    OnCreateCrossRoad();
            }
        }

        protected virtual void OnCreateBusStop()
        {
            CreateBusStopEventHandler handler = this.CreateBusStop;
            if (handler != null) handler(this, GetEventArgs());
        }

        protected virtual void OnCreateRestArea()
        {
            CreateRestAreaEventHandler handler = this.CreateRestArea;
            if (handler != null) handler(this, GetEventArgs());
        }

        protected virtual void OnCreateCrossRoad()
        {
            CreateCrossRoadEventHandler handler = this.CreateCrossRoad;
            if (handler != null) handler(this, GetEventArgs());
        }

        private AppEventArgs<KeyValuePair<Point, IList<KeyValuePair<string, double>>>> GetEventArgs()
        {
            KeyValuePair<Point, IList<KeyValuePair<string, double>>> pair = new KeyValuePair<Point,IList<KeyValuePair<string,double>>>(point, neighbours);
            return new AppEventArgs<KeyValuePair<Point, IList<KeyValuePair<string, double>>>>(pair); 
        }
    }
}
