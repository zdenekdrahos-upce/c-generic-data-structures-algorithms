﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.ComponentModel;
using System.Windows.Forms;
using ZdenekDrahos.App.Controller;
using ZdenekDrahos.App.Model.Files;
using ZdenekDrahos.App.Model.SortedTableLoggers;

namespace ZdenekDrahos.App.View
{
    public partial class FileSortedTableView : Form
    {

        private IFileSortedTableController controller;
        private CrossRoadsGenerator generator;

        public FileSortedTableView()
        {
            InitializeComponent();
            controller = new FileSortedTableController("crossroads.bin");
            controller.Logger = new SortedTableLogger();
            controller.Logger.LogAction += this.OnLogAction;
            generator = new CrossRoadsGenerator();
            controller.Generator = generator.Generate;
        }

        private void OnLogAction(object sender, EventArgs e)
        {
            textBoxLog.AppendText(controller.Logger.Text);
            textBoxLog.AppendText(Environment.NewLine);
        }

        private void FileSortedTableView_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            MessageBox.Show("Záznamy jsou vygenerovány jako body od 1 do počtu prvků, kde klíč vždy odpovídá X i Y souřadnici.");
        }

        private void buttonBuild_Click(object sender, EventArgs e)
        {
            controller.Build(
                ParseNumericUpDown(numericUpDownRecordsCount),
                ParseNumericUpDown(numericUpDownRecordsInBlock),
                ParseNumericUpDown(numericUpDownBlockSize)
            );
            buttonClearBuffer.Enabled = true;
            buttonBinarySearch.Enabled = true;
            buttonInterpolationSearch.Enabled = true;
            buttonRemoveRecord.Enabled = true;
            buttonIndexSearch.Enabled = true;
            buttonIndexRemove.Enabled = true;
            buttonIndexInsert.Enabled = true;
            buttonIndexReload.Enabled = true;
        }

        private void buttonBinarySearch_Click(object sender, EventArgs e)
        {
            controller.BinarySearch(GetKey());
        }

        private void buttonInterpolationSearch_Click(object sender, EventArgs e)
        {
            controller.InterpolationSearch(GetKey());
        }

        private void buttonRemoveRecord_Click(object sender, EventArgs e)
        {
            controller.RemoveRecord(GetKey());
        }

        private void buttonIndexSearch_Click(object sender, EventArgs e)
        {
            controller.IndexSearch(GetKey());
        }

        private void buttonIndexRemove_Click(object sender, EventArgs e)
        {
            controller.IndexRemoveRecord(GetKey());
        }

        private void buttonIndexInsert_Click(object sender, EventArgs e)
        {
            controller.IndexAdd(GetKey(), generator.GenerateCrossRoad());
        }

        private void buttonIndexReload_Click(object sender, EventArgs e)
        {
            controller.ReloadIndex();
        }

        private CrossRoadKey GetKey()
        {
            int x = ParseNumericUpDown(numericUpDownX);
            int y = ParseNumericUpDown(numericUpDownY);
            return new CrossRoadKey(x, y);
        }

        private int ParseNumericUpDown(NumericUpDown control)
        {
            return (int)control.Value;
        }

        private void buttonClearBuffer_Click(object sender, EventArgs e)
        {
            controller.ClearBuffer();
        }

        private void buttonClearLog_Click(object sender, EventArgs e)
        {
            textBoxLog.ResetText();
        }

    }
}
