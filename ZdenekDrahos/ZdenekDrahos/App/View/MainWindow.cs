﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ZdenekDrahos.App.Controller;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.App.View;
using ZdenekDrahos.Graph.ShortestPath;

namespace ZdenekDrahos
{
    public partial class MainWindow : Form
    {
        private IForestController controller;
        private ForestVisualizationPanel visualization;

        public MainWindow()
        {
            controller = new ForestController();
            controller.ForestChanged += this.OnForestChanged;
            visualization = new ForestVisualizationPanel(controller.CrossRoadIterator(), controller.EdgesIterator());
            visualization.RectangleDrawn += this.OnRectangleDrawn;
            InitializeComponent();
            AddVisualizationPanel();
        }

        private void AddVisualizationPanel()
        {
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(visualization);
        }

        private void najítNejkratšíCestuDijkstraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShortestPathDijkstraDialog dijkstraDialog = new ShortestPathDijkstraDialog(controller);
            dijkstraDialog.DrawPath += this.OnDrawPath;
            dijkstraDialog.Show();
        }

        private void maticeSměrováníFloydToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShortestPathMatrixDialog floydDialog = new ShortestPathMatrixDialog(controller);
            floydDialog.DrawPath += this.OnDrawPath;
            floydDialog.Show();
        }

        private void správaZakázanýchCestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (PathLimitDialog dialog = new PathLimitDialog())
            {
                dialog.PathLimit += this.OnChangePathLimit;
                dialog.Setup(controller);
                dialog.ShowDialog();
            }
        }

        private void OnChangePathLimit(object sender, AppEventArgs<NodesPathRestriction<string, ICrossRoad, double>> e)
        {
            controller.PathRestriction = e.Data;
        }

        private void MainWindow_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            MessageBox.Show(this.Text);
        }

        private void načístToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                if (controller.LoadForest(openFileDialog1.FileName))
                    MessageBox.Show("Soubor byl importován");
                else
                    MessageBox.Show("Soubor se nepodařilo importovat");
        }

        private void uložitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                if (controller.SaveForest(saveFileDialog1.FileName))
                    MessageBox.Show("Soubor byl uložen");
                else
                    MessageBox.Show("Soubor se nepodařilo uložit");
        }

        private void editovatKřižovatkuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CrossRoadsView dialog = new CrossRoadsView(controller);
            dialog.Show();
        }

        private void nováKřižovatkaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (CreateCrossRoadForm dialog = new CreateCrossRoadForm(controller.CrossRoadIterator()))
            {
                dialog.CreateBusStop += this.OnCreateBusStop;
                dialog.CreateRestArea += this.OnCreateRestArea;
                dialog.CreateCrossRoad += this.OnCreateCrossRoad;
                dialog.ShowDialog();
            }
        }

        private void OnCreateBusStop(object sender, AppEventArgs<KeyValuePair<Point, IList<KeyValuePair<string, double>>>> e)
        {
            controller.AddBusStop(e.Data.Key, e.Data.Value);
        }

        private void OnCreateRestArea(object sender, AppEventArgs<KeyValuePair<Point, IList<KeyValuePair<string, double>>>> e)
        {
            controller.AddRestArea(e.Data.Key, e.Data.Value);
        }

        private void OnCreateCrossRoad(object sender, AppEventArgs<KeyValuePair<Point, IList<KeyValuePair<string, double>>>> e)
        {
            controller.AddCrossRoad(e.Data.Key, e.Data.Value);
        }

        private void odstranitKřižovatkuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (DeleteCrossRoadForm dialog = new DeleteCrossRoadForm(controller.CrossRoadIterator()))
            {
                dialog.DeleteCrossRoad += this.OnDeleteCrossRoad;
                dialog.ShowDialog();
            }
        }

        private void OnDeleteCrossRoad(object sender, AppEventArgs<string> e)
        {
            controller.RemoveCrossRoad(e.Data);
        }

        private void OnDrawPath(object sender, AppEventArgs<Path<ICrossRoad>> e)
        {
            visualization.SetPath(e.Data);
            OnForestChanged(sender, e);
        }

        private void OnForestChanged(object sender, EventArgs e)
        {
            visualization.SetIterators(controller.CrossRoadIterator(), controller.EdgesIterator());
            if (controller.PathRestriction != null)
                visualization.SetDisabledNodes(controller.PathRestriction.DisabledNodes);
            mainPanel.Invalidate();
            mainPanel.Refresh();
        }

        private void konecProgramuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        # region RANGE_TREE

        private void vybudováníToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.BuildRangeTree();
            MessageBox.Show("Vyhledávání strom byl vybudován. Let the search begin!");
        }

        private void jednoduchéVyhledáváníToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BasicSearchInRangeTreeForm dialog = new BasicSearchInRangeTreeForm(controller);
            dialog.Show();
        }

        private void intervalovéVyhledáváníProVýběrZakázanýchCestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            visualization.IntervalSearchActive = !visualization.IntervalSearchActive;
            intervalSearchToolStripMenuItem.Checked = visualization.IntervalSearchActive;
        }

        public void OnRectangleDrawn(object sender, AppEventArgs<Rectangle> e)
        {
            controller.UpdateDisabledNodeViaIntervalSearch(e.Data);
        }

        # endregion

        private void demonstraceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (FileSortedTableView dialog = new FileSortedTableView())
            {
                dialog.ShowDialog();
            }
        }

    }

}
