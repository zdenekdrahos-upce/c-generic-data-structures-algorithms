﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Windows.Forms;
using ZdenekDrahos.App.Controller;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph.ShortestPath;

namespace ZdenekDrahos.App.View
{
    public partial class ShortestPathDijkstraDialog : Form
    {
        public event DrawPathEventHandler DrawPath;
        private IForestController controller;

        public ShortestPathDijkstraDialog(IForestController controller)
        {
            this.controller = controller;
            InitializeComponent();
        }

        private void ShortestPathDijkstraDialog_Load(object sender, EventArgs e)
        {
            ShortestPathPanel shPanel = new ShortestPathPanel();
            shPanel.LoadBusStops(controller.BusStopIterator());
            shPanel.LoadRestAreas(controller.RestAreaIterator());
            shPanel.FoundPath = this.FindShortestPaths;
            panel.Controls.Add(shPanel);
        }

        public Path<ICrossRoad> FindShortestPaths(string startNode, string endNode)
        {
            Path<ICrossRoad> path;
            try
            {                
                path = controller.FindDijkstraShortestPaths(startNode, endNode);               
            }
            catch (PathNotFoundException)
            {
                path = new Path<ICrossRoad>();
            }
            OnPathFound(path);
            return path;
        }

        protected virtual void OnPathFound(Path<ICrossRoad> e)
        {
            DrawPathEventHandler handler = this.DrawPath;
            if (handler != null)
                handler(this, new AppEventArgs<Path<ICrossRoad>>(e));
        }   
    }
}
