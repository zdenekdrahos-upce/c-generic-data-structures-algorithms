﻿namespace ZdenekDrahos.App.View
{
    partial class FileSortedTableView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonClearBuffer = new System.Windows.Forms.Button();
            this.numericUpDownBlockSize = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonBuild = new System.Windows.Forms.Button();
            this.numericUpDownRecordsInBlock = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownRecordsCount = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownY = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownX = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonRemoveRecord = new System.Windows.Forms.Button();
            this.buttonInterpolationSearch = new System.Windows.Forms.Button();
            this.buttonBinarySearch = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonClearLog = new System.Windows.Forms.Button();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonIndexSearch = new System.Windows.Forms.Button();
            this.buttonIndexInsert = new System.Windows.Forms.Button();
            this.buttonIndexRemove = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.buttonIndexReload = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlockSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecordsInBlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecordsCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownX)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonClearBuffer);
            this.groupBox1.Controls.Add(this.numericUpDownBlockSize);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.buttonBuild);
            this.groupBox1.Controls.Add(this.numericUpDownRecordsInBlock);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.numericUpDownRecordsCount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(186, 154);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Báze dat";
            // 
            // buttonClearBuffer
            // 
            this.buttonClearBuffer.Enabled = false;
            this.buttonClearBuffer.Location = new System.Drawing.Point(10, 122);
            this.buttonClearBuffer.Name = "buttonClearBuffer";
            this.buttonClearBuffer.Size = new System.Drawing.Size(170, 23);
            this.buttonClearBuffer.TabIndex = 11;
            this.buttonClearBuffer.Text = "Vymazat buffer";
            this.buttonClearBuffer.UseVisualStyleBackColor = true;
            this.buttonClearBuffer.Click += new System.EventHandler(this.buttonClearBuffer_Click);
            // 
            // numericUpDownBlockSize
            // 
            this.numericUpDownBlockSize.Location = new System.Drawing.Point(115, 70);
            this.numericUpDownBlockSize.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownBlockSize.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownBlockSize.Name = "numericUpDownBlockSize";
            this.numericUpDownBlockSize.Size = new System.Drawing.Size(65, 20);
            this.numericUpDownBlockSize.TabIndex = 6;
            this.numericUpDownBlockSize.Value = new decimal(new int[] {
            10500,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Velikost bloku:";
            // 
            // buttonBuild
            // 
            this.buttonBuild.Location = new System.Drawing.Point(10, 96);
            this.buttonBuild.Name = "buttonBuild";
            this.buttonBuild.Size = new System.Drawing.Size(170, 23);
            this.buttonBuild.TabIndex = 4;
            this.buttonBuild.Text = "Vybudovat";
            this.buttonBuild.UseVisualStyleBackColor = true;
            this.buttonBuild.Click += new System.EventHandler(this.buttonBuild_Click);
            // 
            // numericUpDownRecordsInBlock
            // 
            this.numericUpDownRecordsInBlock.Location = new System.Drawing.Point(115, 44);
            this.numericUpDownRecordsInBlock.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownRecordsInBlock.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownRecordsInBlock.Name = "numericUpDownRecordsInBlock";
            this.numericUpDownRecordsInBlock.Size = new System.Drawing.Size(65, 20);
            this.numericUpDownRecordsInBlock.TabIndex = 3;
            this.numericUpDownRecordsInBlock.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Záznamů v bloku:";
            // 
            // numericUpDownRecordsCount
            // 
            this.numericUpDownRecordsCount.Location = new System.Drawing.Point(115, 18);
            this.numericUpDownRecordsCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownRecordsCount.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownRecordsCount.Name = "numericUpDownRecordsCount";
            this.numericUpDownRecordsCount.Size = new System.Drawing.Size(65, 20);
            this.numericUpDownRecordsCount.TabIndex = 1;
            this.numericUpDownRecordsCount.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Počet záznamů:";
            // 
            // numericUpDownY
            // 
            this.numericUpDownY.Location = new System.Drawing.Point(114, 45);
            this.numericUpDownY.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.numericUpDownY.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownY.Name = "numericUpDownY";
            this.numericUpDownY.Size = new System.Drawing.Size(65, 20);
            this.numericUpDownY.TabIndex = 8;
            this.numericUpDownY.Value = new decimal(new int[] {
            1989,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Souřadnice X:";
            // 
            // numericUpDownX
            // 
            this.numericUpDownX.Location = new System.Drawing.Point(114, 19);
            this.numericUpDownX.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.numericUpDownX.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownX.Name = "numericUpDownX";
            this.numericUpDownX.Size = new System.Drawing.Size(65, 20);
            this.numericUpDownX.TabIndex = 6;
            this.numericUpDownX.Value = new decimal(new int[] {
            1989,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Souřadnice Y:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonRemoveRecord);
            this.groupBox2.Controls.Add(this.buttonInterpolationSearch);
            this.groupBox2.Controls.Add(this.buttonBinarySearch);
            this.groupBox2.Location = new System.Drawing.Point(13, 259);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(186, 110);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "C - utříděný blokový soubor";
            // 
            // buttonRemoveRecord
            // 
            this.buttonRemoveRecord.Enabled = false;
            this.buttonRemoveRecord.Location = new System.Drawing.Point(10, 77);
            this.buttonRemoveRecord.Name = "buttonRemoveRecord";
            this.buttonRemoveRecord.Size = new System.Drawing.Size(170, 24);
            this.buttonRemoveRecord.TabIndex = 10;
            this.buttonRemoveRecord.Text = "Odebrání záznamu";
            this.buttonRemoveRecord.UseVisualStyleBackColor = true;
            this.buttonRemoveRecord.Click += new System.EventHandler(this.buttonRemoveRecord_Click);
            // 
            // buttonInterpolationSearch
            // 
            this.buttonInterpolationSearch.Enabled = false;
            this.buttonInterpolationSearch.Location = new System.Drawing.Point(10, 48);
            this.buttonInterpolationSearch.Name = "buttonInterpolationSearch";
            this.buttonInterpolationSearch.Size = new System.Drawing.Size(170, 24);
            this.buttonInterpolationSearch.TabIndex = 9;
            this.buttonInterpolationSearch.Text = "Interpolační vyhledávání";
            this.buttonInterpolationSearch.UseVisualStyleBackColor = true;
            this.buttonInterpolationSearch.Click += new System.EventHandler(this.buttonInterpolationSearch_Click);
            // 
            // buttonBinarySearch
            // 
            this.buttonBinarySearch.Enabled = false;
            this.buttonBinarySearch.Location = new System.Drawing.Point(10, 19);
            this.buttonBinarySearch.Name = "buttonBinarySearch";
            this.buttonBinarySearch.Size = new System.Drawing.Size(170, 24);
            this.buttonBinarySearch.TabIndex = 5;
            this.buttonBinarySearch.Text = "Binární vyhledávání";
            this.buttonBinarySearch.UseVisualStyleBackColor = true;
            this.buttonBinarySearch.Click += new System.EventHandler(this.buttonBinarySearch_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonClearLog);
            this.groupBox3.Controls.Add(this.textBoxLog);
            this.groupBox3.Location = new System.Drawing.Point(205, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(330, 497);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Co se děje:";
            // 
            // buttonClearLog
            // 
            this.buttonClearLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonClearLog.Location = new System.Drawing.Point(3, 471);
            this.buttonClearLog.Name = "buttonClearLog";
            this.buttonClearLog.Size = new System.Drawing.Size(324, 23);
            this.buttonClearLog.TabIndex = 1;
            this.buttonClearLog.Text = "Vymazat log";
            this.buttonClearLog.UseVisualStyleBackColor = true;
            this.buttonClearLog.Click += new System.EventHandler(this.buttonClearLog_Click);
            // 
            // textBoxLog
            // 
            this.textBoxLog.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxLog.Location = new System.Drawing.Point(3, 16);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(324, 449);
            this.textBoxLog.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonIndexReload);
            this.groupBox4.Controls.Add(this.buttonIndexSearch);
            this.groupBox4.Controls.Add(this.buttonIndexInsert);
            this.groupBox4.Controls.Add(this.buttonIndexRemove);
            this.groupBox4.Location = new System.Drawing.Point(12, 375);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(186, 135);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "D - RBT index";
            // 
            // buttonIndexSearch
            // 
            this.buttonIndexSearch.Enabled = false;
            this.buttonIndexSearch.Location = new System.Drawing.Point(10, 48);
            this.buttonIndexSearch.Name = "buttonIndexSearch";
            this.buttonIndexSearch.Size = new System.Drawing.Size(170, 23);
            this.buttonIndexSearch.TabIndex = 12;
            this.buttonIndexSearch.Text = "Hledání přes RBT index";
            this.buttonIndexSearch.UseVisualStyleBackColor = true;
            this.buttonIndexSearch.Click += new System.EventHandler(this.buttonIndexSearch_Click);
            // 
            // buttonIndexInsert
            // 
            this.buttonIndexInsert.Enabled = false;
            this.buttonIndexInsert.Location = new System.Drawing.Point(10, 19);
            this.buttonIndexInsert.Name = "buttonIndexInsert";
            this.buttonIndexInsert.Size = new System.Drawing.Size(170, 23);
            this.buttonIndexInsert.TabIndex = 11;
            this.buttonIndexInsert.Text = "Vložit nový záznam";
            this.buttonIndexInsert.UseVisualStyleBackColor = true;
            this.buttonIndexInsert.Click += new System.EventHandler(this.buttonIndexInsert_Click);
            // 
            // buttonIndexRemove
            // 
            this.buttonIndexRemove.Enabled = false;
            this.buttonIndexRemove.Location = new System.Drawing.Point(10, 77);
            this.buttonIndexRemove.Name = "buttonIndexRemove";
            this.buttonIndexRemove.Size = new System.Drawing.Size(170, 23);
            this.buttonIndexRemove.TabIndex = 10;
            this.buttonIndexRemove.Text = "Odebrání záznamu";
            this.buttonIndexRemove.UseVisualStyleBackColor = true;
            this.buttonIndexRemove.Click += new System.EventHandler(this.buttonIndexRemove_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.numericUpDownX);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.numericUpDownY);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Location = new System.Drawing.Point(13, 175);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(185, 72);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Zadání souřadnic";
            // 
            // buttonIndexReload
            // 
            this.buttonIndexReload.Enabled = false;
            this.buttonIndexReload.Location = new System.Drawing.Point(10, 105);
            this.buttonIndexReload.Name = "buttonIndexReload";
            this.buttonIndexReload.Size = new System.Drawing.Size(170, 23);
            this.buttonIndexReload.TabIndex = 13;
            this.buttonIndexReload.Text = "Znovu načíst index ze souboru";
            this.buttonIndexReload.UseVisualStyleBackColor = true;
            this.buttonIndexReload.Click += new System.EventHandler(this.buttonIndexReload_Click);
            // 
            // FileSortedTableView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 515);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FileSortedTableView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Blokově orientovaný utříděný soubor s přímým přístupem";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.FileSortedTableView_HelpButtonClicked);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlockSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecordsInBlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecordsCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownX)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDownRecordsInBlock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownRecordsCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonBuild;
        private System.Windows.Forms.NumericUpDown numericUpDownY;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDownX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonRemoveRecord;
        private System.Windows.Forms.Button buttonInterpolationSearch;
        private System.Windows.Forms.Button buttonBinarySearch;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Button buttonClearBuffer;
        private System.Windows.Forms.Button buttonClearLog;
        private System.Windows.Forms.NumericUpDown numericUpDownBlockSize;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonIndexSearch;
        private System.Windows.Forms.Button buttonIndexInsert;
        private System.Windows.Forms.Button buttonIndexRemove;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button buttonIndexReload;
    }
}