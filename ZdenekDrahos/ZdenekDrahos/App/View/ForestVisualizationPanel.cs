﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph;
using ZdenekDrahos.Inpg2.NewRectangle;

namespace ZdenekDrahos.App.View
{
    public delegate void RectangleDrawnEventHandler(object sender, AppEventArgs<Rectangle> e);

    public partial class ForestVisualizationPanel : UserControl
    {

        private IEnumerable<ICrossRoad> crossRoadIterator;
        private IEnumerable<Edge<string, ICrossRoad, double>> edgesIterator;
        private IList<ICrossRoad> disabledCrossRoads;
        private IList<Point> path;
        private Rectangle disabledCrossRoadsArea;
        private Brush disabledCrossRoadsBrush = new SolidBrush(Color.FromArgb(20, 255, 0, 0));

        public ForestVisualizationPanel(IEnumerable<ICrossRoad> crossRoadIterator, IEnumerable<Edge<string, ICrossRoad, double>> edgesIterator)
        {
            path = new List<Point>();
            InitializeComponent();
            SetIterators(crossRoadIterator, edgesIterator);
        }

        public event RectangleDrawnEventHandler RectangleDrawn;

        public void SetIterators(IEnumerable<ICrossRoad> crossRoadIterator, IEnumerable<Edge<string, ICrossRoad, double>> edgesIterator)
        {
            this.crossRoadIterator = crossRoadIterator;
            this.edgesIterator = edgesIterator;
        }

        public void SetDisabledNodes(IList<ICrossRoad> disabledCrossRoads)
        {
            this.disabledCrossRoads = disabledCrossRoads;
        }

        public void SetPath(IEnumerable<ICrossRoad> crossroadsInPath)
        {
            path.Clear();
            foreach (ICrossRoad item in crossroadsInPath)
                path.Add(item.Point);
        }

        private void panelForest_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            if (disabledCrossRoadsArea != null)
                DrawDisabledArea(graphics);
            foreach (ICrossRoad cr in crossRoadIterator)
                DrawCrossRoad(graphics, cr);
            foreach (Edge<string, ICrossRoad, double> edge in edgesIterator)
                DrawEdge(graphics, edge);
            if (path.Count > 1)
                DrawPath(graphics);
        }

        private void DrawCrossRoad(Graphics graphics, ICrossRoad crossRoad) 
        {
            Brush color = GetCrossRoadColor(crossRoad);
            graphics.FillRectangle(color, crossRoad.X - 3, crossRoad.Y - 3, 6, 6);
            graphics.DrawString(crossRoad.ToString(), new Font("Arial", 10), color, crossRoad.X, crossRoad.Y);            
        }

        private Brush GetCrossRoadColor(ICrossRoad crossRoad)
        {
            if (disabledCrossRoads != null && disabledCrossRoads.Contains(crossRoad))
                return Brushes.Red;
            if (crossRoad.IsBusStop)
                return Brushes.Green;
            if (crossRoad.IsRestArea)
                return Brushes.Blue;
            else
                return Brushes.LightGray;
        }

        private void DrawEdge(Graphics graphics, Edge<string, ICrossRoad, double> edge)
        {
            graphics.DrawLine(Pens.Orange, edge.StartNode.Point, edge.EndNode.Point);
            float centerX = edge.StartNode.X + (edge.EndNode.X - edge.StartNode.X) / 2;
            float centerY = edge.StartNode.Y + (edge.EndNode.Y - edge.StartNode.Y) / 2;
            graphics.DrawString(edge.EdgeCost.ToString(), new Font("Arial", 10), Brushes.Black, centerX, centerY);
        }

        private void DrawPath(Graphics graphics)
        {
            Pen pen = new Pen(Color.SkyBlue, 3);
            pen.EndCap = LineCap.ArrowAnchor;
            for (int i = 0; i < path.Count - 1; i++)
                graphics.DrawLine(pen, path[i], path[i + 1]);
        }

        private void DrawDisabledArea(Graphics graphics)
        {
            graphics.FillRectangle(disabledCrossRoadsBrush, disabledCrossRoadsArea);
            graphics.DrawRectangle(Pens.Red, disabledCrossRoadsArea);
        }

        # region INTERVAL_SEARCH

        public bool IntervalSearchActive { get; set; }

        private AddRectangle rectangle = new AddRectangle(0);

        private void panelForest_MouseDown(object sender, MouseEventArgs e)
        {
            if (IntervalSearchActive)
                rectangle.Down(e.Location);
        }

        private void panelForest_MouseMove(object sender, MouseEventArgs e)
        {
            if (IntervalSearchActive)
                rectangle.Move(e.Location, CreateGraphics());
        }

        private void panelForest_MouseUp(object sender, MouseEventArgs e)
        {
            if (IntervalSearchActive)
            {
                try
                {
                    disabledCrossRoadsArea = rectangle.Up(e.Location, CreateGraphics());
                    OnRectangleDrawn(disabledCrossRoadsArea);
                }
                catch (Exception) { }
            }
        }

        protected virtual void OnRectangleDrawn(Rectangle rect)
        {
            RectangleDrawnEventHandler handler = this.RectangleDrawn;
            if (handler != null)
                handler(this, new AppEventArgs<Rectangle>(rect));
        }  

        # endregion

    }
}
