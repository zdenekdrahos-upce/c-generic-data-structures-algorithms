﻿namespace ZdenekDrahos.App.View
{
    partial class ShortestPathMatrixDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewDistance = new System.Windows.Forms.DataGridView();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridViewPredecessor = new System.Windows.Forms.DataGridView();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonReloadSave = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDistance)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPredecessor)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewDistance
            // 
            this.dataGridViewDistance.AllowUserToAddRows = false;
            this.dataGridViewDistance.AllowUserToDeleteRows = false;
            this.dataGridViewDistance.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridViewDistance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDistance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDistance.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDistance.Name = "dataGridViewDistance";
            this.dataGridViewDistance.ReadOnly = true;
            this.dataGridViewDistance.RowHeadersWidth = 50;
            this.dataGridViewDistance.Size = new System.Drawing.Size(415, 206);
            this.dataGridViewDistance.TabIndex = 9;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Location = new System.Drawing.Point(163, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(429, 238);
            this.tabControl.TabIndex = 10;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDistance);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(421, 212);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Distanční matice";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridViewPredecessor);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(421, 212);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Matice následníků";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPredecessor
            // 
            this.dataGridViewPredecessor.AllowUserToAddRows = false;
            this.dataGridViewPredecessor.AllowUserToDeleteRows = false;
            this.dataGridViewPredecessor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridViewPredecessor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPredecessor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPredecessor.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewPredecessor.Name = "dataGridViewPredecessor";
            this.dataGridViewPredecessor.ReadOnly = true;
            this.dataGridViewPredecessor.RowHeadersWidth = 50;
            this.dataGridViewPredecessor.Size = new System.Drawing.Size(387, 206);
            this.dataGridViewPredecessor.TabIndex = 10;
            // 
            // panelSearch
            // 
            this.panelSearch.Location = new System.Drawing.Point(4, 13);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(153, 237);
            this.panelSearch.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(164, 262);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Matice:";
            // 
            // buttonReloadSave
            // 
            this.buttonReloadSave.Location = new System.Drawing.Point(212, 257);
            this.buttonReloadSave.Name = "buttonReloadSave";
            this.buttonReloadSave.Size = new System.Drawing.Size(178, 23);
            this.buttonReloadSave.TabIndex = 13;
            this.buttonReloadSave.Text = "Znovu vypočítat a uložit matice";
            this.buttonReloadSave.UseVisualStyleBackColor = true;
            this.buttonReloadSave.Click += new System.EventHandler(this.buttonReloadSave_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(397, 257);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(191, 23);
            this.buttonLoad.TabIndex = 14;
            this.buttonLoad.Text = "Načíst uloženou matici";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // ShortestPathMatrixDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 289);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.buttonReloadSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelSearch);
            this.Controls.Add(this.tabControl);
            this.Name = "ShortestPathMatrixDialog";
            this.Text = "Floydův algoritmus hledání nejkratší cesty";
            this.Load += new System.EventHandler(this.ShortestPathMatrixDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDistance)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPredecessor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDistance;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridViewPredecessor;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonReloadSave;
        private System.Windows.Forms.Button buttonLoad;
    }
}