﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using ZdenekDrahos.App.Controller;
using ZdenekDrahos.App.Model;

namespace ZdenekDrahos.App.View
{
    public partial class BasicSearchInRangeTreeForm : Form
    {
        private IForestController controller;

        public BasicSearchInRangeTreeForm(IForestController controller)
        {
            this.controller = controller;
            InitializeComponent();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            try
            {
                ICrossRoad crossRoad = controller.BasicSearchInRangeTree(GetPoint());
                textBox1.Text = string.Format(@"
Nalezena křižovatka:
ID: {0}
Jde o zastávku: {1}
Jde o odpočívadlo: {2}
", crossRoad.ID, GetTrueFalseAnswer(crossRoad.IsBusStop), GetTrueFalseAnswer(crossRoad.IsRestArea));
            }
            catch (Exception)
            {
                textBox1.Text = "Žádná křižovatka se na daných souřadnicích nenachází";
            }
        }

        private string GetTrueFalseAnswer(bool question)
        {
            return question ? "ano" : "ne";
        }

        private Point GetPoint()
        {
            return new Point(
                (int)numericUpDownX.Value,
                (int)numericUpDownY.Value
            );
        }

    }
}
