﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ZdenekDrahos.App.Model;
using ZdenekDrahos.Graph.ShortestPath;

namespace ZdenekDrahos.App.View
{
    public delegate Path<ICrossRoad> FoundPathCallback(string startNode, string endNode);

    public partial class ShortestPathPanel : UserControl
    {
        public FoundPathCallback FoundPath;

        public ShortestPathPanel()
        {
            InitializeComponent();
        }

        public void LoadRestAreas(IEnumerable<ICrossRoad> restAreaKeysIterator)
        {
            foreach (ICrossRoad key in restAreaKeysIterator)
                comboBoxStart.Items.Add(key.ID);
        }

        public void LoadBusStops(IEnumerable<ICrossRoad> busStopKeysIterator)
        {
            foreach (ICrossRoad key in busStopKeysIterator)
                comboBoxEnd.Items.Add(key.ID);
        }

        private void comboBoxStart_SelectedIndexChanged(object sender, EventArgs e)
        {
            TryCalculatePath();
        }

        private void comboBoxEnd_SelectedIndexChanged(object sender, EventArgs e)
        {
            TryCalculatePath();
        }

        private void TryCalculatePath()
        {
            if (comboBoxStart.SelectedIndex >= 0 && comboBoxEnd.SelectedIndex >= 0)
            {
                string start = comboBoxStart.SelectedItem.ToString();
                string end = comboBoxEnd.SelectedItem.ToString();
                textBoxPath.Clear();
                Path<ICrossRoad> path = FoundPath(start, end);
                if (path.Nodes == null)
                    textBoxPath.AppendText("Cesta nenalezena");
                else if (path.Nodes.Count == 0)
                    textBoxPath.AppendText("Prázdná cesta - cesta z jednoho do stejného vrcholu");
                else
                {
                    foreach (ICrossRoad item in path)
                        textBoxPath.AppendText(string.Format("{0}\r\n", item));
                    textBoxPath.AppendText(string.Format("\r\nCelkový čas: {0}\r\n", path.Cost));
                }
            }
        }
    }
}
