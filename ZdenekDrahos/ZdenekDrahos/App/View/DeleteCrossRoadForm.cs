﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;
using System.Windows.Forms;
using ZdenekDrahos.App.Model;

namespace ZdenekDrahos.App.View
{
    public delegate void DeleteCrossRoadEventHandler(object sender, AppEventArgs<string> e);

    public partial class DeleteCrossRoadForm : Form
    {
        public event DeleteCrossRoadEventHandler DeleteCrossRoad;
        public string ID { get; private set; }

        public DeleteCrossRoadForm(IEnumerable<ICrossRoad> crossRoadIterator)
        {
            InitializeComponent();
            LoadCombobox(crossRoadIterator);
        }

        private void LoadCombobox(IEnumerable<ICrossRoad> crossRoadIterator)
        {
            foreach (ICrossRoad key in crossRoadIterator)
                comboBox1.Items.Add(key.ID);
        }

        private void DeleteCrossRoadForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!string.IsNullOrEmpty(comboBox1.SelectedItem.ToString()) && DialogResult == DialogResult.OK)
                OnDeleteCrossRoad(new AppEventArgs<string>(comboBox1.SelectedItem.ToString()));
        }

        protected virtual void OnDeleteCrossRoad(AppEventArgs<string> e)
        {
            DeleteCrossRoadEventHandler handler = this.DeleteCrossRoad;
            if (handler != null)
                handler(this, e);
        }  
    }
}
