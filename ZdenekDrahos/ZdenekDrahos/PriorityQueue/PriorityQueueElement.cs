﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.PriorityQueue
{
    class PriorityQueueElement<TPriority, TElement> : IComparable<PriorityQueueElement<TPriority, TElement>>
    {

        public TPriority Key { get; set; }
        public TElement Value { get; private set; }

        public PriorityQueueElement(TElement value)
        {
            if (!(value is TPriority))
                throw new ArgumentException();
            Key = (TPriority) ((object) value);
            Value = value;
        }

        public PriorityQueueElement(TPriority key, TElement value)
        {
            Key = key;
            Value = value;
        }


        #region IComparable<PriorityQueueElement<TPriority,TElement>> Members

        public int CompareTo(PriorityQueueElement<TPriority, TElement> other)
        {
            IComparable<TPriority> b = other.Key as IComparable<TPriority>;
            return b.CompareTo(Key);
        }

        #endregion
    }
}
