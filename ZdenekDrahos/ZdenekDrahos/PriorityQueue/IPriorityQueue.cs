﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.PriorityQueue
{
    public interface IPriorityQueue<TPriority, TElement> where TPriority : IComparable<TPriority>
    {
        PriorityProviderCallback<TPriority, TElement> PriorityProvider { get; set; }

        void Clear();

        bool IsEmpty();

        int Count { get; }

        TElement Dequeue();

        TElement Max();

        void Enqueue(TElement element);

        void ChangePriority(TElement element);

        void ChangePriority(TPriority newPriority, TElement element);
    }
}
