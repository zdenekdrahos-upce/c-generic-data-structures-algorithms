﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.PriorityQueue.PairingHeap
{
    public class PairingHeap<TPriority, TElement> : IPriorityQueue<TPriority, TElement> where TPriority : IComparable<TPriority>
    {
        private Node<TPriority, TElement> root;
        private AuxiliaryNodesTable<TPriority, TElement> nodes;

        public PairingHeap()
        {
            root = null;
            nodes = new AuxiliaryNodesTable<TPriority, TElement>();
        }

        #region IPriorityQueue<TPriority,TElement> Members

        public PriorityProviderCallback<TPriority, TElement> PriorityProvider { get; set; }

        public void Clear()
        {
            root = null;
            nodes.Clear();
        }

        public bool IsEmpty()
        {
            return Count == 0;
        }

        public int Count
        {
            get { return nodes.NodesCount; }
            private set { }
        }

        public TElement Dequeue()
        {
            if (IsEmpty())
                throw new InvalidOperationException();
            Node<TPriority, TElement> removedNode = root;
            if (root.HasLeftChild())
                root = CombineSiblings(root.LeftChild);
            else
                root = null;
            TElement removedElement = removedNode.QueueElement.Value;
            nodes.Remove(removedElement);
            return removedElement;
        }

        public TElement Max()
        {
            if (IsEmpty())
                throw new InvalidOperationException();
            return root.QueueElement.Value;
        }

        public void Enqueue(TElement element)
        {
            Node<TPriority, TElement> newNode = new Node<TPriority, TElement>(GetNewElement(element));
            if (root == null)
                root = newNode;
            else
                MeldRootWithNode(newNode);
            nodes.Add(newNode);
        }

        public void ChangePriority(TElement element)
        {
            TPriority newPriority = PriorityProvider(element);
            ChangePriority(newPriority, element);
        }

        public void ChangePriority(TPriority newPriority, TElement element)
        {
            Node<TPriority, TElement> updatedNode = nodes.GetNode(element);
            updatedNode.ChangePriority(newPriority);
            if (updatedNode != root)
            {
                updatedNode.CreateSubtreeFromNode();
                MeldRootWithNode(updatedNode);
            }
        }

        #endregion

        private PriorityQueueElement<TPriority, TElement> GetNewElement(TElement element)
        {
            if (PriorityProvider == null)
                return new PriorityQueueElement<TPriority, TElement>(element);
            else
                return new PriorityQueueElement<TPriority, TElement>(PriorityProvider(element), element);
        }

        private void MeldRootWithNode(Node<TPriority, TElement> node)
        {
            root = CompareAndLink(root, node);
        }

        private Node<TPriority, TElement> CompareAndLink(Node<TPriority, TElement> first, Node<TPriority, TElement> second)
        {
            if (second == null)
                return first;            
            else if (second.HasLowerPriority(first.QueueElement))
                return AttachFirstAsLeftmostChildOfSecond(first, second);
            else
                return AttachSecondAsLeftmostChildOfFirst(first, second);
        }

        private Node<TPriority, TElement> AttachFirstAsLeftmostChildOfSecond(Node<TPriority, TElement> first, Node<TPriority, TElement> second)
        {
            second.Previous = first.Previous;
            first.Previous = second;
            first.NextSibling = second.LeftChild;
            if (first.NextSibling != null)
                first.NextSibling.Previous = first;
            second.LeftChild = first;
            return second;
        }

        private Node<TPriority, TElement> AttachSecondAsLeftmostChildOfFirst(Node<TPriority, TElement> first, Node<TPriority, TElement> second)
        {
            second.Previous = first;
            first.NextSibling = second.NextSibling;
            if (first.NextSibling != null)
                first.NextSibling.Previous = first;
            second.NextSibling = first.LeftChild;
            if (second.NextSibling != null)
                second.NextSibling.Previous = second;
            first.LeftChild = second;
            return first;
        }

        private Node<TPriority, TElement> CombineSiblings(Node<TPriority, TElement> firstSibling)
        {
            if (firstSibling.NextSibling == null)
                return firstSibling;
            IList<Node<TPriority, TElement>> tempTrees = new List<Node<TPriority, TElement>>();
            // Store the subtrees
            while (firstSibling != null) {
                tempTrees.Add(firstSibling);
                firstSibling.Previous.NextSibling = null;  // break links
                firstSibling = firstSibling.NextSibling;
            }
            // Combine subtrees two at a time, going left to right
            int i = 0;
            for (; i + 1 < tempTrees.Count; i += 2)
                tempTrees[i] = CompareAndLink(tempTrees[i], tempTrees[i + 1]);
            int j = i - 2;
            // j has the result of last compareAndLink.
            // If an odd number of trees, get the last one.
            if (j == tempTrees.Count - 3)
                tempTrees[j] = CompareAndLink(tempTrees[j], tempTrees[j + 2]);
            // Now go right to left, merging last tree with
            // next to last. The result becomes the new last.
            for (; j >= 2; j -= 2)
                tempTrees[j - 2] = CompareAndLink(tempTrees[j - 2], tempTrees[j]);
            return tempTrees[0];
        }

    }
}
