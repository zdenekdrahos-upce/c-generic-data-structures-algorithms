﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Table;

namespace ZdenekDrahos.PriorityQueue.PairingHeap
{
    class AuxiliaryNodesTable<TPriority, TElement>
    {

        private ITable<TElement, Node<TPriority, TElement>> nodesTable;

        public AuxiliaryNodesTable()
        {
            nodesTable = new HashTable<TElement, Node<TPriority, TElement>>();
        }

        public int NodesCount
        {
            get { return nodesTable.Count; }
            private set {}
        }
        

        public void Clear()
        {
            nodesTable.Clear();
        }

        public Node<TPriority, TElement> GetNode(TElement element)
        {
            if (nodesTable.Keys.Contains(element))
                return nodesTable.Find(element);
            else
                throw new ArgumentException();
        }

        public void Remove(TElement element)
        {
            nodesTable.Remove(element);
        }

        public void Add(Node<TPriority, TElement> newNode)
        {
            TElement element = newNode.QueueElement.Value;
            if (!nodesTable.Keys.Contains(element))
                nodesTable.Add(element, newNode);
            else
                throw new ArgumentException("duplicite");
        }

    }
}
