﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.PriorityQueue.PairingHeap
{
    class Node<TPriority, TElement>
    {

        public PriorityQueueElement<TPriority, TElement> QueueElement;
        public Node<TPriority, TElement> LeftChild;
        public Node<TPriority, TElement> NextSibling;
        public Node<TPriority, TElement> Previous;

        private IComparable<PriorityQueueElement<TPriority, TElement>> comparable;

        public Node(PriorityQueueElement<TPriority, TElement> element)
        {
            QueueElement = element;
            comparable = element as IComparable<PriorityQueueElement<TPriority, TElement>>;
        }

        public bool HasLowerPriority(PriorityQueueElement<TPriority, TElement> a)
        {
            return comparable.CompareTo(a) > 0;
        }

        public bool HasLeftChild()
        {
            return LeftChild != null;
        }

        public void CreateSubtreeFromNode()
        {
            if (NextSibling != null)
                NextSibling.Previous = Previous;
            if (Previous.LeftChild == this) // it's the oldest son
                Previous.LeftChild = NextSibling;
            else
                Previous.NextSibling = NextSibling;
            NextSibling = null;
        }

        public void ChangePriority(TPriority newPriority)
        {
            QueueElement.Key = newPriority;
        }

    }
}
