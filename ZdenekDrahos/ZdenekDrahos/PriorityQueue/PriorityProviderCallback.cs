﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.PriorityQueue
{
    // if interface then error with generics
    public delegate TPriority PriorityProviderCallback<TPriority, TElement>(TElement element);
}
