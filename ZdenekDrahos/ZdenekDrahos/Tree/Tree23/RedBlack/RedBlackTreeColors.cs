﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Tree.BinarySearchTree;
using ZdenekDrahos.Tree.BinaryTree;

namespace ZdenekDrahos.Tree.Tree23.RedBlack
{
    class RedBlackTreeColors<TKey, TValue> where TKey : IComparable<TKey>
    {

        public void SetRed(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            node.Value.Value.IsRed = true;
        }

        public void SetBlack(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            node.Value.Value.IsBlack = true;
        }

        public void CopyRed(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node,
                                     IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> copy)
        {
            node.Value.Value.IsRed = IsRed(copy);
        }

        public bool IsRed(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            return !IsBlack(node);
        }

        public bool IsBlack(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            return node == null ? true : node.Value.Value.IsBlack;
        }

    }
}
