﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Tree.BinarySearchTree;
using ZdenekDrahos.Tree.BinaryTree;

namespace ZdenekDrahos.Tree.Tree23.RedBlack
{
    class RedBlackTreeVerifier<TKey, TValue> : ITree23Verifier<TKey, TValue> where TKey : IComparable<TKey>
    {

        private RedBlackTreeColors<TKey, TValue> colors = new RedBlackTreeColors<TKey, TValue>();

        #region ITree23Verifier<TKey,TValue> Members

        public void VerifyTree(ITree23<TKey, TValue> tree)
        {
            RedBlackTree<TKey, TValue> redBlackTree = tree as RedBlackTree<TKey, TValue>;
            if (redBlackTree != null)
                VerifyRedBlackTreeProperties(redBlackTree.BinarySearchTree.BinaryTree.Root);
            else
                throw new ArgumentException("Only RedBlackTree can be verified");
        }

        #endregion

        private void VerifyRedBlackTreeProperties(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> root)
        {
            VerifyNodeColors(root);
            VerifyBlackRoot(root);
            VerifyBlackLeafs(root);
            VerifyParentAndChildsOfRedNodes(root);
            VerifyNumberOfBlackNodesInPath(root);
        }

        private void VerifyNodeColors(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (node == null)
                return;
            if (!colors.IsRed(node) && !colors.IsBlack(node))
                throw new ArgumentException("Each node must be red or black");
            VerifyNodeColors(node.Left);
            VerifyNodeColors(node.Right);
        }

        private void VerifyBlackRoot(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> root)
        {
            if (colors.IsRed(root))
                throw new ArgumentException("Root node must be black");
        }

        private void VerifyBlackLeafs(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> root)
        {
            // Property 3 is implicit because childs are set to null (if node is null then color is black)
        }

        private void VerifyParentAndChildsOfRedNodes(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {

            if (colors.IsRed(node))
            {
                if (node.HasLeftChild() && colors.IsRed(node.Left))
                    throw new ArgumentException("Left child of red node must be black node");
                if (node.HasRightChild() && colors.IsRed(node.Right))
                    throw new ArgumentException("Right child of red node must be black node");
                if (node.HasParent() && colors.IsRed(node.Parent))
                    throw new ArgumentException("Parent of red node must be black node");
            }
            if (node != null)
            {
                VerifyParentAndChildsOfRedNodes(node.Left);
                VerifyParentAndChildsOfRedNodes(node.Right);
            }
        }

        private void VerifyNumberOfBlackNodesInPath(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> root)
        {
            VerifyProperty5Helper(root, 0, -1);
        }

        private int VerifyProperty5Helper(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node, int blackCount, int pathBlackCount)
        {
            if (node == null || colors.IsBlack(node))
            {
                blackCount++;
            }
            if (node == null)
            {
                if (pathBlackCount == -1)
                {
                    pathBlackCount = blackCount;
                }
                else if (blackCount != pathBlackCount)
                {
                    throw new ArgumentException("All paths from any node to its leaf nodes must contain the same number of black nodes");
                }
                return pathBlackCount;
            }
            pathBlackCount = VerifyProperty5Helper(node.Left, blackCount, pathBlackCount);
            pathBlackCount = VerifyProperty5Helper(node.Right, blackCount, pathBlackCount);
            return pathBlackCount;
        }
    }
}
