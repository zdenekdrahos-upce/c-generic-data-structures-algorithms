﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Tree.BinarySearchTree;

namespace ZdenekDrahos.Tree.Tree23.RedBlack
{
    public interface IDevRedBlackTree<TKey, TValue> : ITree23<TKey, TValue> where TKey : IComparable<TKey>
    {

        IDevBinarySearchTree<TKey, RedBlackTreeValue<TValue>> BinarySearchTree { get; }

    }
}
