﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Tree.Tree23.RedBlack
{
    [Serializable]
    public class RedBlackTreeValue<TValue>
    {

        private bool isRed;
        private TValue value;

        public static RedBlackTreeValue<TValue> CreateRed(TValue value)
        {
            return new RedBlackTreeValue<TValue>(value, true);
        }

        public static RedBlackTreeValue<TValue> CreateBlack(TValue value)
        {
            return new RedBlackTreeValue<TValue>(value, false);
        }

        public RedBlackTreeValue(TValue value, bool isRed)
        {
            Value = value;
            IsRed = isRed;
        }

        public TValue Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public bool IsRed {
            get { return isRed; }
            set { isRed = value; }
        }

        public bool IsBlack
        {
            get { return !IsRed; }
            set { IsRed = !value; }
        }

    }
}
