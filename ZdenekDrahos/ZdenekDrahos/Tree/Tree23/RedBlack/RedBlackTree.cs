﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Tree.BinarySearchTree;
using ZdenekDrahos.Tree.BinaryTree;
using ZdenekDrahos.Tree.BinaryTree.Rotation;

namespace ZdenekDrahos.Tree.Tree23.RedBlack
{
    public class RedBlackTree<TKey, TValue> : IDevRedBlackTree<TKey, TValue> where TKey : IComparable<TKey>
    {

        private RedBlackTreeColors<TKey, TValue> colors;
        private IRotations<BstNode<TKey, RedBlackTreeValue<TValue>>> rotations;

        public RedBlackTree()
        {
            colors = new RedBlackTreeColors<TKey, TValue>();
            rotations = new Rotations<BstNode<TKey, RedBlackTreeValue<TValue>>>();
            BinarySearchTree = new BinarySearchTree<TKey, RedBlackTreeValue<TValue>>();
        }

        #region IDevRedBlackTree<TKey,TValue> Members

        public IDevBinarySearchTree<TKey, RedBlackTreeValue<TValue>> BinarySearchTree { get; private set; }

        # endregion

        #region ITree23<TKey,TValue> Members

        public void Clear()
        {
            BinarySearchTree.Clear();
        }

        public bool IsEmpty()
        {
            return BinarySearchTree.IsEmpty();
        }

        public int Count
        {
            get { return BinarySearchTree.Count; }
            private set { }
        }

        public void Add(TKey key, TValue value)
        {
            RedBlackTreeValue<TValue> newValue = RedBlackTreeValue<TValue>.CreateRed(value);
            IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> add = BinarySearchTree.AddDev(key, newValue);
            InsertCase1(add);
        }

        public TValue Find(TKey key)
        {
            IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node = BinarySearchTree.FindNodeDev(key);
            return node.Value.Value.Value;
        }

        public TValue Remove(TKey key)
        {
            IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node = BinarySearchTree.FindNodeDev(key);
            TValue removedValue = node.Value.Value.Value;
            if (node.HasLeftChild() && node.HasRightChild())
            {
                // Copy key/value from predecessor and then delete it instead
                IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> predecessor = BinarySearchTree.FindMaxDev(node.Left);
                node.Value.Key = predecessor.Value.Key;
                node.Value.Value.Value = predecessor.Value.Value.Value;
                node = predecessor;
            }

            if (!node.HasLeftChild() || !node.HasRightChild())
            {
                IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> child = !node.HasRightChild() ? node.Left : node.Right;
                if (colors.IsBlack(node))
                {
                    colors.CopyRed(node, child);
                    DeleteCase1(node);
                }
                BinarySearchTree.BinaryTree.ReplaceNode(node, child);

                if (colors.IsRed(BinarySearchTree.BinaryTree.Root))
                    colors.SetBlack(BinarySearchTree.BinaryTree.Root);
            }
            BinarySearchTree.BinaryTree.RemoveNode();
            return removedValue;
        }

        public void VerifyTree(ITree23Verifier<TKey, TValue> verifier)
        {
            verifier.VerifyTree(this);
        }

        #endregion

        # region INSERTION

        private void InsertCase1(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (!node.HasParent())
                colors.SetBlack(node);
            else
                InsertCase2(node);
        }

        private void InsertCase2(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (colors.IsRed(node.Parent))
                InsertCase3(node);
        }

        private void InsertCase3(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (node.HasUncle() && colors.IsRed(node.GetUncle()))
            {
                colors.SetBlack(node.Parent);
                colors.SetBlack(node.GetUncle());
                colors.SetRed(node.GetGrandParent());
                InsertCase1(node.GetGrandParent());
            }
            else
                InsertCase4(node);
        }

        private void InsertCase4(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (node.IsRightChild() && node.Parent.IsLeftChild())
            {
                RotateLeft(node.Parent);
                node = node.Left;
            }
            else if (node.IsLeftChild() && node.Parent.IsRightChild())
            {
                RotateRight(node.Parent);
                node = node.Right;
            }
            InsertCase5(node);
        }

        private void InsertCase5(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            colors.SetBlack(node.Parent);
            colors.SetRed(node.GetGrandParent());
            if (node.IsLeftChild() && node.Parent.IsLeftChild())
                RotateRight(node.GetGrandParent());
            else if (node.IsRightChild() && node.Parent.IsRightChild())
                RotateLeft(node.GetGrandParent());
        }

        # endregion

        # region REMOVAL

        private void DeleteCase1(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (node.HasParent())
                DeleteCase2(node);
        }

        private void DeleteCase2(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (node.HasSibling() && colors.IsRed(node.GetSibling()))
            {
                colors.SetRed(node.Parent);
                colors.SetBlack(node.GetSibling());
                if (node.IsLeftChild())
                    RotateLeft(node.Parent);
                else
                    RotateRight(node.Parent);
            }
            DeleteCase3(node);
        }

        private void DeleteCase3(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (colors.IsBlack(node.Parent) && node.HasSibling() && colors.IsBlack(node.GetSibling()) &&
                colors.IsBlack(node.GetSibling().Left) && colors.IsBlack(node.GetSibling().Right))
            {
                colors.SetRed(node.GetSibling());
                DeleteCase1(node.Parent);
            }
            else
                DeleteCase4(node);
        }

        private void DeleteCase4(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (colors.IsRed(node.Parent) && node.HasSibling() && colors.IsBlack(node.GetSibling()) &&
                colors.IsBlack(node.GetSibling().Left) && colors.IsBlack(node.GetSibling().Right))
            {
                colors.SetRed(node.GetSibling());
                colors.SetBlack(node.Parent);
            }
            else
                DeleteCase5(node);
        }

        private void DeleteCase5(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (node.HasSibling())
            {
                if (node.IsLeftChild() && colors.IsBlack(node.GetSibling()) &&
                    colors.IsRed(node.GetSibling().Left) && colors.IsBlack(node.GetSibling().Right))
                {
                    colors.SetRed(node.GetSibling());
                    colors.SetBlack(node.GetSibling().Left);
                    RotateRight(node.GetSibling());
                }
                else if (node.IsRightChild() && colors.IsBlack(node.GetSibling()) &&
                         colors.IsRed(node.GetSibling().Right) && colors.IsBlack(node.GetSibling().Left))
                {
                    colors.SetRed(node.GetSibling());
                    colors.SetBlack(node.GetSibling().Right);
                    RotateLeft(node.GetSibling());
                }
                DeleteCase6(node);
            }
        }

        private void DeleteCase6(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            if (node.HasSibling())
            {
                colors.CopyRed(node.GetSibling(), node.Parent);
                colors.SetBlack(node.Parent);
                if (node.IsLeftChild() && colors.IsRed(node.GetSibling().Right))
                {
                    colors.SetBlack(node.GetSibling().Right);
                    RotateLeft(node.Parent);
                }
                else if (colors.IsRed(node.GetSibling().Left))
                {
                    colors.SetBlack(node.GetSibling().Left);
                    RotateRight(node.Parent);
                }
            }
        }

        # endregion

        # region ROTATIONS 

        private void RotateLeft(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            rotations.RotateLeft(BinarySearchTree.BinaryTree, node);
        }

        private void RotateRight(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> node)
        {
            rotations.RotateRight(BinarySearchTree.BinaryTree, node);
        }

        # endregion

    }
}