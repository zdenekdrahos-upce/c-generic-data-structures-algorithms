﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.IO;
using ZdenekDrahos.Files.Block;
using ZdenekDrahos.Files.Block.Blocks;
using ZdenekDrahos.Files.Block.Records;
using ZdenekDrahos.Tree.BinarySearchTree;
using ZdenekDrahos.Tree.BinaryTree;

namespace ZdenekDrahos.Tree.Tree23.RedBlack.Export
{
    class RedBlackTreeDataMapper<TKey, TValue> : IRedBlackTreeDataMapper<TKey,TValue> where TKey : IComparable<TKey>
    {

        private int recordIndex = 0;

        public RedBlackTreeDataMapper(BlockFile<TKey, RedBlackTreeValue<TValue>> indexFile)
        {
            IndexFile = indexFile;
        }

        public BlockFile<TKey, RedBlackTreeValue<TValue>> IndexFile { get; private set; }

        public void Save(ITree23<TKey, TValue> tree)
        {
            RedBlackTree<TKey, TValue> redBlackTree = tree as RedBlackTree<TKey, TValue>;
            if (redBlackTree != null)
                SaveTreeToBlockFile(redBlackTree);
            else
                throw new ArgumentException("Only RedBlackTree can be exported");
        }

        public ITree23<TKey, TValue> Load()
        {
            Record<TKey, RedBlackTreeValue<TValue>> record;
            RedBlackTree<TKey, TValue> tree = new RedBlackTree<TKey, TValue>();

            foreach (Block<TKey, RedBlackTreeValue<TValue>> block in IndexFile.ReadAll())
                for (int recordIndex = 0; recordIndex < IndexFile.ControlBlock.RecordsInBlockCount; recordIndex++)
                    if (block.FreeBlocks[recordIndex] == false)
                    {
                        record = block.Records[recordIndex];
                        tree.BinarySearchTree.Add(record.Key, record.Value);
                    }

            return tree;
        }

        private void SaveTreeToBlockFile(RedBlackTree<TKey, TValue> tree)
        {
            IndexFile.RemoveAllBlocks();

            using (FileStream fileStream = IndexFile.BinaryFile.GetWriteStream())
            {
                using (BinaryWriter binaryWriter = IndexFile.BinaryFile.GetBinaryWriter(fileStream))
                {
                    InitNextBlock();
                    SeekToFirstBlock(binaryWriter);

                    foreach (IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> item in tree.BinarySearchTree.BreadthFirstSearchDev())
                    {
                        AddRecordToBuffer(item.Value);
                        if (IsBlockFull())
                            WriteBlock(binaryWriter);
                    }
                    if (!IndexFile.Buffer.IsEmpty())
                        WriteBlock(binaryWriter);
                }
            }

            IndexFile.WriteControlBlock();
        }

        private void SeekToFirstBlock(BinaryWriter binaryWriter)
        {
            binaryWriter.Seek(IndexFile.ControlBlock.ControlBlockSize, SeekOrigin.Begin);
        }

        private void AddRecordToBuffer(BstNode<TKey, RedBlackTreeValue<TValue>> node)
        {
            IndexFile.Buffer.AddRecord(node.Key, node.Value);
            recordIndex++;
        }

        private bool IsBlockFull()
        {
            return recordIndex >= IndexFile.ControlBlock.RecordsInBlockCount;
        }

        private void WriteBlock(BinaryWriter binaryWriter)
        {
            byte[] blockBytes = IndexFile.SerializeBuffer();
            binaryWriter.Write(blockBytes);
            IndexFile.ControlBlock.AllocatedBlocksCount++;
            InitNextBlock();
        }

        private void InitNextBlock()
        {
            recordIndex = 0;
            IndexFile.AppendEmptyBlock();
        }

    }
}
