﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Files.Block;

namespace ZdenekDrahos.Tree.Tree23.RedBlack.Export
{
    interface IRedBlackTreeDataMapper<TKey, TValue> where TKey : IComparable<TKey>
    {

        BlockFile<TKey, RedBlackTreeValue<TValue>> IndexFile { get; }

        ITree23<TKey, TValue> Load();

        void Save(ITree23<TKey, TValue> tree);

    }
}
