﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Tree.BinarySearchTree;
using ZdenekDrahos.Tree.BinaryTree;

namespace ZdenekDrahos.Tree.Tree23.RedBlack.Export
{
    class RedBlackTreePrinter<TKey, TValue> where TKey : IComparable<TKey>
    {

        public void Print(IDevRedBlackTree<TKey, TValue> tree)
        {
            PrintNode(tree.BinarySearchTree.BinaryTree.Root, 0);
        }

        private void PrintNode(IBinaryNode<BstNode<TKey, RedBlackTreeValue<TValue>>> n, int indent)
        {
            if (n == null)
            {
                Console.Write("<empty tree>");
                return;
            }

            if (n.Right != null)
                PrintNode(n.Right, indent + 4);

            for (int i = 0; i < indent; i++)
                Console.Write(" ");

            if (!n.Value.Value.IsRed)
                Console.WriteLine(n.Value.Key);
            else
                Console.WriteLine("<" + n.Value.Key + ">");

            if (n.Left != null)
                PrintNode(n.Left, indent + 4);
        }

    }
}
