﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Tree.Tree23
{
    public interface ITree23<TKey, TValue> where TKey : IComparable<TKey>
    {

        void Clear();

        bool IsEmpty();

        int Count { get; }

        void Add(TKey key, TValue value);

        TValue Find(TKey key);

        TValue Remove(TKey key);

        void VerifyTree(ITree23Verifier<TKey, TValue> verifier);

    }
}
