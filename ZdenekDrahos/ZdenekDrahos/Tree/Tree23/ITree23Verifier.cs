﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Tree.Tree23
{

    public interface ITree23Verifier<TKey, TValue> where TKey : IComparable<TKey>
    {

        void VerifyTree(ITree23<TKey, TValue> tree);

    }
}
