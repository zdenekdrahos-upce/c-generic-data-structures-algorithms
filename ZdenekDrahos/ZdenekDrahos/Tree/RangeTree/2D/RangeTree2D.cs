﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Tree.BinarySearchTree;
using ZdenekDrahos.Tree.RangeTree._1D;
using ZdenekDrahos.Tree.RangeTree._2D.Tree;

namespace ZdenekDrahos.Tree.RangeTree._2D
{
    public class RangeTree2D<TKey, TValue> : IRangeTree2D<TKey, TValue> where TKey : IComparable<TKey>
    {

        private TreeBuilder2D<TKey, TValue> builder;
        private IBinarySearchTree<Keys2D<TKey>, Node2D<TKey, TValue>> binarySearchTree;

        public RangeTree2D(Func<TValue, TKey> getKeyX, Func<TValue, TKey> getKeyY)
        {
            builder = new TreeBuilder2D<TKey, TValue>();
            GetKeyX = getKeyX;
            GetKeyY = getKeyY;
        }

        #region IRangeTree2D<TKey,TValue> Members

        public Func<TValue, TKey> GetKeyX
        {
            get { return builder.GetKeyX; }
            set { builder.GetKeyX = value; }
        }

        public Func<TValue, TKey> GetKeyY
        {
            get { return builder.GetKeyY; }
            set { builder.GetKeyY = value; }
        }

        public void Build(IEnumerable<TValue> elements)
        {
            binarySearchTree = builder.Build(elements);
        }

        public IList<TValue> Find(TKey x, TKey y)
        {
            Node2D<TKey, TValue> node = FindNode(x);
            IList<TValue> result = FilterFindResultByY(node.ListNode.Value, y);
            if (result.Count > 0)
                return result;
            else
                throw new KeyNotFoundException("y");
        }

        public IList<TValue> IntervalSearch(TKey startX, TKey startY, TKey endX, TKey endY)
        {
            if (IsInvalidIntervalSearch(startX, startY, endX, endY))
                throw new InvalidIntervalSearchException();
            Node2D<TKey, TValue> node = FindNode(startX, endX);
            IList<TValue> searchResult;
            if (node.IsLeaf())
                searchResult = node.ListNode.Value;
            else
                searchResult = FindInRangeTreeY(node.RangeTree, startY, endY);
            return FilterIntervalResult(searchResult, startX, startY, endX, endY);
        }

        #endregion

        private Node2D<TKey, TValue> FindNode(TKey x)
        {
            Keys2D<TKey> keys = Keys2D<TKey>.CreateSearchKey(x);
            return binarySearchTree.Find(keys);
        }

        private Node2D<TKey, TValue> FindNode(TKey startX, TKey endX)
        {
            Keys2D<TKey> keys = Keys2D<TKey>.CreateSearchKey(startX, endX);
            return binarySearchTree.Find(keys);
        }

        private IList<TValue> FindInRangeTreeY(IRangeTree1D<TKey, TValue> rangeY, TKey startY, TKey endY)
        {
            if (startY.CompareTo(endY) == 0)
                return rangeY.Find(startY);
            else
                return rangeY.IntervalSearch(startY, endY);
        }

        private IList<TValue> FilterFindResultByY(IList<TValue> searchResult, TKey y)
        {
            IList<TValue> result = new List<TValue>();
            foreach (TValue item in searchResult)
                if (GetKeyY(item).CompareTo(y) == 0)
                    result.Add(item);
            return result;
        }

        private IList<TValue> FilterIntervalResult(IList<TValue> searchResult, TKey startX, TKey startY, TKey endX, TKey endY)
        {
            IList<TValue> result = new List<TValue>();
            foreach (TValue item in searchResult)
                if (GetKeyX(item).CompareTo(startX) != -1 && GetKeyX(item).CompareTo(endX) != 1 &&
                    GetKeyY(item).CompareTo(startY) != -1 && GetKeyY(item).CompareTo(endY) != 1)
                    result.Add(item);
            return result;
        }

        private bool IsInvalidIntervalSearch(TKey startX, TKey startY, TKey endX, TKey endY)
        {
            return (startX.CompareTo(endX) == 0 && startY.CompareTo(endY) == 0) ||
                    (startX.CompareTo(endX) == 0 && startY.CompareTo(endY) > -1) ||
                    (startY.CompareTo(endY) == 0 && startX.CompareTo(endX) > -1) ||
                    startX.CompareTo(endX) == 1 || startY.CompareTo(endY) == 1;
        }

    }
}
