﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.Tree.RangeTree._2D
{
    public interface IRangeTree2D<TKey, TValue> where TKey : IComparable<TKey>
    {

        Func<TValue, TKey> GetKeyX { get; set; }

        Func<TValue, TKey> GetKeyY { get; set; }

        void Build(IEnumerable<TValue> elements);

        IList<TValue> Find(TKey x, TKey y);

        IList<TValue> IntervalSearch(TKey startX, TKey startY, TKey endX, TKey endY);

    }
}
