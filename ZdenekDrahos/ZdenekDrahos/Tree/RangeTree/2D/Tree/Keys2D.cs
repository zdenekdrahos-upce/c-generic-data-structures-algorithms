﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ZdenekDrahos.Tree.RangeTree._1D.Tree;

namespace ZdenekDrahos.Tree.RangeTree._2D.Tree
{
    class Keys2D<TKey> : IComparable<Keys2D<TKey>> where TKey : IComparable<TKey>
    {
        private bool IsIntervalSearch;
        public Key1D<TKey> MedianX { get; private set; }
        private TKey min, max;

        public static Keys2D<TKey> CreateSearchKey(TKey x)
        {
            return new Keys2D<TKey>(Key1D<TKey>.CreateLeaf(x), false);
        }

        public static Keys2D<TKey> CreateSearchKey(TKey startX, TKey endX)
        {
            Key1D<TKey> keys = Key1D<TKey>.CreateNonLeaf(startX);
            Keys2D<TKey> key = new Keys2D<TKey>(null, true);
            key.min = startX;
            key.max = endX;
            return key;
        }

        public static Keys2D<TKey> CreateTreeNode(Key1D<TKey> keys)
        {
            return new Keys2D<TKey>(keys, false);
        }

        private Keys2D(Key1D<TKey> keys, bool isIntervalSearch)
        {
            MedianX = keys;
            IsIntervalSearch = isIntervalSearch;
        }

        #region IComparable<Keys2D<TKey,TValue>> Members

        public int CompareTo(Keys2D<TKey> other)
        {
            if (IsIntervalSearch && other.IsIntervalSearch)
                throw new NotSupportedException();
            else if (other.IsIntervalSearch)
                return IntervalCompare(this, other);
            else if (IsIntervalSearch)
                return IntervalCompare(other, this);
            else
                return MedianX.CompareTo(other.MedianX);
        }

        #endregion

        private int IntervalCompare(Keys2D<TKey> node, Keys2D<TKey> search)
        {
            if (search.IsNodeBetweenMinAndMax(node))
                return 0;
            else if (search.IsFromLeftBranch(node))
                return -1;
            else
                return 1;
        }

        private bool IsNodeBetweenMinAndMax(Keys2D<TKey> node)
        {
            return IsMinSmaller(node) && IsMaxEqualOrLarger(node);
        }

        private bool IsMinSmaller(Keys2D<TKey> node)
        {
            if (min.CompareTo(max) == 0) // if min == max then node value can be median -> min is smaller or equal
                return min.CompareTo(node.MedianX.Median) <= 0;
            else
                return min.CompareTo(node.MedianX.Median) == -1;
        }

        private bool IsMaxEqualOrLarger(Keys2D<TKey> node)
        {
            return max.CompareTo(node.MedianX.Median) >= 0;
        }

        private bool IsFromLeftBranch(Keys2D<TKey> node)
        {
            return min.CompareTo(node.MedianX.Median) == -1 && max.CompareTo(node.MedianX.Median) == -1;
        }
    }
}
