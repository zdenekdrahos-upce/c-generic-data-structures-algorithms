﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Table;
using ZdenekDrahos.Tree.BinarySearchTree;
using ZdenekDrahos.Tree.RangeTree._1D;
using ZdenekDrahos.Tree.RangeTree._1D.Tree;

namespace ZdenekDrahos.Tree.RangeTree._2D.Tree
{
    class TreeBuilder2D<TKey, TValue> where TKey : IComparable<TKey>
    {
        private TreeBuilder1D<TKey, TValue> builder1D;
        private IBinarySearchTree<Key1D<TKey>, Node<TValue>> treeX;

        private IBinarySearchTree<Keys2D<TKey>, Node2D<TKey, TValue>> binarySearchTree;

        public TreeBuilder2D()
        {
            builder1D = new TreeBuilder1D<TKey, TValue>();
            treeX = new BinarySearchTree<Key1D<TKey>, Node<TValue>>();
        }

        public Func<TValue, TKey> GetKeyX { get; set; }

        public Func<TValue, TKey> GetKeyY { get; set; }

        public IBinarySearchTree<Keys2D<TKey>, Node2D<TKey, TValue>> Build(IEnumerable<TValue> elements)
        {
            InitBuild();
            BuildRangeTreeX(elements);
            Build2DTree();
            return binarySearchTree;
        }

        private void InitBuild()
        {
            binarySearchTree = new BinarySearchTree<Keys2D<TKey>, Node2D<TKey, TValue>>();
            treeX.Clear();
        }

        private void BuildRangeTreeX(IEnumerable<TValue> elements)
        {
            builder1D.GetKey = GetKeyX;
            treeX = builder1D.Build(elements);
        }

        private void Build2DTree()
        {
            Keys2D<TKey> keys;
            Node2D<TKey, TValue> node;
            foreach (Key1D<TKey> item in treeX.KeysBreadthFirstSearch())
            {
                keys = Keys2D<TKey>.CreateTreeNode(item);
                if (item.IsLeaf)
                    node = Node2D<TKey, TValue>.CreateLeaf(treeX.Find(item).ListNode);
                else
                    node = Node2D<TKey, TValue>.CreateNonLeaf(GetRangeTreeY(item));
                binarySearchTree.Add(keys, node);
            }
        }

        private IRangeTree1D<TKey, TValue> GetRangeTreeY(Key1D<TKey> key)
        {
            List<TValue> list = new List<TValue>();
            foreach (Node<TValue> node in treeX.BreadthFirstSearch(key))
                if (node.ListNode != null) // is leaf
                    list.AddRange(node.ListNode.Value);
            IRangeTree1D<TKey, TValue> rangeTree = new RangeTree1D<TKey, TValue>(GetKeyY);
            rangeTree.Build(list);
            return rangeTree;
        }
    }
}
