﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Tree.RangeTree._1D;

namespace ZdenekDrahos.Tree.RangeTree._2D.Tree
{
    class Node2D<TKey, TValue> where TKey : IComparable<TKey>
    {

        public static Node2D<TKey, TValue> CreateNonLeaf(IRangeTree1D<TKey, TValue> rangeTree)
        {
            return new Node2D<TKey, TValue>(rangeTree, null);
        }

        public static Node2D<TKey, TValue> CreateLeaf(LinkedListNode<IList<TValue>> listNode)
        {
            return new Node2D<TKey, TValue>(null, listNode);
        }

        private Node2D(IRangeTree1D<TKey, TValue> rangeTree, LinkedListNode<IList<TValue>> listNode)
        {
            RangeTree = rangeTree;
            ListNode = listNode;
        }

        public IRangeTree1D<TKey, TValue> RangeTree { get; private set; }
        public LinkedListNode<IList<TValue>> ListNode { get; private set; }

        public bool IsLeaf()
        {
            return ListNode != null;
        }
    }
}
