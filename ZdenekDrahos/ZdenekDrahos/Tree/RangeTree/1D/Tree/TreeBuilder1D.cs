﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ZdenekDrahos.Statistics.Median;
using ZdenekDrahos.Tree.BinarySearchTree;

namespace ZdenekDrahos.Tree.RangeTree._1D.Tree
{
    class TreeBuilder1D<TKey, TValue> where TKey : IComparable<TKey>
    {

        private IList<TKey> keys;
        private IMedianIndex median;
        private LinkedList<IList<TValue>> linkedList;
        private IBinarySearchTree<Key1D<TKey>, Node<TValue>> binarySearchTree;

        public TreeBuilder1D()
        {
            keys = new List<TKey>();
            median = new MedianIndex();
        }

        public Func<TValue, TKey> GetKey { get; set; }

        public IBinarySearchTree<Key1D<TKey>, Node<TValue>> Build(IEnumerable<TValue> elements)
        {
            InitBuild();
            LoadLinkedList(elements);
            BuildTreeStructure();
            AddLeafs();
            return binarySearchTree;
        }

        private void InitBuild()
        {
            keys.Clear();
            linkedList = new LinkedList<IList<TValue>>();
            binarySearchTree = new BinarySearchTree<Key1D<TKey>, Node<TValue>>();
        }

        private void LoadLinkedList(IEnumerable<TValue> elements)
        {
            SortedList<TKey, IList<TValue>> sortedList = GetSortedKeys(elements);
            foreach (KeyValuePair<TKey, IList<TValue>> pair in sortedList)
            {
                keys.Add(pair.Key);
                linkedList.AddLast(pair.Value);
            }
        }

        private SortedList<TKey, IList<TValue>> GetSortedKeys(IEnumerable<TValue> elements)
        {
            TKey key;
            SortedList<TKey, IList<TValue>> list = new SortedList<TKey, IList<TValue>>();
            foreach (TValue item in elements)
            {
                key = GetKey(item);
                if (!list.ContainsKey(key))
                    list.Add(key, new List<TValue>(1));
                list[key].Add(item);
            }
            return list;
        }

        private void BuildTreeStructure()
        {
            int[] currentIndexSchedule = new int[] { linkedList.Count };
            bool continueBuilding = true;
            int startIndex, length;
            while (continueBuilding)
            {
                continueBuilding = false;
                startIndex = 0;
                for (int i = 0; i < currentIndexSchedule.Length; i++)
                {
                    length = currentIndexSchedule[i];
                    if (length <= 1)
                    {
                        startIndex += length;
                        continue;
                    }
                    continueBuilding = true;
                    AddNonLeaf(startIndex, length);
                    startIndex += length;
                }
                if (continueBuilding)
                    currentIndexSchedule = UpdateIndexSchedule(currentIndexSchedule);
            }
        }

        private void AddNonLeaf(int startIndex, int length)
        {
            TKey medianKey = GetMedianKeyForGroup(startIndex, length);
            Key1D<TKey> key = Key1D<TKey>.CreateNonLeaf(medianKey);
            Node<TValue> value = Node<TValue>.CreateNonLeaf();
            binarySearchTree.Add(key, value);
        }

        private TKey GetMedianKeyForGroup(int startIndex, int length)
        {
            int medianIndex = startIndex + median.GetMedianIndex(length);
            return keys[medianIndex];
        }

        private int[] UpdateIndexSchedule(int[] currentSchedule)
        {
            IList<int> newSchedule = new List<int>();
            int indexMiddle;
            for (int i = 0; i < currentSchedule.Length; i++)
            {
                indexMiddle = median.GetMedianIndex(currentSchedule[i]);
                newSchedule.Add(indexMiddle);
                newSchedule.Add(currentSchedule[i] - indexMiddle);
            }
            return newSchedule.ToArray<int>();
        }

        private void AddLeafs()
        {
            LinkedListNode<IList<TValue>> node = linkedList.First;
            while (node != null)
            {
                AddLeaf(node);
                node = node.Next;
            }
        }

        private void AddLeaf(LinkedListNode<IList<TValue>> node)
        {
            Key1D<TKey> key = Key1D<TKey>.CreateLeaf(GetKey(node.Value[0]));
            Node<TValue> value = Node<TValue>.CreateLeaf(node);
            binarySearchTree.Add(key, value);
        }

    }
}
