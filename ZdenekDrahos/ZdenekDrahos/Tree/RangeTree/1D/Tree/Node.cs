﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;

namespace ZdenekDrahos.Tree.RangeTree._1D.Tree
{
    class Node<TValue>
    {

        private bool isLeaf;
        private LinkedListNode<IList<TValue>> listNode;

        public static Node<TValue> CreateNonLeaf() 
        {
            return new Node<TValue>(false, null);
        }

        public static Node<TValue> CreateLeaf(LinkedListNode<IList<TValue>> listNode) 
        {
            return new Node<TValue>(true, listNode);
        }

        private Node(bool isLeaf, LinkedListNode<IList<TValue>> listNode)
        {
            this.isLeaf = isLeaf;
            this.listNode = listNode;
        }

        public LinkedListNode<IList<TValue>> ListNode {
            get
            {
                return isLeaf ? listNode : null;
            }
            private set 
            {
                listNode = value;
            }
        }


    }
}
