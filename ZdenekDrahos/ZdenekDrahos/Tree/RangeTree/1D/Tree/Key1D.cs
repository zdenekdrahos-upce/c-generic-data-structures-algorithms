﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Tree.RangeTree._1D.Tree
{
    class Key1D<TKey> : IComparable<Key1D<TKey>> where TKey : IComparable<TKey>
    {

        public bool IsLeaf { get; private set; }
        public TKey Median { get; private set; }

        public static Key1D<TKey> CreateLeaf(TKey key)
        {
            return new Key1D<TKey>(key, true);
        }

        public static Key1D<TKey> CreateNonLeaf(TKey median)
        {
            return new Key1D<TKey>(median, false);
        }

        private Key1D(TKey median, bool isLeaf)
        {
            Median = median;
            this.IsLeaf = isLeaf;
        }

        #region IComparable<Keys<TFindKey>> Members

        public int CompareTo(Key1D<TKey> other)
        {
            if (IsLeaf == other.IsLeaf)
                return Median.CompareTo(other.Median);
            else
                return Median.CompareTo(other.Median) == -1 ? -1 : 1;
        }

        #endregion
    }
}
