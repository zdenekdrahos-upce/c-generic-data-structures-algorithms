﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Tree.BinarySearchTree;
using ZdenekDrahos.Tree.RangeTree._1D.Tree;

namespace ZdenekDrahos.Tree.RangeTree._1D
{
    public class RangeTree1D<TKey, TValue> : IRangeTree1D<TKey, TValue> where TKey : IComparable<TKey>
    {

        private TreeBuilder1D<TKey, TValue> builder;
        private IBinarySearchTree<Key1D<TKey>, Node<TValue>> binarySearchTree;

        public RangeTree1D(Func<TValue, TKey> getKey)
        {
            builder = new TreeBuilder1D<TKey, TValue>();
            GetKey = getKey;
        }

        #region IRangeTree1D<TElement,TFindKey> Members

        public Func<TValue, TKey> GetKey
        {
            get { return builder.GetKey; }
            set { builder.GetKey = value; }
        }

        public void Build(IEnumerable<TValue> elements)
        {
            binarySearchTree = builder.Build(elements);
        }

        public IList<TValue> Find(TKey key)
        {
            Key1D<TKey> keys = Key1D<TKey>.CreateLeaf(key);
            return binarySearchTree.Find(keys).ListNode.Value;
        }

        public IList<TValue> IntervalSearch(TKey start, TKey end)
        {
            if (start.CompareTo(end) >= 0)
                throw new InvalidIntervalSearchException();
            return FindElementsInInterval(start, end);
        }

        #endregion

        private IList<TValue> FindElementsInInterval(TKey start, TKey end)
        {
            List<TValue> result = new List<TValue>();
            LinkedListNode<IList<TValue>> current = FindClosestLeaf(start);
            while (current != null && GetKey(current.Value[0]).CompareTo(end) <= 0)
            {
                if (start.CompareTo(GetKey(current.Value[0])) <= 0)
                    result.AddRange(current.Value);
                current = current.Next;
            }
            return result;
        }

        private LinkedListNode<IList<TValue>> FindClosestLeaf(TKey key)
        {
            Key1D<TKey> keys = Key1D<TKey>.CreateLeaf(key);
            return binarySearchTree.FindClosestLeaf(keys).ListNode;
        }


    }
}
