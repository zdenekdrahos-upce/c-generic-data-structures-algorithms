﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.Tree.RangeTree._1D
{
    public interface IRangeTree1D<TKey, TValue> where TKey : IComparable<TKey>
    {

        Func<TValue, TKey> GetKey { get; set; }

        void Build(IEnumerable<TValue> elements);

        IList<TValue> Find(TKey key);

        IList<TValue> IntervalSearch(TKey start, TKey end);

    }
}
