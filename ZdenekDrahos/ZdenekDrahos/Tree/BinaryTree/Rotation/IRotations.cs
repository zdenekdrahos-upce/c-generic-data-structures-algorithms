﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using ZdenekDrahos.Tree.BinaryTree;

namespace ZdenekDrahos.Tree.BinaryTree.Rotation
{
    interface IRotations<TValue>
    {
        void RotateLeft(IBinaryTree<TValue> binaryTree, IBinaryNode<TValue> node);

        void RotateRight(IBinaryTree<TValue> binaryTree, IBinaryNode<TValue> node);
    }
}
