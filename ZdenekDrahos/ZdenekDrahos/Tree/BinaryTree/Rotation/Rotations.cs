﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.Tree.BinaryTree.Rotation
{
    class Rotations<TValue> : IRotations<TValue>
    {

        public void RotateLeft(IBinaryTree<TValue> binaryTree, IBinaryNode<TValue> node)
        {
            IBinaryNode<TValue> right = node.Right;
            binaryTree.ReplaceNode(node, right);
            node.Right = right.Left;
            if (right.HasLeftChild())
                right.Left.Parent = node;
            right.Left = node;
            node.Parent = right;
        }

        public void RotateRight(IBinaryTree<TValue> binaryTree, IBinaryNode<TValue> node)
        {
            IBinaryNode<TValue> left = node.Left;
            binaryTree.ReplaceNode(node, left);
            node.Left = left.Right;
            if (left.HasRightChild())
                left.Right.Parent = node;
            left.Right = node;
            node.Parent = left;
        }

    }
}
