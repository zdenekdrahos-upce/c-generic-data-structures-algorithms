﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Collections.Generic;

namespace ZdenekDrahos.Tree.BinaryTree
{
    public interface IBinaryTree<TValue>
    {

        void Clear();

        bool IsEmpty();

        int Count { get; }

        IBinaryNode<TValue> Root { get; }

        IBinaryNode<TValue> InsertRoot(TValue value);

        IBinaryNode<TValue> InsertLeftChild(IBinaryNode<TValue> node, TValue value);

        IBinaryNode<TValue> InsertRightChild(IBinaryNode<TValue> node, TValue value);

        IEnumerable<IBinaryNode<TValue>> BreadthFirstSearch();

        IEnumerable<IBinaryNode<TValue>> BreadthFirstSearch(IBinaryNode<TValue> startNode);

        void ReplaceNode(IBinaryNode<TValue> oldNode, IBinaryNode<TValue> newNode);

        void RemoveNode();

    }
}
