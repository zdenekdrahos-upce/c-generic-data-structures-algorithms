﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;

namespace ZdenekDrahos.Tree.BinaryTree
{
    public class BinaryNode<TValue> : IBinaryNode<TValue>
    {

        public BinaryNode(TValue value)
        {
            Value = value;
        }

        #region IBinaryNode<TValue> Members

        public TValue Value { get; set; }
        public IBinaryNode<TValue> Left { get; set; }
        public IBinaryNode<TValue> Right { get; set; }
        public IBinaryNode<TValue> Parent { get; set; }

        public bool IsLeaf()
        {
            return !HasLeftChild() && !HasRightChild();
        }

        public bool IsLeftChild()
        {
            return HasParent() && Parent.Left == this;
        }

        public bool IsRightChild()
        {
            return HasParent() && Parent.Right == this;
        }

        public bool HasParent()
        {
            return Parent != null;
        }

        public bool HasLeftChild()
        {
            return Left != null;
        }

        public bool HasRightChild()
        {
            return Right != null;
        }

        public bool HasSibling()
        {
            if (HasParent())
                return IsLeftChild() ? Parent.HasRightChild() : Parent.HasLeftChild();
            return false;
        }

        public IBinaryNode<TValue> GetSibling()
        {
            if (HasSibling())
                return IsLeftChild() ? Parent.Right : Parent.Left;
            throw new InvalidOperationException();
        }

        public bool HasGrandParent()
        {
            return HasParent() && Parent.HasParent();
        }

        public IBinaryNode<TValue> GetGrandParent()
        {
            if (HasGrandParent())
                return Parent.Parent;
            throw new InvalidOperationException();
        }

        public bool HasUncle()
        {
            return Parent.HasSibling();
        }

        public IBinaryNode<TValue> GetUncle()
        {
            if (HasUncle())
                return Parent.GetSibling();
            throw new InvalidOperationException();
        }

        #endregion

    }
}
