﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.Tree.BinaryTree
{
    public interface IBinaryNode<TValue>
    {

        TValue Value { get; set; }

        IBinaryNode<TValue> Left { get; set; }

        IBinaryNode<TValue> Right { get; set; }

        IBinaryNode<TValue> Parent { get; set; }

        bool IsLeaf();

        bool IsLeftChild();

        bool IsRightChild();

        bool HasParent();

        bool HasLeftChild();

        bool HasRightChild();

        bool HasSibling();

        IBinaryNode<TValue> GetSibling();

        bool HasGrandParent();

        IBinaryNode<TValue> GetGrandParent();

        bool HasUncle();

        IBinaryNode<TValue> GetUncle();

    }
}
