﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Tree.BinaryTree.Iterators;

namespace ZdenekDrahos.Tree.BinaryTree
{
    public class DynamicBinaryTree<TValue> : IBinaryTree<TValue>
    {

        public DynamicBinaryTree()
        {
            Clear();
        }

        #region IBinaryTree<TValue> Members

        public void Clear()
        {
            Root = null;
            Count = 0;
        }

        public bool IsEmpty()
        {
            return Count == 0;
        }

        public int Count { get; private set; }

        public IBinaryNode<TValue> Root { get; private set; }

        public IBinaryNode<TValue> InsertRoot(TValue value)
        {
            if (IsEmpty())
            {
                Count = 1;
                Root = new BinaryNode<TValue>(value);
                return Root;
            }
            else
                throw new InvalidOperationException();
        }

        public IBinaryNode<TValue> InsertLeftChild(IBinaryNode<TValue> node, TValue value)
        {
            if (node == null || node.HasLeftChild())
                throw new ArgumentException();
            node.Left = CreateChildNode(node, value);
            Count++;
            return node.Left;
        }

        public IBinaryNode<TValue> InsertRightChild(IBinaryNode<TValue> node, TValue value)
        {
            if (node == null || node.HasRightChild())
                throw new ArgumentException();
            node.Right = CreateChildNode(node, value);
            Count++;
            return node.Right;
        }

        public IEnumerable<IBinaryNode<TValue>> BreadthFirstSearch()
        {
            return BreadthFirstSearch(Root);
        }

        public IEnumerable<IBinaryNode<TValue>> BreadthFirstSearch(IBinaryNode<TValue> start)
        {
            return new BreadthFirstSearch<TValue>(start);
        }

        public void ReplaceNode(IBinaryNode<TValue> oldNode, IBinaryNode<TValue> newNode)
        {
            if (!oldNode.HasParent())
                Root = newNode;
            else
            {
                if (oldNode.IsLeftChild())
                    oldNode.Parent.Left = newNode;
                else
                    oldNode.Parent.Right = newNode;
            }
            if (newNode != null)
                newNode.Parent = oldNode.Parent;
        }

        public void RemoveNode()
        {
            Count--;
            if (Count == 0)
                Clear();
        }

        #endregion

        public IBinaryNode<TValue> CreateChildNode(IBinaryNode<TValue> node, TValue value)
        {
            IBinaryNode<TValue> newNode = new BinaryNode<TValue>(value);
            newNode.Parent = node;
            return newNode;
        }
    }
}
