﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;

namespace ZdenekDrahos.Tree.BinaryTree.Iterators
{
    public class BreadthFirstSearch<TValue> : IEnumerable<IBinaryNode<TValue>>
    {

        private IBinaryNode<TValue> start, current;
        private Queue<IBinaryNode<TValue>> queue;

        public BreadthFirstSearch(IBinaryNode<TValue> start)
        {
            if (start == null)
                throw new ArgumentNullException();
            this.start = start;
            queue = new Queue<IBinaryNode<TValue>>();
        }

        #region IEnumerable<TValue> Members

        public IEnumerator<IBinaryNode<TValue>> GetEnumerator()
        {
            InitIterator();
            while (queue.Count > 0)
            {
                current = queue.Dequeue();
                if (current.HasLeftChild())
                    queue.Enqueue(current.Left);
                if (current.HasRightChild())
                    queue.Enqueue(current.Right);
                yield return current;
            }
        }

        #endregion

        private void InitIterator()
        {
            queue.Clear();
            queue.Enqueue(start);
        }

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
