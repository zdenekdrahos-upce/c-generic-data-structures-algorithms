﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Tree.BinaryTree;

namespace ZdenekDrahos.Tree.BinarySearchTree
{
    public interface IBinarySearchTree<TKey, TValue> where TKey : IComparable<TKey>
    {

        void Clear();

        bool IsEmpty();

        int Count { get; }

        void Add(TKey key, TValue value);

        TValue Find(TKey key);

        TValue FindClosestLeaf(TKey key);

        TValue FindMin();

        TValue FindMax();

        TValue Remove(TKey key);

        IEnumerable<TValue> BreadthFirstSearch();

        IEnumerable<TValue> BreadthFirstSearch(TKey startNode);

        IEnumerable<TKey> KeysBreadthFirstSearch();

        IEnumerable<TKey> KeysBreadthFirstSearch(TKey startNode);

    }
}
