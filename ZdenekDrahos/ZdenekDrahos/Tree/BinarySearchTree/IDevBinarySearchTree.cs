﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Tree.BinaryTree;

namespace ZdenekDrahos.Tree.BinarySearchTree
{
    public interface IDevBinarySearchTree<TKey, TValue> : IBinarySearchTree<TKey, TValue> where TKey : IComparable<TKey>
    {

        IBinaryTree<BstNode<TKey, TValue>> BinaryTree { get; }

        IBinaryNode<BstNode<TKey, TValue>> AddDev(TKey key, TValue value);

        IBinaryNode<BstNode<TKey, TValue>> FindNodeDev(TKey key);

        TValue RemoveDev(IBinaryNode<BstNode<TKey, TValue>> node);

        IBinaryNode<BstNode<TKey, TValue>> FindMinDev(IBinaryNode<BstNode<TKey, TValue>> node);

        IBinaryNode<BstNode<TKey, TValue>> FindMaxDev(IBinaryNode<BstNode<TKey, TValue>> node);

        IEnumerable<IBinaryNode<BstNode<TKey, TValue>>> BreadthFirstSearchDev();

    }
}
