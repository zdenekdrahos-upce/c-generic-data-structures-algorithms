﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using ZdenekDrahos.Tree.BinaryTree;

namespace ZdenekDrahos.Tree.BinarySearchTree
{
    public class BinarySearchTree<TKey, TValue> : IDevBinarySearchTree<TKey, TValue> where TKey : IComparable<TKey>
    {

        public BinarySearchTree()
        {
            BinaryTree = new DynamicBinaryTree<BstNode<TKey, TValue>>();
        }

        #region IBinarySearchTree<TKey,TValue> Members

        public void Clear()
        {
            BinaryTree.Clear();
        }

        public bool IsEmpty()
        {
            return BinaryTree.IsEmpty();
        }

        public int Count
        {
            get { return BinaryTree.Count; }
            private set { }
        }

        public void Add(TKey key, TValue value)
        {
            AddDev(key, value);
        }

        public TValue Find(TKey key)
        {
            IBinaryNode<BstNode<TKey, TValue>> node = FindNodeDev(key);
            return node.Value.Value;
        }

        public TValue FindClosestLeaf(TKey key)
        {
            IBinaryNode<BstNode<TKey, TValue>> current = BinaryTree.Root;
            while (current != null)
            {
                if (current.IsLeaf())
                    return current.Value.Value;
                if ((current.Value.IsKeyFromLeftBranch(key) && current.HasLeftChild()) || !current.HasRightChild())
                    current = current.Left;
                else
                    current = current.Right;
            }
            throw new KeyNotFoundException();
        }

        public TValue FindMin()
        {
            return FindMinDev(BinaryTree.Root).Value.Value;
        }

        public TValue FindMax()
        {
            return FindMaxDev(BinaryTree.Root).Value.Value;
        }

        public TValue Remove(TKey key)
        {
            IBinaryNode<BstNode<TKey, TValue>> node = FindNodeDev(key);
            return RemoveDev(node);
        }

        public IEnumerable<TValue> BreadthFirstSearch()
        {
            return BreadthFirstSearch(BinaryTree.Root.Value.Key);
        }

        public IEnumerable<TValue> BreadthFirstSearch(TKey startNode)
        {
            foreach (IBinaryNode<BstNode<TKey, TValue>> node in GetBreadthFirstSearchIterator(startNode))
                yield return node.Value.Value;
        }

        public IEnumerable<TKey> KeysBreadthFirstSearch()
        {
            return KeysBreadthFirstSearch(BinaryTree.Root.Value.Key);
        }

        public IEnumerable<TKey> KeysBreadthFirstSearch(TKey startNode)
        {
            foreach (IBinaryNode<BstNode<TKey, TValue>> node in GetBreadthFirstSearchIterator(startNode))
                yield return node.Value.Key;
        }

        #endregion

        #region IDevBinarySearchTree<TKey,TValue> Members

        public IBinaryTree<BstNode<TKey, TValue>> BinaryTree { get; private set; }

        public IBinaryNode<BstNode<TKey, TValue>> AddDev(TKey key, TValue value)
        {
            IBinaryNode<BstNode<TKey, TValue>> node = TryFindNode(key);
            if (node != null)
                throw new ArgumentException("key");
            BstNode<TKey, TValue> newNode = new BstNode<TKey, TValue>(key, value);
            if (BinaryTree.IsEmpty())
                return BinaryTree.InsertRoot(newNode);
            else
                return InsertHelper(BinaryTree.Root, newNode);
        }

        public IBinaryNode<BstNode<TKey, TValue>> FindNodeDev(TKey key)
        {
            IBinaryNode<BstNode<TKey, TValue>> node = TryFindNode(key);
            if (node != null)
                return node;
            else
                throw new KeyNotFoundException();
        }

        public IBinaryNode<BstNode<TKey, TValue>> FindMinDev(IBinaryNode<BstNode<TKey, TValue>> node)
        {
            if (node == null)
                throw new ArgumentNullException();
            while (node.Left != null)
                node = node.Left;
            return node;
        }

        public IBinaryNode<BstNode<TKey, TValue>> FindMaxDev(IBinaryNode<BstNode<TKey, TValue>> node)
        {
            if (node == null)
                throw new ArgumentNullException();
            while (node.Right != null)
                node = node.Right;
            return node;
        }

        public TValue RemoveDev(IBinaryNode<BstNode<TKey, TValue>> node)
        {
            TValue removedValue;
            if (Count == 1)
                removedValue = BinaryTree.Root.Value.Value;
            else
            {
                removedValue = node.Value.Value;
                DeleteHelper(node);
            }
            BinaryTree.RemoveNode();
            return removedValue;
        }

        public IEnumerable<IBinaryNode<BstNode<TKey, TValue>>> BreadthFirstSearchDev()
        {
            return GetBreadthFirstSearchIterator(BinaryTree.Root.Value.Key);
        }

        # endregion

        private IBinaryNode<BstNode<TKey, TValue>> TryFindNode(TKey key)
        {
            IBinaryNode<BstNode<TKey, TValue>> node = BinaryTree.Root;
            while (node != null)
            {
                if (node.Value.IsKeyFromNode(key))
                    return node;
                else if (node.Value.IsKeyFromLeftBranch(key))
                    node = node.Left;
                else
                    node = node.Right;
            }
            return null;
        }

        private IBinaryNode<BstNode<TKey, TValue>> InsertHelper(IBinaryNode<BstNode<TKey, TValue>> parentNode, BstNode<TKey, TValue> newNode)
        {
            if (parentNode.Value.IsKeyFromLeftBranch(newNode.Key))
                if (parentNode.HasLeftChild())
                    return InsertHelper(parentNode.Left, newNode);
                else
                    return BinaryTree.InsertLeftChild(parentNode, newNode);
            else
                if (parentNode.HasRightChild())
                    return InsertHelper(parentNode.Right, newNode);
                else
                    return BinaryTree.InsertRightChild(parentNode, newNode);
        }

        private IEnumerable<IBinaryNode<BstNode<TKey, TValue>>> GetBreadthFirstSearchIterator(TKey startNode)
        {
            IBinaryNode<BstNode<TKey, TValue>> start = FindNodeDev(startNode);
            return BinaryTree.BreadthFirstSearch(start);
        }

        private void DeleteHelper(IBinaryNode<BstNode<TKey, TValue>> node)
        {
            if (node.IsLeaf())
                DeleteLeafNode(node);
            else if (node.HasLeftChild() && node.HasRightChild())
                DeleteNodeWithTwoChilds(node);
            else
                DeleteNodeWithOneChild(node);
        }

        private void DeleteLeafNode(IBinaryNode<BstNode<TKey, TValue>> node)
        {
            if (node.IsLeftChild())
                node.Parent.Left = null;
            else
                node.Parent.Right = null;
        }

        private void DeleteNodeWithOneChild(IBinaryNode<BstNode<TKey, TValue>> node)
        {
            IBinaryNode<BstNode<TKey, TValue>> child = node.HasLeftChild() ? node.Left : node.Right;
            BinaryTree.ReplaceNode(node, child);
        }

        private void DeleteNodeWithTwoChilds(IBinaryNode<BstNode<TKey, TValue>> node)
        {
            // Copy key/value from predecessor and then delete it instead
            IBinaryNode<BstNode<TKey, TValue>> predecessor = FindMaxDev(node.Left);
            if (predecessor == node)
                predecessor = FindMinDev(node.Right);
            node.Value.Key = predecessor.Value.Key;
            node.Value.Value = predecessor.Value.Value;
            DeleteHelper(predecessor);
        }

    }
}
