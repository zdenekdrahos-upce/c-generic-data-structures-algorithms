﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZdenekDrahos.Tree.BinarySearchTree
{
    public class BstNode<TKey, TValue> where TKey : IComparable<TKey>
    {

        public BstNode(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

        public TKey Key { get; set; }
        public TValue Value { get; set; }

        public bool IsKeyFromLeftBranch(TKey key)
        {
            return key.CompareTo(Key) < 0;
        }

        public bool IsKeyFromNode(TKey key)
        {
            return Key.CompareTo(key) == 0;
        }

    }
}
