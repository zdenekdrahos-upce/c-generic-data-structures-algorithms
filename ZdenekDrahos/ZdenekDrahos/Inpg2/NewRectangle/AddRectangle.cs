﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System.Drawing;
using ZdenekDrahos.Inpg2.WinApi;

namespace ZdenekDrahos.Inpg2.NewRectangle
{
    /// <summary>
    /// Zapouzdření pro vytvoření nového obdélniku. Průběžný obdélník je kreslen v XOR režimu. 
    /// Obsahuje základní metody pro eventy: down, move a up. Kreslení XOR pomocí Win API.
    /// </summary>
    class AddRectangle
    {

        private XorRect xorRect;

        public AddRectangle(int minRectMize)
        {
            xorRect = new XorRect(minRectMize);
        }

        public void Down(Point startPoint)
        {
            xorRect.SetStart(startPoint);
        }

        public void Move(Point currentPoint, Graphics graphics)
        {
            if (xorRect.IsActive)
            {
                DrawRect(graphics);
                xorRect.RecalculateRectangle(currentPoint);
                DrawRect(graphics);
            }
        }

        public Rectangle Up(Point endPoint, Graphics graphics)
        {
            if (xorRect.IsActive)
            {
                DrawRect(graphics);
                xorRect.RecalculateRectangle(endPoint);
                xorRect.IsActive = false;
                if (xorRect.IsRectangleValid())
                    return xorRect.GetRectangleAndReset();                
            }
            throw new SmallRectangleException();
        }

        private void DrawRect(Graphics graphics)
        {
            using (WinApiGraphics g = new WinApiGraphics(graphics))
            {
                Point location = xorRect.Rectangle.Location;
                Size size = xorRect.Rectangle.Size;
                g.DrawXorRect(location, new Point(location.X + size.Width, location.Y + size.Height));
            }
        }

    }
}
