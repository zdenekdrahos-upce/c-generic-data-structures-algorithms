﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Drawing;

namespace ZdenekDrahos.Inpg2.NewRectangle
{
    /// <summary>
    /// Definuje obdélník kreslený pomocí tažení myší. Slouží k uložení polohy a velikosti
    /// obdélníku, přičemž velikost může být přepoítána např. během MOVE event.
    /// </summary>
    class XorRect
    {

        private Point startPoint;
        private Rectangle rectangle;
        private int minimalSize;

        public Rectangle Rectangle
        {
            get { return rectangle; }
            private set { rectangle = value; }
        }

        public bool IsActive { get; set; }

        public XorRect(int minSize)
        {
            IsActive = false;
            minimalSize = minSize;
            Rectangle = new Rectangle();
        }

        public void SetStart(Point start)
        {
            IsActive = true;
            startPoint = start;
        }

        public bool IsRectangleValid()
        {
            return rectangle.Size.Width >= minimalSize && rectangle.Size.Height >= minimalSize;
        }

        public void RecalculateRectangle(Point e)
        {
            rectangle.Location = GetNewLocation(e);
            rectangle.Size = GetNewSize(e);
        }

        private Size GetNewSize(Point e)
        {
            int sirka = Math.Abs(e.X - startPoint.X);
            int vyska = Math.Abs(e.Y - startPoint.Y);
            return new Size(sirka, vyska);
        }

        private Point GetNewLocation(Point e)
        {
            if (e.X < startPoint.X && e.Y > startPoint.Y)
                return new Point(startPoint.X - rectangle.Width, startPoint.Y);
            else if (e.X > startPoint.X && e.Y < startPoint.Y)
                return new Point(startPoint.X, startPoint.Y - rectangle.Height);
            else if (e.X < startPoint.X)
                return new Point(e.X, e.Y);
            else
                return new Point(startPoint.X, startPoint.Y);
        }

        public Rectangle GetRectangleAndReset()
        {
            Rectangle rect = Rectangle;
            Rectangle = new Rectangle();
            return rect;            
        }
    }
}
