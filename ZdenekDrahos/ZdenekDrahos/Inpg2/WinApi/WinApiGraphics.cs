﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */
using System;
using System.Runtime.InteropServices;
using System.Drawing;

namespace ZdenekDrahos.Inpg2.WinApi
{
    /// <summary>
    /// Kreslení pomocí WIN API
    /// </summary>
    public class WinApiGraphics : IDisposable
    {

        [DllImport("gdi32.dll")]
        public static extern bool LineTo(IntPtr HDC, int x, int y);
        [DllImport("gdi32.dll")]
        public static extern int SetROP2(IntPtr HDC, int drawMode);
        [DllImport("gdi32.dll")]
        public static extern bool MoveToEx(IntPtr HDC, int x, int y, IntPtr lpPoint);
        [DllImport("gdi32.dll")]
        public static extern bool Ellipse(IntPtr HDC, int nLeftRect,
        int nTopRect, int nRightRect, int nBottomRect);
        [DllImportAttribute("gdi32.dll")]
        public static extern void Rectangle(IntPtr HDC, int X1, int Y1, int X2, int Y2);
        [DllImportAttribute("gdi32.dll")]
        public static extern IntPtr GetStockObject(int brStyle);
        [DllImportAttribute("gdi32.dll")]
        public static extern IntPtr CreatePen(int enPenStyle, int nWidth, int crColor);
        [DllImportAttribute("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        [DllImportAttribute("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr HDC, IntPtr hObject);
        [DllImportAttribute("gdi32.dll")]
        public static extern IntPtr CreateSolidBrush(int crColor);

        private Graphics graphics;
        private IntPtr HDC;

        public WinApiGraphics(Graphics g)
        {
            graphics = g;
            HDC = graphics.GetHdc();
        }

        public void DrawXorRect(Point start, Point end)
        {                    
            IntPtr AktPen = WinApiGraphics.CreatePen((int)PenStyles.PS_DASH, 1, RGBColor(Color.Blue));
            IntPtr OldPen = WinApiGraphics.SelectObject(HDC, AktPen);
            IntPtr OldBrush = WinApiGraphics.SelectObject(HDC,
            WinApiGraphics.GetStockObject((int)StockObjects.NULL_BRUSH));
            int OldMode = WinApiGraphics.SetROP2(HDC, (int)RasterOps.R2_NOTXORPEN);
            WinApiGraphics.Rectangle(HDC, start.X, start.Y, end.X, end.Y);
            WinApiGraphics.SelectObject(HDC, OldPen);
            WinApiGraphics.DeleteObject(AktPen);
            WinApiGraphics.SelectObject(HDC, OldBrush);
            WinApiGraphics.SetROP2(HDC, OldMode);
        }

        public void DrawXorLine(Point start, Point end)
        {
            IntPtr AktPen = WinApiGraphics.CreatePen((int)PenStyles.PS_DASH, 1, RGBColor(Color.Black));
            IntPtr OldPen = WinApiGraphics.SelectObject(HDC, AktPen);
            IntPtr OldBrush = WinApiGraphics.SelectObject(HDC,
            WinApiGraphics.GetStockObject((int)StockObjects.NULL_BRUSH));
            int OldMode = WinApiGraphics.SetROP2(HDC, (int)RasterOps.R2_NOTXORPEN);
            WinApiGraphics.MoveToEx(HDC, start.X, start.Y, IntPtr.Zero);
            WinApiGraphics.LineTo(HDC, end.X, end.Y);
            WinApiGraphics.SelectObject(HDC, OldPen);
            WinApiGraphics.DeleteObject(AktPen);
            WinApiGraphics.SelectObject(HDC, OldBrush);
            WinApiGraphics.SetROP2(HDC, OldMode);
        }

        public void DrawXorEllipse(Point center, int radius)
        {
            IntPtr AktPen = WinApiGraphics.CreatePen((int)PenStyles.PS_DASH, 2, RGBColor(Color.Black));
            IntPtr OldPen = WinApiGraphics.SelectObject(HDC, AktPen);
            IntPtr OldBrush = WinApiGraphics.SelectObject(HDC,
            WinApiGraphics.GetStockObject((int)StockObjects.NULL_BRUSH));
            int OldMode = WinApiGraphics.SetROP2(HDC, (int)RasterOps.R2_NOTXORPEN);
            WinApiGraphics.Ellipse(HDC, center.X - radius, center.Y - radius, center.X + radius, center.Y + radius);
            WinApiGraphics.SelectObject(HDC, OldPen);
            WinApiGraphics.DeleteObject(AktPen);
            WinApiGraphics.SelectObject(HDC, OldBrush);
            WinApiGraphics.SetROP2(HDC, OldMode);
        }

        #region IDisposable Members

        public void Dispose()
        {
            graphics.ReleaseHdc(HDC);
        }

        #endregion

        private int RGBColor(Color color)
        {
            return color.ToArgb();
        }
    }
}
