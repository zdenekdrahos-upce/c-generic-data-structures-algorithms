﻿/*
 * C# Generic Data Structures & Algorithms (https://bitbucket.org/zdenekdrahos/c-generic-data-structures-algorithms)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace ZdenekDrahos.Inpg2.WinApi
{
    public enum StockObjects
    {
        BLACK_BRUSH = 0,
        DKGRAY_BRUSH = 1,
        GRAY_BRUSH = 2,
        HOLLOW_BRUSH = 3,
        LTGRAY_BRUSH = 4,
        NULL_BRUSH = 5,
        WHITE_BRUSH = 6,
        BLACK_PEN = 7,
        NULL_PEN = 8,
        WHITE_PEN = 9,
    }

}
